#include <iostream>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;

int main(int argc, char *argv[])
{
    Mat im = imread(argv[1], 0);
    if (im.empty())
    {
        cout << "Can not load image." << endl;
        return -1;
    }

    // canny edge detect
    Mat result;
    Canny(im, result, 50, 150);
    
    imwrite("lena-canny.png", result);
    imshow("lena-canny", result);
    waitKey(0);

    return 0;
}
