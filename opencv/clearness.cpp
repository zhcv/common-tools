#include <highgui/highgui.hpp>
#include <imgproc/imgproc.hpp>
#include <iostream>
#include <string> 
#include<iostream>
#include <sstream>


using namespace std;
using namespace cv;
 
int main()
{ 
    
    //定义转化后的图像
    Mat imageGrey;  
    char buffer[6000];  //左右眼虹膜图像 各3000  定义大一点
    int m=1;
    int n=1;


    for(int i=1;i<3000;i++)
    {
      sprintf(buffer,"E:\\zqx\\L\\%d.bmp",i);   //u盘中的一个文件夹
      Mat imageSource=imread(buffer);         
      //转灰度图像处理
      cvtColor(imageSource, imageGrey, CV_RGB2GRAY);   //转化成灰度图像
      Mat imageSobel;                                  //soble算子
      Laplacian(imageGrey, imageSobel, CV_16U);  
      //把灰度图像  拉普拉斯操作后 图像名字为imagesoble   为了避免数据溢出  图像深度为CV_16U
      //图像的平均灰度
      double meanValue = 0.0;
      meanValue = mean(imageSobel)[0];   //imagesoble图像 的平均灰度为0   
      //定义字符流   输出字符
      stringstream meanValueStream;
      string meanValueString;
      meanValueStream << meanValue;
      meanValueStream >> meanValueString;
      meanValueString = "Articulation(Laplacian Method): " + meanValueString;

    if(meanValue<2.4)    //自己可以定义阈值
    {
       printf("图像不合格,太模糊");
       printf("\n");
       putText(imageSource, meanValueString, Point(20, 50), CV_FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 255, 25), 2);
       imshow("不合格的图像", imageSource);
       waitKey(50);
       stringstream ss;
       ss<<m;
       string savepath="C:\\不合格的图像\\"+ss.str()+".bmp";
       imwrite(savepath,imageSource);
       m++;
    }
    else
    {
       printf("图像合格 ");
       printf("\n");
    //各参数依次是：照片/添加的文字/左上角坐标/字体/字体大小/颜色/字体粗细      Scalar(255, 255, 255)是白色字体  Scalar(0,0, 0)是黑色字体  顺序b g r  cv.color是  r g b顺序 
    //putText(imageSource, meanValueString, Point(20, 50), CV_FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 255, 25), 2);
       imshow("合格的图像", imageSource);
       waitKey(50);
       stringstream ss;
       ss<<n;
       string savepath="C:\\合格的图像\\"+ss.str()+".bmp";
       imwrite(savepath,imageSource);
       n++;

     }
    
   }
    return 0;  
}
