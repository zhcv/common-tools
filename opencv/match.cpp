
原  
Ubuntu下opencv3.3和opencv_contrib的编译安装
2017年12月12日 11:19:54 nudt_qxx 阅读数：11244
版权声明：本文为博主原创文章，未经博主允许不得转载。 https://blog.csdn.net/xiangxianghehe/article/details/78780269

###文件准备

1.opencv-3.3.1.zip
2. opencv_contrib-3.3.1.zip

###准备
先安装以下依赖包

sudo apt-get install build-essential  
  
sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev  
  
sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev  

sudo apt-get install build-essential qt5-default ccache libv4l-dev libavresample-dev  libgphoto2-dev libopenblas-base libopenblas-dev doxygen  openjdk-8-jdk pylint libvtk6-dev

sudo apt-get install pkg-config

    1
    2
    3
    4
    5
    6
    7
    8
    9

###编译
1.解压下载好的包：

sudo unzip opencv-3.3.1.zip
sudo unzip opencv_contrib-3.3.1.zip

    1
    2

2.解压完后需要将opencv_contrib.zip提取到opencv目录下，同时在该目录下新建一个文件夹build：

sudo cp -r opencv_contrib-3.3.1 opencv-3.3.1  #复制opencv_contrib到opencv目录下

cd opencv-3.3.1

sudo mkdir build                              #新建文件夹build


    1
    2
    3
    4
    5
    6
    7

3.现在进入到opencv-3.3.1目录下，查看文件结构：

#ls

3rdparty  cmake            data     LICENSE               platforms
apps      CMakeLists.txt   doc      modules               README.md
build     CONTRIBUTING.md  include  opencv_contrib-3.3.1  samples

    1
    2
    3
    4
    5

4.进入build目录，并且执行cmake生成makefile文件：


cd build  
  
 
sudo cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/home/files/opencv-3.3.1/opencv_contrib-3.3.1/modules/ .. 
 

    1
    2
    3
    4
    5
    6

sudo cmake -DENABLE_PRECOMPILED_HEADERS=OFF -D CMAKE_BUILD_TYPE=Release -DBUILD_TIFF=ON -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/home/airobot/opencv-3.4.3/opencv_contrib-3.4.3/modules/ ..

    1
    2

注意：OPENCV_EXTRA_MODULES_PATH就是你 opencv_contrib-3.3.1下面的modules目录，请按照自己的实际目录修改地址。还有后面的两点不可省略！！

接下来就是漫长的等待了…

生成完毕提示：

--   Install path:                  /usr/local
-- 
--   cvconfig.h is in:              /home/files/opencv-3.3.1/build
-- -----------------------------------------------------------------
-- 
-- Configuring done
-- Generating done
-- Build files have been written to: /home/files/opencv-3.3.1/build

    1
    2
    3
    4
    5
    6
    7
    8
    9

5.在cmake成功之后，就可以在build文件下make了：

sudo make -j8       #8线程编译
sudo make install

    1
    2

接下来就是更漫长的等待了…具体时间因人而异，我的电脑跑了20分钟。如果看到下图，说明成功了，再make install就ok了！

这里写图片描述

6.链接库共享
编译安装完毕之后，为了让你的链接库被系统共享，让编译器发现，需要执行管理命令ldconfig：

sudo ldconfig -v  

    1

###问题
Ubuntu16.04会报错：

Scanning dependencies of target opencv_test_superres
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFReadDirectory@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFGetFieldDefaulted@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFNumberOfDirectories@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFIsTiled@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFOpen@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFDefaultStripSize@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFReadRGBAImage@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFReadTile@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFSetField@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFWriteScanline@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `_TIFFfree@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFGetField@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFScanlineSize@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFSetWarningHandler@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFTileSize@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `_TIFFmalloc@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFSetErrorHandler@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFSetDirectory@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFReadScanline@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFClose@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFNumberOfTiles@LIBTIFF_4.0'
//usr/lib/x86_64-linux-gnu/libvtkIOImage-6.2.so.6.2: undefined reference to `TIFFClientOpen@LIBTIFF_4.0'
[ 74%] Building CXX object modules/imgcodecs/CMakeFiles/opencv_perf_imgcodecs.dir/perf/perf_main.cpp.o
[ 74%] Building CXX object modules/shape/CMakeFiles/opencv_test_shape.dir/test/test_main.cpp.o
[ 74%] Building CXX object modules/highgui/CMakeFiles/opencv_test_highgui.dir/test/test_main.cpp.o
[ 75%] Building CXX object modules/superres/CMakeFiles/opencv_test_superres.dir/test/test_main.cpp.o
[ 75%] Building CXX object modules/videoio/CMakeFiles/opencv_perf_videoio.dir/perf/perf_main.cpp.o
[ 75%] Building CXX object modules/videoio/CMakeFiles/opencv_test_videoio.dir/test/test_main.cpp.o
collect2: error: ld returned 1 exit status
modules/viz/CMakeFiles/opencv_test_viz.dir/build.make:233: recipe for target 'bin/opencv_test_viz' failed
make[2]: *** [bin/opencv_test_viz] Error 1
CMakeFiles/Makefile2:5627: recipe for target 'modules/viz/CMakeFiles/opencv_test_viz.dir/all' failed
make[1]: *** [modules/viz/CMakeFiles/opencv_test_viz.dir/all] Error 2
make[1]: *** Waiting for unfinished jobs...

    1
    2
    3
    4
    5
    6
    7
    8
    9
    10
    11
    12
    13
    14
    15
    16
    17
    18
    19
    20
    21
    22
    23
    24
    25
    26
    27
    28
    29
    30
    31
    32
    33
    34
    35

解决方法：

sudo apt-get autoremove libtiff5-dev

sudo apt-get install libtiff5-dev

    1
    2
    3

###例子
接下来你就可以在Clion开始写你的Opencv程序了，示例： Clion下第一个opencv3程序 ，记得在Cmakelists.txt上多花点功夫写对。

下面这个例子能跑，证明你上面环境配置完全正确！



/**
* @概述：采用FAST算子检测特征点，采用SIFT算子对特征点进行特征提取，并使用BruteForce匹配法进行特征点的匹配
* @类和函数：FastFeatureDetector + SiftDescriptorExtractor + BruteForceMatcher
*/


#include<opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;

int main(int argc, char** argv)
{
    Mat objImage = imread("1.jpg", IMREAD_COLOR);
    Mat sceneImage = imread("2.jpg", IMREAD_COLOR);
    //-- Step 1: Detect the keypoints using SURF Detector
    int minHessian = 400;
    Ptr<SURF> detector = SURF::create(minHessian);
    std::vector<KeyPoint> obj_keypoint, scene_keypoint;
    detector->detect(objImage, obj_keypoint);
    detector->detect(sceneImage, scene_keypoint);
    //computer the descriptors
    Mat obj_descriptors, scene_descriptors;
    detector->compute(objImage, obj_keypoint, obj_descriptors);
    detector->compute(sceneImage, scene_keypoint, scene_descriptors);
    //use BruteForce to match,and get good_matches
    BFMatcher matcher;
    vector<DMatch> matches;
    matcher.match(obj_descriptors, scene_descriptors, matches);
    sort(matches.begin(), matches.end());  //筛选匹配点
    vector<DMatch> good_matches;
    for (int i = 0; i < min(50, (int)(matches.size()*0.15)); i++) {
        good_matches.push_back(matches[i]);
    }
    //draw matches
    Mat imgMatches;
    drawMatches(objImage, obj_keypoint, sceneImage, scene_keypoint,good_matches, imgMatches);
    //get obj bounding
    vector<Point2f> obj_good_keypoint;
    vector<Point2f> scene_good_keypoint;
    for (int i = 0; i < good_matches.size(); i++) {
        obj_good_keypoint.push_back(obj_keypoint[good_matches[i].queryIdx].pt);
        scene_good_keypoint.push_back(scene_keypoint[good_matches[i].trainIdx].pt);
    }
    vector<Point2f> obj_box(4);
    vector<Point2f> scene_box(4);
    obj_box[0] = Point(0, 0);
    obj_box[1] = Point(objImage.cols, 0);
    obj_box[2] = Point(objImage.cols, objImage.rows);
    obj_box[3] = Point(0, objImage.rows);
    Mat H = findHomography(obj_good_keypoint, scene_good_keypoint, RANSAC); //find the perspective transformation between the source and the destination
    perspectiveTransform(obj_box, scene_box, H);
    line(imgMatches, scene_box[0]+Point2f((float)objImage.cols, 0), scene_box[1] + Point2f((float)objImage.cols, 0), Scalar(0, 255, 0), 2);
    line(imgMatches, scene_box[1] + Point2f((float)objImage.cols, 0), scene_box[2] + Point2f((float)objImage.cols, 0), Scalar(0, 255, 0), 2);
    line(imgMatches, scene_box[2] + Point2f((float)objImage.cols, 0), scene_box[3] + Point2f((float)objImage.cols, 0), Scalar(0, 255, 0), 2);
    line(imgMatches, scene_box[3] + Point2f((float)objImage.cols, 0), scene_box[0] + Point2f((float)objImage.cols, 0), Scalar(0, 255, 0), 2);
    //show the result
    imshow("匹配图", imgMatches);
    //save picture file
    imwrite("final.jpg",imgMatches);
    waitKey(0);
    return 0;
}
