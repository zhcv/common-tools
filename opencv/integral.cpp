#include <iostream>
#include <string.h>
#include <iomanip>

using namespace std;

/******************************************
* 快速计算积分图
* Integral(i,j) = Integral(i,j-1) + Integral(i-1,j) - Integral(i-1,j-1) + Image(i,j)
* 于是，对一张W*H的图像直接求取积分图，需要：(W-1)+(H-1)+3*(W-1)*(H-1)次加法
*/

void Integral(unsigned char* inputMatrix, unsigned long* outputMatrix, int width, int height)
{
    // calculate integral of the first line
    for(int i=0;i<width;i++){
        outputMatrix[i] = inputMatrix[i];
        if(i>0){
            outputMatrix[i] += outputMatrix[i-1];
        }
    }
    for (int i=1;i<height;i++){
        int offset = i*width;
        // first column of each line
        outputMatrix[offset] = outputMatrix[offset-width] + inputMatrix[offset];
        // other columns
        for (int j = 1; j < width; j++) {
            outputMatrix[offset+j] = outputMatrix[offset + j - 1] +
                         outputMatrix[offset + j - width] -
                         outputMatrix[offset + j - width - 1] +
                         inputMatrix[offset + j];
        }
    }
    return;
}

// test
int main(int argc, char** argv)
{
    cout << ">> ----------------------------" << "\n" << endl;

    int nRows = 4;
    int nCols = 6;
    unsigned char inputMatrix[24] = {1,2,3,4,5,6,
                     1,1,1,1,1,1,
                     2,1,2,1,2,1,
                     4,5,6,1,2,3};
    unsigned long outputMatrix[24];
    memset(outputMatrix, 0, 24*sizeof(unsigned long));
    Integral(inputMatrix, outputMatrix, nCols, nRows);
    for(int r = 0; r < nRows; r++) {
        for (int c = 0; c < nCols; c++) {
            cout << setw(5) << outputMatrix[r * nCols + c];
        }
        cout << endl;
    }

    cout << "\n" << ">> ----------------------------" << endl;
    return 0;
}
