def salt_pepper(img, pad=101, show=1):
    img = to_std_float(img)

    noise = np.random.randint(pad, size=(img.shape[0], img.shape[1], 1))
    # Convert high and low bounds of pad in noise to salt & pepper noise then
    # add it to image, 1 is subtracted from pad to match bounds behaviour of 
    # np.random.randint.
    img = np.where(noise == 0, 0, img)
    img = np.where(noise == (pad - 1), 1, img)

    # Properly handles the conversion from float16 back to uint8
    img = to_std_uint8(img)

    display_result(img, 'Image with Salt & Pepper Noise', show)
    return img


def to_std_float(img):
    # Converts img to 0 to 1 float to avoid wrapping that occurs with uint8
    img.astype(np.float16, copy = False)
    img = np.multiply(img, (1/255))
     
    return img

def to_std_uint8(img):
    # Properly handles the conversion to uint8
    img = cv2.convertScaleAbs(img, alpha = (255/1))
 
    return img


def display_result(img, title = 'Image', show = 1):
    cv2.imshow(title, img)
    # Required to show and close the image window
    if show == 1:
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def median(img, ksize = 3, title = 'Median Filter Result', show = 1):
    # Median filter function provided by OpenCV. ksize is the kernel size.
    img = cv2.medianBlur(img, ksize) 
    display_result(img, title, show)
 
    return img
