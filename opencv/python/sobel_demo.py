#coding=utf-8
import cv2
import numpy as np  
 
img = cv2.imread("lion.jpeg", 0)

# XXX sobel smooth
""" 
x = cv2.Sobel(img,cv2.CV_16S,1,0)
y = cv2.Sobel(img,cv2.CV_16S,0,1)
 
absX = cv2.convertScaleAbs(x)   # 转回uint8
absY = cv2.convertScaleAbs(y)
 
dst = cv2.addWeighted(absX,0.5,absY,0.5,0)
 
cv2.imshow("absX", absX)
cv2.imshow("absY", absY)
 
cv2.imshow("Result", dst)
cv2.imwrite('2MeEmcr6MCdYfnPsn_sobel.jpg', dst)

# diff = img - dst
diff = cv2.subtract(img, dst)
cv2.imwrite('2MeEmcr6MCdYfnPsn_diff2.jpg', diff)
 
cv2.waitKey(0)
cv2.destroyAllWindows() 
"""

# XXX Laplacian opreator
x = cv2.Laplacian(img, cv2.CV_16S, ksize=3)
 
dst = cv2.convertScaleAbs(x)   # convert to uint8
 
cv2.imshow("Result", dst)
cv2.imwrite('selen_laplacian.jpg', dst)

# diff = img - dst
diff = cv2.subtract(img, dst)
cv2.imwrite('selen_laplacian_diff2.jpg', diff)
 
cv2.waitKey(0)
cv2.destroyAllWindows() 
