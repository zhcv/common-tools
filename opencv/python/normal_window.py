import numpy as np
import cv2
import sys

img = cv2.imread('lena.jpg')
cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image', img)
cv2.waitKey()
cv2.destroyAllWindows()
