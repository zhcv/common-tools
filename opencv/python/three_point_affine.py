# -*- coding: utf-8 -*-
import cv2
import numpy as np


img = cv2.imread('lena.jpg')
rows, cols, ch = img.shape

pts1 = np.float32([[0, 0], [cols - 1, 0], [0, rows - 1]])

coefs = [0, rows * 0.33, rows*0.33*1.732, 0, 0, rows]  # 120°
# coefs = [0, rows * (1.732/3.732), rows/3.732, 0, 0, rows]  # 150°

pts2 = np.float32([[coefs[0], coefs[1]], [coefs[2], coefs[3]], [coefs[4], coefs[5]]])

M = cv2.getAffineTransform(pts1, pts2)
dst = cv2.warpAffine(img, M, (cols, rows))
dst1 = dst[int(coefs[3]):int(coefs[5]), int(coefs[0]):int(coefs[2]), :]

def white_backdrop(img, coef):
    rows, cols, channel= img.shape
    for i in range(rows):
        for j in range(cols):
            if j*coefs[1]+i*coefs[2]<=coefs[1]*coefs[2] \
            or j*coefs[1]+i*coefs[2] > coefs[5]*coefs[2]:
                img[i][j] = [255, 255, 255]
    cv2.imshow('d', img)
    k = cv2.waitKey(0)
    if k == ord('s'):
        cv2.imwrite('cat2.jpg', dst1)
        cv2.destroyAllWindows()

white_backdrop(dst1, coefs)
