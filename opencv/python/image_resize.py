# coding: utf-8
# =============================================================================
# INTER_NEAREST - a nearest-neighbor interpolation
# INTER_LINEAR - a bilinear interpolation (used by default)
# INTER_AREA - resampling using pixel area relation. It may be a preferred 
#   method for image decimation, as it gives moire’-free results. But when the 
#   image is zoomed, it is similar to the INTER_NEAREST method.
# INTER_CUBIC - a bicubic interpolation over 4x4 pixel neighborhood
# INTER_LANCZOS4 - a Lanczos interpolation over 8x8 pixel neighborhood
# =============================================================================
import cv2
import imageio
import itertools
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd
import time

# standard images in imageio
# ref_images = ['coffee.png', 'page.png', 'immunohistochemistry.png', 'horse.png']
ref_images = ['imageio:astronaut.png', 'imageio:chelsea.png']
# Limit starting size
images_orig = [cv2.resize(imageio.imread(im), (400, 400)) for im in ref_images]
# interpolation methods to compare
methods = [("area", cv2.INTER_AREA),
           ("nearest", cv2.INTER_NEAREST),
           ("linear", cv2.INTER_LINEAR),
           ("cubic", cv2.INTER_CUBIC),
           ("lanczos4", cv2.INTER_LANCZOS4)]

# opencv version
print "opencv version", cv2.__version__


# function to display image
def display(images, titles=['']):
    if isinstance(images[0], list):
        c = len(images[0])
        r = len(images)
        images = list(itertools.chain(*images))
    else:
        c = len(images)
        r = 1
    plt.figure(figsize=(4*c, 4*r))
    gs1 = gridspec.GridSpec(r, c, wspace=0, hspace=0)
    #gs1.update(space=0.01, hspace=0.01) # set the spacing between axes.
    titles = itertools.cycle(titles)
    for i in range(r*c):
        im = images[i]
        title = titles.next()
        plt.subplot(gs1[i])
        plt.imshow(im, cmap='gray', interpolation='none')
        plt.axis('off')
        if i < c:
            plt.title(title)
    plt.tight_layout()


images_small = [cv2.resize(im, (50, 50), interpolation=cv2.INTER_AREA) for im in images_orig]
image_set = [[cv2.resize(im, (400, 400), interpolation=m[1]) for m in methods] for im in images_small]
image_set = [[ima,] + imb for ima, imb in zip(images_small, image_set)]
names = ["original 50x50",] + [m[0] + " 400x400" for m in methods]
display(image_set, names)
plt.show()
