import os
import cv2
import numpy as np

image_path = '/data/jf/cardio_thoracic/ymlx'
label_path = '/data/jf/cardio_thoracic/ying_segment'
color_path = '/data/jf/cardio_thoracic/ymlx_color'

imgs_list = os.listdir(image_path)
labels_list = os.listdir(label_path)
f = open('ctr.txt', 'r')


for line in f.readlines():
    imfile, ctr = line.strip().split(',')
    img_name = os.path.join(image_path, imfile[:-4] + '.jpg')
    label_name = os.path.join(label_path, imfile)
    img1 = cv2.imread(img_name)
    img2 = cv2.imread(label_name, 0)
    
    height, width  = img2.shape

    kernel = np.ones((5, 5), np.uint8)

    imgh = img2.copy()
    imgb = img2.copy()

    img2[img2 != 13] = 0
    img2[img2 != 0] = 75
    img2 = cv2.erode(img2, kernel)
    img_zeros = np.zeros_like(imgb)
    imgr = cv2.merge([img_zeros, img2, img_zeros])

    imgb[imgb != 12] = 0
    imgb[imgb != 0] = 75
    imgb = cv2.erode(imgb, kernel)
    imgb = cv2.merge([img_zeros, imgb, img_zeros])

    imgh[imgh != 9] = 0
    imgh[imgh != 0] = 45
    imgh = cv2.erode(imgh, kernel)
    imgh = cv2.merge([img_zeros, img_zeros, imgh])
    
    imgc = imgr + imgb + imgh
    
    dst = cv2.addWeighted(img1, 1.0, imgc, 0.3, 0.1)
    dst = cv2.dilate(dst, kernel)
    
    # font = cv2.FONT_HERSHEY_SIMPLEX,
    font = cv2.FONT_HERSHEY_COMPLEX
    text = "CTR: %s" % str(round(float(ctr), 2))
    cv2.putText(dst, text, (width - 600, 255), font, 3, (0, 0, 250), 5)
    new_name = os.path.join(color_path, imfile)
    cv2.imwrite(new_name, dst)
    

f.close()
