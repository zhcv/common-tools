#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image
from scipy import misc,io
import os
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
from img_utils import (mls_affine_deformation_inv, 
                       mls_rigid_deformation_inv, 
                       mls_similarity_deformation_inv)

"""
1: mls_affine_deformation_inv        train/png_warp1 trainannot/png_warp1
2: mls_rigid_deformation_inv         train/png_warp2 trainannot/png_warp2
3: mls_similarity_deformation_inv    train/png_warp3 trainannot/png_warp3

raw image_path train/png  trainannot/png
img_list: img_list.txt

"""


#img_lab_path = os.path.join(linux_data_dir, all_img_lab)


def generate_ctrl_pts(height, width, size=5, range=5):
    ''' Generate control points p and q. 
        p: size x size grids
        q: size x size grids with noise
        noise: from uniform distribution

    ### Params:
        * height - integer: height of the image
        * width - integer: width of the image
        * size - integer: grid size
        * range - bound of the uniform dirtribution, [-range, range]
    ### Return:
        A tuple of p and q
    '''
    x, y = np.meshgrid(np.linspace(0, height, size), np.linspace(0, width, size))
    p = np.concatenate((x.reshape(size**2, 1), y.reshape(size**2, 1)), axis=1)
    noise = np.random.uniform(-range, range, (size, size, 2))
    noise[[0,-1], :, :] = 0
    noise[:, [0,-1], :] = 0
    q = p + noise.reshape(size**2, 2)
    return p, q


if __name__ == "__main__":
    import time
    start_time = time.time()

    src = './crop.jpg'
    dst = './crop.jpg'
    
    src_new = 'chest_warps.jpg'
    dst_new = 'chest_warps_2.jpg'

    
    imgA = misc.imread(src)
    imgB = misc.imread(dst)

    height, width = imgA.shape

    p, q = generate_ctrl_pts(height, width, size=5, range=20)
    pB, qB = generate_ctrl_pts(height, width, size=3, range=15)
    _imgA = mls_affine_deformation_inv(imgA, p, q)
    _imgB = mls_affine_deformation_inv(imgB, pB, qB)
    
    imgA_new = np.uint8(_imgA * 255)
    imgB_new = np.uint8(_imgB * 255)

    
    misc.imsave(src_new, imgA_new)
    misc.imsave(dst_new, imgB_new)

    duration = time.time() - start_time
    print "time consuming:", duration
