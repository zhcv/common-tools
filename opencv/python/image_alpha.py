import os
import cv2
import numpy as np

srcs = os.listdir('520_JPG')
src2 = os.listdir('520_LABEL')

dst = '520_SEGMENT'

for imfile in srcs[7:]:
    name1 = os.path.join('520_JPG', imfile)
    name2 = os.path.join('520_LABEL', imfile[:-4] + '_label.png')
    img1 = cv2.imread(name1)
    img2 = cv2.imread(name2, 0)
    imgb = img2.copy()
    img2[img2 != 247] = 0
    img2[img2 != 0] = 255
    img_zeros = np.zeros_like(imgb)
    imgr = cv2.merge([img_zeros, img_zeros, img2])
    imgb[imgb != 228] = 0
    imgb[imgb != 0] = 255
    imgb = cv2.merge([img_zeros,imgb, img_zeros])

    imgc = imgr + imgb
    
    # cv2.imwrite('abc.jpg', imgc)
    dst = cv2.addWeighted(img1, 1.0, imgc, 0.6, 0.1)
    new_name = os.path.join('520_SEGMENT', imfile)
    cv2.imwrite(new_name, dst)
