import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('example.png', 0)

# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# find contours of all the components and holes 
gray_temp = gray.copy() 
# copy the gray image because function
# findContours will change the imput image into another  
image, contours, hierarchy = cv2.findContours(gray_temp, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

#show the contours of the imput image
cv2.drawContours(img, contours, -1, (0, 255, 255), 2)
plt.figure('original image with contours'), plt.imshow(img, cmap = 'gray')

#find the max area of all the contours and fill it with 0
area = []
for i in xrange(len(contours)):
    area.append(cv2.contourArea(contours[i]))

max_idx = np.argmax(area)
cv2.fillConvexPoly(gray, contours[max_idx], 0)

#show image without max connect components 
plt.figure('remove max connect com')
plt.imshow(gray, cmap = 'gray')

plt.show()
