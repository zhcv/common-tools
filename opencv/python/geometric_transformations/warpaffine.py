"""
Translation is the shifting of object's location. If you know the shift in (x,y)
direction, let it be (tx,ty), you can create the transformation matrix M as 
follows:
M = [1, 0, tx, 0, 1, ty]
(shape=(2, 3))
"""

import cv2
import numpy as np

img = cv2.imread('lena.jpg', 0)
rows, cols = img.shape

M = np.float32([[1, 0, 100], [0, 1, 50]])
dst = cv2.warpAffine(img, M, (cols, rows))

cv2.imshow('img', dst)
cv2.waitKey(0)
cv2.destroyAllWindows()
