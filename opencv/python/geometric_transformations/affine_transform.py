import numpy as np
import cv2
import matplotlib.pyplot as plt


img = cv2.imread('lena.jpg')
rows,cols,ch = img.shape
b, g, r = cv2.split(img)
img = cv2.merge([r, g, b])

# img3=cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

pts1 = np.float32([[50,50],[200,50],[50,200]])
pts2 = np.float32([[10,100],[200,50],[100,250]])

M = cv2.getAffineTransform(pts1,pts2)

dst = cv2.warpAffine(img,M,(cols,rows))

plt.subplot(121),plt.imshow(img, cmap=plt.cm.brg_r),plt.title('Input')
plt.subplot(122),plt.imshow(dst),plt.title('Output')
plt.show()
