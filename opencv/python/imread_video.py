import sys
import os
import random
import math
import numpy as np
import linecache
import string
import imageio
import skimage
import cv2


if __name__ == "__main__":
    print '--STA--'
    strPath = '/home/zhang/Desktop/Megamind.avi'
    if (os.path.exists(strPath) == False):
        print 'file not exist'
    # use imageio to read video file
    videoReader = imageio.get_reader(strPath)
    FrameNum = videoReader.get_length()
    print 'frame num = ', FrameNum
    for i in range(0, FrameNum):
        img1 = videoReader.get_data(i)
        grayImg = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        print '%5d'%i, grayImg.shape
    print '--END--'

