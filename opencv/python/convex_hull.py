import matplotlib.pyplot as plt
from skimage import data, color, morphology
"""
if many objects in image, and need to computer small convex , 
convex_hull_object() function is to be used
"""

img = color.rgb2gray(data.horse())
img = (img < 0.5) * 1

chull = morphology.convex_hull_image(img)

fig, axes = plt.subplots(1, 2, figsize=(8, 8))
ax0, ax1 = axes.ravel()
ax0.imshow(img, plt.cm.gray)
ax0.set_title('orginal image')

ax1.imshow(chull, plt.cm.gray)
ax1.set_title('convex_hull_image')

# plt.show()
