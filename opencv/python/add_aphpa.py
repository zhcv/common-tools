# -*- coding: utf-8 -*-

from PIL import Image

def addTransparency(img, factor=0.7):
    img = img.convert('RGBA')
    img_blender = Image.new('RGBA', img.size, (0,0,0,0))
    img = Image.blend(img_blender, img, factor)
    return img


def add_alpha(img):
    img = img.convert('RGBA')
    r, g, b, alpha = img.split()
    alpha = alpha.point(lambda i: i>0 and 178)
    return img.putalpha(alpha)

def blend_two_images():
    """blended_img = img1 * (1 – alpha) + img2* alpha"""
    img1 = Image.open( "bridge.png ")
    img1 = img1.convert('RGBA')
 
    img2 = Image.open( "birds.png ")
    img2 = img2.convert('RGBA')
    
    img = Image.blend(img1, img2, 0.3)
    img.show()
    img.save( "blend.png")
 
    return

def blend_two_images2():
    img1 = Image.open( "bridge.png ")
    img1 = img1.convert('RGBA')
 
    img2 = Image.open( "birds.png ")
    img2 = img2.convert('RGBA')
    
    r, g, b, alpha = img2.split()
    alpha = alpha.point(lambda i: i>0 and 204)
 
    img = Image.composite(img2, img1, alpha)
 
    img.show()
    img.save( "blend2.png")
 
    return
