#!/usr/bin/env bash

git clone https://github.com/opencv/opencv.git
git https://github.com/opencv/opencv_contrib.git


git git tag --list

# git checkout same_branch/same_tag

cd opencv && git checkout 3.4.0
mv ../opencv_contrib opencv/

mkdir build

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=/home/opencv/opencv_contrib/modules/ ..

# cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local

sudo make -j8

sudo make install

sudo ldconfig -v
