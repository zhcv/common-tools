#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{   
    // 定义视频的宽度和高度
    Size s(320, 240);
    // 创建writer, 并指定FOURCC及FPS等参数
    VideoWriter writer = VideoWriter("myvideoo.avi", 
        CV_FOURCC('M', 'J', 'P', 'G'), 25, s);
    // 检查是否创建成功
    if (!writer.isOpened())
    {
        cerr << "Can not create video file.\n" << endl;
        return -1;
    }

    // 视频帧
    Mat frame(s, CV_8UC3);

    for(int i=0; i<100; i++)
    {
        // 将图像置位黑色
        frame = Scalar::all(0);
        // 将整数i转化为i字符串类型
        char text[128];
        snprintf(text, sizeof(text), "%d", i);
        // 将数字绘到画面上
        putText(frame, text, Point(s.width/3, s.height/3), 
            FONT_HERSHEY_SCRIPT_SIMPLEX, 3, Scalar(0, 0, 255), 3, 8);
        // 将图像写入视频
        writer << frame;
    }
    // 退出程序时自动关闭视频文件
    return 0;
}
