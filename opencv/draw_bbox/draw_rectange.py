#!/usr/bin/env python
# -*- coding:utf-8 -*-
from PIL import Image, ImageDraw
from PIL import ImageFont

font = ImageFont.truetype('/usr/share/fonts/truetype/wqy/wqy-microhei.ttc', 32)
im = Image.open("1.2.410.200048.22012.20180402084837.1.1.1.jpeg").convert('RGB')
draw = ImageDraw.Draw(im)



def draw_rect(bbox, text, line=5):
    x, y , x_end, y_end = bbox
    text_p = (x, y - 45)
    for i in range(1, line + 1):
        draw.rectangle((x + (line - i), y + (line - i), x_end + i, y_end + i), 
                       outline='red')
    draw.text(text_p, text, (0, 0, 255), font=font)

draw_rect([1825, 1391, 1897, 1511], u'钙化结节')
draw_rect([2064, 1835, 2166, 2107], u'胸膜增厚/粘连')

im.save('rectangle_1.2.410.200048.22012.20180402084837.1.1.1.jpg')    
# im.show()
