#include "watershedsegmentation.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void WaterShedSegmentation::setMarkers(const cv::Mat &markerImage) // 该函数将原图像转换为整数图像
{
    markerImage.convertTo(markers,CV_32S);
}

cv::Mat WaterShedSegmentation::process(const cv::Mat &image)
{
  // 使用分水岭算法
  cv::watershed(image,markers);
  return markers;
}

// 以下是两种简化结果的特殊方法
// 以图像的形式返回分水岭结果
cv::Mat WaterShedSegmentation::getSegmentation()
{
  cv::Mat tmp;
  // 所有像素值高于255的标签分割均赋值为255
  markers.convertTo(tmp,CV_8U);
  return tmp;
}

cv::Mat WaterShedSegmentation::getWatersheds()
{
  cv::Mat tmp;
  markers.convertTo(tmp,CV_8U,255,255);
  return tmp;
}
