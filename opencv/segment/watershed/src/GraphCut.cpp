/* opencv inner difine func
 *
 * cv::grabCut(image,    // 输入图像
 *             result,   // 分割输出结果
 *             rectangle,// 包含前景物体的矩形
 *             bgModel,fgModel, // 模型
 *             1,        // 迭代次数
 *             cv::GC_INIT_WITH_RECT); // 使用矩形进行初始化
 */



// GrabCut算法
    cv::Mat image= cv::imread("c:/Fig8.04(a).jpg");

    // 设定矩形
    cv::Rect rectangle(50,70,image.cols-150,image.rows-180);

    cv::Mat result; // 分割结果 (4种可能取值)
    cv::Mat bgModel,fgModel; // 模型(内部使用)
    // 进行GrabCut分割
    cv::grabCut(image, result, rectangle, bgModel, fgModel, 1, cv::GC_INIT_WITH_RECT); 
    // 得到可能为前景的像素
    cv::compare(result, cv::GC_PR_FGD, result,cv::CMP_EQ);
    // 生成输出图像
    cv::Mat foreground(image.size(), CV_8UC3, cv::Scalar(255,255,255));
    image.copyTo(foreground, result); // 不复制背景数据

    // 包含矩形的原始图像
    cv::rectangle(image, rectangle, cv::Scalar(255,255,255),1);
    cv::namedWindow("Orginal Image");
    cv::imshow("Orginal Image", image);

    // 输出前景图像结果
    cv::namedWindow("Foreground Of Segmented Image");
    cv::imshow("Foreground Of Segmented Image", foreground);
