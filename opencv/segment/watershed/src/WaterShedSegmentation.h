#ifndef WATERSHEDSEGMENTATION_H
#define WATERSHEDSEGMENTATION_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class WaterShedSegmentation
{
public:

    void setMarkers(const cv::Mat &markerImage); // 将原图像转换为整数图像
    cv::Mat process(const cv::Mat &image); // // 分水岭算法实现
    // 以下是两种简化结果的特殊方法
    cv::Mat getSegmentation();
    cv::Mat getWatersheds();

private:
    cv::Mat markers; // 用于非零像素点的标记
};

#endif // WATERSHEDSEGMENTATION_H

