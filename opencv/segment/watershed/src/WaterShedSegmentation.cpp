#include <QCoreApplication>
#include "watershedsegmentation.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // 输入待处理的图像
    cv::Mat image= cv::imread("c:/Fig4.41(a).jpg");
    if (!image.data)
        return 0;

    cv::namedWindow("Original Image");
    cv::imshow("Original Image",image);

    // 输入图像，将其转化为二值图像
    cv::Mat binary;
    binary= cv::imread("c:/Fig4.41(a).jpg",0);

    // 显示二值图像
    cv::namedWindow("Binary Image");
    cv::imshow("Binary Image",binary);

    // 移除噪点
    cv::Mat fg;
    cv::erode(binary,fg,cv::Mat(),cv::Point(-1,-1),6);

    // 显示前景图像
    cv::namedWindow("Foreground Image");
    cv::imshow("Foreground Image", fg);

    // 识别背景图像，生成的黑色像素对应背景像素
    cv::Mat bg;
    cv::dilate(binary,bg,cv::Mat(),cv::Point(-1,-1),6);
    cv::threshold(bg, bg, 1, 128, cv::THRESH_BINARY_INV);

    // 显示背景图像
    cv::namedWindow("Background Image");
    cv::imshow("Background Image", bg);

    // 显示标记图像
    cv::Mat markers(binary.size(), CV_8U,cv::Scalar(0));
    markers= fg + bg;
    cv::namedWindow("Markers");
    cv::imshow("Markers", markers);

    // 以下进行分水岭算法
    WaterShedSegmentation segmenter;

    segmenter.setMarkers(markers);
    segmenter.process(image);

    // 以下是两种处理结果，显示分割结果
    cv::namedWindow("Segmentation");
    cv::imshow("Segmentation", segmenter.getSegmentation());

    cv::namedWindow("Watersheds");
    cv::imshow("Watersheds",segmenter.getWatersheds());

    return a.exec();
}
