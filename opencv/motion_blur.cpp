/*--------------------------- motion blur and demotion blur --------------------------------*/
// 此代码用于实现模糊运动的添加与消除。
// 原理：在已知模糊运动核的前提下，可通过核线性卷积的形式对图像添加运动模糊，
//       反之也可利用该核精确的去除该运动模糊。
// 说明：本例代码是在梳理前人代码的基础上整理得到，仅使用了C++常用库与opencv2.4.5
//       AddMotionBlur的createLinearFilter函数在opencv3+版本中已经去除，故而建议只用opencv2+
//       ker 核的大小不能过大，例如，以lena图为例，ker的len为20时，会导致无法复原。 
// Input: 
//      彩色三通道图像，在读取时转化为灰度图
// output:
//      添加运动模糊的单通道灰度图，或去除运动模糊后的单通道灰度图
// version: 1.0 
// date: 2018/6/6
// by xie qunyi
// 转载请注明：
/*------------------------------------------------------------------------------------------*/
 
#include <iostream>
#include <opencv2/opencv.hpp>
 
using namespace std;
using namespace cv;
 
// Create an image of complex number type (2 channels to store 
// real part and imaginary part) from an input grayscale image
// src : single channel grayscale image input
// dst : two channel complex image output
void i2z(cv::Mat src, cv::Mat& dst)
{
    //convert the image to float type, create another one filled with zeros, 
    //and make an array of these 2 images
    cv::Mat im_array[] = { cv::Mat_<float>(src), cv::Mat::zeros(src.size(), CV_32F) };
 
    //combine as a 2 channel image to represent a complex number type image
    cv::Mat im_complex; cv::merge(im_array, 2, im_complex);
 
    //copy to destination
    im_complex.copyTo(dst);
}
 
// convert a 2 channel complex image to a single channel grayscale image
// by getting magnitude
// src : two channel complex image input
// dst : single channel grayscale image output
void z2i(cv::Mat src, cv::Mat& dst)
{
    //split the complex image to 2
    cv::Mat im_tmp[2]; cv::split(src, im_tmp);
    
    //get absolute value
    cv::Mat im_f; cv::magnitude(im_tmp[0], im_tmp[1], im_f);
    
    //copy to destination
    im_f.copyTo(dst);
}
 
// return complex image C = A./B
// if A = a+b*i and B = c+d*i;
// then C = A./B = ((a*c+b*d)/(c^2+d^2))+((b*c-a*d)/(c^2+d^2))*i
cv::Mat complexDiv(const cv::Mat& A, const cv::Mat& B)
{
    cv::Mat A_tmp[2]; cv::split(A, A_tmp);
    cv::Mat a, b;
    A_tmp[0].copyTo(a);
    A_tmp[1].copyTo(b);
    
    cv::Mat B_tmp[2]; cv::split(B, B_tmp);
    cv::Mat c, d;
    B_tmp[0].copyTo(c);
    B_tmp[1].copyTo(d);
    
    cv::Mat C_tmp[2];
    cv::Mat g = (c.mul(c)+d.mul(d));
    C_tmp[0] = (a.mul(c)+b.mul(d))/g;
    C_tmp[1] = (b.mul(c)-a.mul(d))/g;
    
    cv::Mat C;
    cv::merge(C_tmp, 2, C);
 
    return C;
}
 
// add motion blur to the src image
// motion degree is depended on the kernel ker
// ker can be product by matlab func : fspecial
// matlab code : {LEN = 3; THETA = 0; ker = fspecial('motion', LEN, THETA);}
cv::Mat AddMotionBlur(const cv::Mat& src, const cv::Mat& ker)  
{   
    // convert to float data
    cv::Mat sample_float;
    src.convertTo(sample_float, CV_32FC1);
    
    // motion blur
    cv::Point anchor(0, 0);
    double delta = 0;
    cv::Mat dst = cv::Mat(sample_float.size(), sample_float.type());
    Ptr<cv::FilterEngine> fe = cv::createLinearFilter(sample_float.type(), ker.type(), ker, anchor,
        delta, BORDER_WRAP, BORDER_CONSTANT, cv::Scalar(0));
    fe->apply(sample_float, dst);
    
    return dst; 
}
 
// remove motion blur which is added by the special kernel ker
// the same function in matlab is:
// {[hei,wid,~] = size(blurredimage);If = fft2(blurredimage);
//  Pf = fft2(ker,hei,wid); deblurred = ifft2(If./Pf);}
cv::Mat DemotionBlur(const cv::Mat& src, const cv::Mat& ker)
{ 
    // If
    Mat blurred_co; 
    i2z(src, blurred_co);
    Mat If;
    dft(blurred_co, If);
    
    // Pf
    Mat im_complex_ker; 
    cv::Mat tmp = cv::Mat::zeros(src.rows, src.cols, CV_32FC1);
    ker.copyTo(tmp(cv::Rect(0,0,ker.cols,ker.rows)));
    Mat tmp_co;
    i2z(tmp, tmp_co);
    Mat Pf;
    dft(tmp_co, Pf);
    
    // If./Pf
    cv::Mat im_co = complexDiv(If, Pf);
    
    //Convert the DFT result into grayscale
    Mat im_de;
    dft(im_co, im_de, DFT_INVERSE+DFT_SCALE); 
    Mat im_deblur; z2i(im_de, im_deblur);
    
    return im_deblur; 
}
 
int main(int argc, char** argv){
    
    // 读取测试样例
    const std::string ImageName = "./lena.jpg";
    cv::Mat DemoImage = cv::imread(ImageName, CV_LOAD_IMAGE_GRAYSCALE);
    
    // 运动模糊核
    float kernel[1][3] = {{0.333333333,0.33333333,0.33333333}};
    cv::Mat ker = cv::Mat(1, 3, CV_32FC1, &kernel);
    
    // 添加运动模糊
    cv::Mat blur = AddMotionBlur(DemoImage, ker);
    cv::imwrite("./blur.jpg", blur);

    // 去除运动模糊
    cv::Mat deblur = DemotionBlur(blur, ker);
    cv::imwrite("./deblur.jpg", deblur);

    return 0;
}
