#include <stdlib.h>
#include <assert.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <stdio.h>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv)
{
  Mat img1=imread("frame_v1.jpg");
  Mat img2=imread("frame_v2.jpg");

  Scalar mu,mu1,sigma,sigma1,mu_mu1,mu_sq,mu1_sq,sig_sq,sig_sq1,lum,con,sub1,t1,t2,t3,t4,lum1,con1;
  Scalar str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12;
  Scalar C1 = 6.5025, C2 = 58.5225,C3=C2/2;

  int dv=(img1.rows/11)+1,dv1=(img1.cols/11)+1;
  int nop=dv*dv1;

  int s_r1=(img1.rows)/dv,s_c1=(img1.cols)/dv1;
  int s_r2=(img2.rows)/dv,s_c2=(img2.cols)/dv1;

  int st_r = 0;
  int st_r1 = 0;
  
  
  for(int no_ci = 0;no_ci < dv;no_ci++)
  {
   
   int ed_r = min(st_r + s_r1,img1.rows); 
   
   if(nop == nop-1 & img1.rows - ed_r < s_r1)
   {
    ed_r = ed_r + (img1.rows - ed_r);
   }

   int st_c = 0;
   for(int no_cit = 0;no_cit < dv1;no_cit++)
   {
    
    int ed_c = min(st_c + s_c1,img1.cols);
    if(nop == nop-1 & img1.cols - ed_c < s_c1)
    {
     ed_c = ed_c + (img1.cols - ed_c);
    }
    
    Mat A_=img1(Range(st_r,ed_r),Range(st_c,ed_c));
    Mat A_1=img2(Range(st_r,ed_r),Range(st_c,ed_c));
    
    ////////////////////////////////////////////////
    
    Mat x,y;
    
    cvtColor(A_,x,CV_BGR2GRAY);
    cvtColor(A_1,y,CV_BGR2GRAY);

    //image size
    meanStdDev(x,mu,sigma);
    meanStdDev(y,mu1,sigma1);
    
    //luminance
    multiply(mu,mu1,mu_mu1);
    pow(mu,2,mu_sq);
    pow(mu1,2,mu1_sq);
    multiply(2,mu_mu1,t1);
    add(t1,C1,t1);
    
    add(mu_sq,mu1_sq,t2);
    add(t2,C1,t2);
    
    divide(t1,t2,lum);
    
    add(lum,lum1,lum1);
    
    //contrast
    multiply(sigma,sigma1,t3);
    multiply(2,t3,t3);
    add(t3,C2,t3);
    
    pow(sigma,2,sig_sq);
    pow(sigma1,2,sig_sq1);
    add(sig_sq,sig_sq1,t4);
    add(t4,C2,t4);
    
    divide(t3,t4,con);
    
    add(con,con1,con1);
    
    //structural
    Scalar str;
    Scalar s1=x.rows,s2=x.cols,N;

    multiply(s1,s2,N);
    
    for(int i=0;i<x.rows;i++)
    {
     for(int j=0;j<x.cols;j++)
     {
      str1=x.at<uchar>(i,j);
      str2=y.at<uchar>(i,j);

      subtract(str1,mu,str3);
      subtract(str2,mu1,str4);
      
      multiply(str3,str4,str7);
      divide(str7,N,str7);

      add(str7,str,str);
     }
    }
    
    add(str,C3,str8);
    
    multiply(sigma,sigma1,str9);
    add(str9,C3,str10);
    divide(str8,str10,str11);
    add(str11,str12,str12);
    /////////////////////////////////////////////////

    st_c = ed_c +1;
   }

   st_r = ed_r +1;
  }

  cout << "lum" << (lum1/nop) << "\n";
  cout << "con" << (con1/nop) << "\n";
  cout << "str" << (str12/nop) << "\n";
  cout << "ssim" << (lum1/nop)*(con1/nop)*(str12/nop) <<"\n";

  cout<<"done";
  getchar();


 }
