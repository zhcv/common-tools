#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    Mat imageSource = imread(argv[1]);
    Mat imageGrey;

    cvtColor(imageSource, imageGrey, CV_RGB2GRAY);
    Mat meanValueImage;
    Mat meanStdValueImage;

    //求灰度图像的标准差
    meanStdDev(imageGrey, meanValueImage, meanStdValueImage);
    double meanValue = 0.0;
    meanValue = meanStdValueImage.at<double>(0, 0);

    //double to string
    stringstream meanValueStream;
    string meanValueString;
    meanValueStream << meanValue*meanValue;
    meanValueStream >> meanValueString;
    meanValueString = "Articulation(Variance Method): " + meanValueString;

    putText(imageSource, meanValueString, Point(20, 88), CV_FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 255, 25), 2);
    imshow("Articulation", imageSource);
    waitKey();
}
