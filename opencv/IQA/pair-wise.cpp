// declare shifting indices array
int shifts[4][2] = {{0, 1}, {1, 0}, {1, 1}, {-1, 1}};
 
// calculate pair-wise products for every combination of shifting indices
for(int itr_shift = 1; itr_shift <= 4; itr_shift++) 
{
    int* reqshift = shifts[itr_shift - 1]; // the required shift index
    // declare shifted image
    Mat shifted_structdis(imdist_scaled.size(), CV_64F, 1);
    // BwImage is a helper class to create a copy of the image and create helper functions to access it's pixel values
    BwImage OrigArr(structdis);
    BwImage ShiftArr(shifted_structdis); 
    // calculate pair-wise component for the given orientation
    for(int i = 0; i < structdis.rows; i++)
    {
        for(int j = 0; j < structdis.cols; j++) { if(i + reqshift[0] >= 0 && i + reqshift[0] < structdis.rows && j + reqshift[1] >= 0 && j + reqshift[1] < structdis.cols)
            {
                ShiftArr[i][j] = OrigArr[i + reqshift[0]][j + reqshift[1]];
            }f
            else
            {
                ShiftArr[i][j] = 0;
            }
        }   
    }
    Mat shifted_new_structdis;
    shifted_new_structdis = ShiftArr.equate(shifted_new_structdis);
    // find the pairwise product
    multiply(structdis, shifted_new_structdis, shifted_new_structdis);
}

/*
The two for loops can be reduced to a couple of lines using cv2.warpAffine function as shown below. 
This leads to a huge speed up.
*/
# create affine matrix (to shift the image)
M = np.float32([[1, 0, reqshift[1]], [0, 1, reqshift[0]]])
ShiftArr = cv2.warpAffine(OrigArr, M, (structdis.shape[1], structdis.shape[0])

// Step 3: Prediction of Image Quality Score

// make a svm_node object and push values of feature vectors into it
struct svm_node x[37];

 // features is a rescaled vector to (-1, 1)
 for(int i =; i < 36; ++i) {
   x[i].value = features[i];
     x[i].index = i + 1; // svm_node indexes start from 1
     }

      // load the model from modelfile - allmodel
      string model = "allmodel";
      model = svm_load_model(modelfile.c_str())

       // get number of classes from the model
       int nr_class = svm_get_nr_class(model);
       double *prob_estimates = (double *) malloc(nr_class*sizeof(double));

        // predict the quality score using svm_predict_probability function
        qualityscore = svm_predict_probability(model, x, prob_estimates);
