#include <iostream>

using namespace std;

void help()
{
cout
<< "--------------------------------------------------------------------------" << endl
<< "This program shows how to write video files. You can extract the R or G or" << endl
<< "B color channel of the input video. You can choose to use the source codec" << endl
<< "(Y) or select a custom one. (N)"                                            << endl
<< "Usage:"                                                                     << endl
<< "./video-write inputvideoName [ R | G | B] [Y | N]"                          << endl
<< "--------------------------------------------------------------------------" << endl
<< endl;
}
// ...
int main(int argc, char *argv[], char *window_name)
{
help();
// here comes the actual source code
}
