/*
 * =====================================================================================
 *
 *       Filename:  image_add.cpp
 *
 *    Description:  image add weighted
 *
 *        Version:  1.0
 *        Created:  09/06/2018 12:41:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<opencv2/opencv.hpp>
#include<iostream>

using namespace std;
using namespace cv;

int main(int argc, char* *argv){
    Mat img1 = imread(argv[1]);
    Mat img2 = imread(argv[2]);

    Mat dst;
    imshow("img1", img1);
    imshow("img2", img2);
    // img1在坐标(10,10)的蓝色通道,
    cout<<"img1"<<int(img1.at<Vec3b>(10, 10)[0])<<endl; 
    cout<<"img2"<<int(img2.at<Vec3b>(10, 10)[0])<<endl;
    dst = img1 + img2;
    // add(img1, img2, dst) // 这两个加法效果相同
    // addWeight(img1, 0.5, img2, 0.5, 0, dst)
    cout<<"dst "<<int(dst.at<Vec3b>(10, 10)[0])<<endl;
    imshow("dst", dst);
    waitKey(0);
    return 0;
}
