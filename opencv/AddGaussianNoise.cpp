//mu高斯函数的偏移，sigma高斯函数的标准差
double generateGaussianNoise(double mu, double sigma)
{
    //定义小值，numeric_limits<double>::min()是函数,返回编译器允许的double型数最小值
    const double epsilon = std::numeric_limits<double>::min();
    static double z0, z1;
    static bool flag = false;
    flag = !flag;
    //flag为假构造高斯随机变量x
    if(!flag) return z1 * sigma + mu;
    //构造随机变量
    double u1, u2;
    do
    {
        u1 = rand() * (1.0 / RAND_MAX);
        u2 = rand() * (1.0 / RAND_MAX);
    } while (u1 <= epsilon);
    //flag为真构造高斯随机变量x
    z0 = sqrt(-2.0 * log(u1)) * cos(2 * CV_PI * u2);
    z1 = sqrt(-2.0 * log(u1)) * sin(2 * CV_PI * u2);
    return z0 * sigma + mu;
}
 
cv::Mat addGaussianNoise(cv::Mat &image)
{
    cv::Mat result = image.clone();
    int channels = image.channels();
    int rows = image.rows, cols = image.cols * image.channels();
    //判断图像连续性
    if (result.isContinuous()) cols = rows * cols, rows = 1;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            //添加高斯噪声
            int val = result.ptr<uchar>(i)[j] + generateGaussianNoise(2, 0.8) * 32;
            if (val < 0) val = 0;
            if (val > 255) val = 255;
            result.ptr<uchar>(i)[j] = (uchar)val;
        }
    }
    return result;
}
