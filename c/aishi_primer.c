#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define BOOL bool
#define MAXN 1000
#define MAXL 10000

bool check[MAXN];
int prime[MAXL];

int main()
{
    int n,count;
    while (scanf("%d", &n))
    {
        count = 0;
        memset(check, 0, sizeof(check));
        for (int i = 2; i <= n; i++)
        {
            if (!check[i])
                prime[count++] = i;
            for (int j = i + i; j <= n; j += i)
                check[j] = 1;
        }
        for (int i = 0; i < count; i++)
            printf("%d\n", prime[i]);
    }
    return 0;
}
