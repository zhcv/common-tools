#include <stdio.h>
#include <stdlib.h>

struct node {
    int x;
    struct node *next;
};


int main(int argc, char **argv)
{
    /*  This will be the unchanging first node */    
    struct node *root;

    /* Now root points to has its next pointer equal to a null pointer set */
    root->next = 0;
    /*  By using the -> operator, you can modify what the node, a pointer,
     *  (root in this case) points to.
     */
    /*  Segmentation fault */
    root->x = 5;

    printf("root'x = %d\n", root->x);
    
    return 0;
}
