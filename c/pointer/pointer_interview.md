Top 5 Questions about C/C++ Pointers 
====================================

This article summarizes top questions asked about C/C++ pointers on 
[stackoverflow.com](https://stackoverflow.com/) Pointers are the most confusing part of C/C++, 
those questions use simple examples to explain key pointer concepts.

1. Count from 1 to 1000 without using loops
```c
#include <stdio.h>
#include <stdlib.h>

void main(int j) {
  printf("%d\n", j);
  (&main + (&exit - &main)*(j/1000))(j+1);
}
```
The only other method to count 1 to 1000 is using recursion. According to C language, j has ‘1’as its value at the beginning. When 1 <= j < 1000, &main + (&exit - &main)*(j/1000) always evaluated to &main, which is the memory address of main. (&main)(j+1) is the next iteration we want to get, which would print ‘2’ on the screen, etc. The stop condition of this recursion is that When j hits 1000, &main + (&exit - &main)*(j/1000) evaluates to &exit, which will elegantly exit this process, and has the error code 1001 returned to the operating system.


2. Why a[5] == 5[a]?

a[b] means *(a+b) by C standard. a is base address, b is an offset starting from a. a[b] is the value in the address of a+b.

Thus a+5 and 5+a is the same memory address. Their value *(a+5) and *(5+a) is the same. So a[5] == 5[a]

3. How many levels of pointers can we have?

As much as a human can handle. Any c/c++ compiler definitely support more than that.

4. C pointer to array/array of pointers disambiguation

What is the difference between the following declarations:
```
int* arr1[8];
int (*arr2)[8];
int *(arr3[8]);
```
By C precedence table, array [], function return () have higher precedence over pointer *.

For int* arr1[8]
arr1 first and foremost is an array no matter what type the element is. After applying pointer *, we know arr1 is an array of int pointers.

For int (*arr2)[8]
By bracket overriding rule, pointer * has higher precedence over array [] in this case. Then arr2 is first and foremost a pointer no matter what it is pointing to. After applying array [], we know arr2 is a pointer to an array of int.

For int *(arr3[8])
Bracket in this case does not change any default precedence, so it is the same as int* arr1[8]

5. What's the point of const pointers?

(1) void foo(int* const ptr);
(2) void foo(int* ptr);

For the caller of foo, value of ptr is copied into foo in both (1) and (2).
(1) and (2) make a difference only for the implementer of foo, not the client of foo.

In (2), implementer may accidentally change the value of ptr which may potentially introduce bugs.

(1) is like implementer say to the compiler before writing the body of foo, “hey, I don’t want to value of ptr, if it is changed in some obscure way, make the compilation fail, let me check on that”

=======================================================================================
If you want someone to read your code, please put the code inside <pre><code> and </code></pre> tags. For example:
<pre><code>
String foo = "bar";
</code></pre>
