#include <stdio.h>
#include <stdlib.h>

/* The only other method to count 1 to 1000 is using recursion. According to C 
 * language, j has ‘1’as its value at the beginning. When 1 <= j < 1000, &main +
 * (&exit - &main)*(j/1000) always evaluated to &main, which is the memory 
 * address of main. (&main)(j+1) is the next iteration we want to get, which 
 * would print ‘2’ on the screen, etc. The stop condition of this recursion is 
 * that When j hits 1000, &main + (&exit - &main)*(j/1000) evaluates to &exit, 
 * which will elegantly exit this process, and has the error code 1001 returned 
 * to the operating system.
 */


void main(int j)
{
    printf("%d\n", j);
    (&main + (&exit - &main) * (j / 1000))(j + 1);
}
