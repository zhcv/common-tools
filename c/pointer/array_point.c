#include <stdio.h>

/*  a[b] means *(a+b) by C standard. a is base address, b is an offset 
 *  starting from a. a[b] is the value in the address of a+b. 
 *  Thus a+5 and 5+a is the same memory address. Their value *(a+5) and *(5+a) 
 *  is the same. So a[5] == 5[a] 
 *  */


int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7};
    
    printf("a[5] = %d\n", a[5]);
    
    printf("5[a] = %d\n", 5[a]);

    return 0;
}
