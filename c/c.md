# Interesting Facts in C Programming

1. The case labels of a switch statement can occur inside if-else statements.
2. arr[index] is same as index[arr]
3. We can use ‘<:, :>’ in place of ‘[,]’ and ‘<%, %>’ in place of ‘{,}’
4. Using #include in strange places. Let "a.txt" contains("GeeksforGeeks")

```
// a.txt: ("GeeksforGeeks\n")
// #include "a.txt" ===> ("GeeksforGeeks\n")
int main() 
{ 
    printf
    #include "a.txt" 
    ; 
} 
```
5. `can ignore input in scanf() by using an ‘*’ after ‘%’ in format specifiers`


# C++ bitset interesting facts
<!-- -------------------------------------------------------------------------->


## c manual
``` 
apt-get install manpages-dev
apt-get install manpages-posix-dev
```