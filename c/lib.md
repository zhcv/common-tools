# GNU C Library master sources

[source](https://sourceware.org/git/?p=glibc.git)

```git clone git://sourceware.org/git/glibc.git
```


## Release
Release tarballs are available via anonymous ftp at 
[ftp](http://ftp.gnu.org/gnu/glibc/) and its mirrors. 
