#include<stdio.h>
#include<math.h>

int main()
{
    float a, b, c, disc, x1, x2;
    scanf("a=%1f, b=%1f, c=%1f", &a, &b, &c);
    printf("a=%1f, b=%1f, c=%1f\n", a, b, c);
    disc = b * b - 4 * a * c;
    printf("disc = %1f\n", disc);
    if (disc > 0)
    {   
        x1 = (sqrt(disc) - b) / (2 * a);
        x2 = (-sqrt(disc) - b) / (2 * a);
        printf("Equality have two real root, x1=%1f, x2=%1f\n", x1, x2);
    }
    else 
    { 
        printf("Equality does'n have two real root\n"); 
    }
    return 0;
}

