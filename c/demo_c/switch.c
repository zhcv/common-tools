/*
 * =====================================================================================
 *
 *       Filename:  switch.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  09/09/2018 04:20:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<stdio.h>
#include<stdlib.h>

int main()
{   
    char major;
    printf("What is your major?\n");
    major = getchar();
    switch(major)
    {
        case 'c': printf("Computer science\n"); break;
        case 'd': printf("Digital media technology\n"); break;
        case 'i': printf("Information management\n"); break;
        default: printf("You are a student of CIS\n");
    }        
        
}

