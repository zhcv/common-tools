#include<stdio.h>

int main()
{   
    int row, column;
    for(row=1; row<6; row++)
    {
        for (column=1; column<7; column++)
        {
            if (column % 6 == 0)
                printf("\n");
            else
            {
                if (row != 3)
                {
                    if (column == 1 || column == 5)
                    {
                        printf("%c", '*');
                    }
                    else
                        printf(" ");
                }
                else
                {
                    printf("%c", '*');
                }
            }
        } 
    }
    return 0;
}
