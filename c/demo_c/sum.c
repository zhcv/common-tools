/*
 * =====================================================================================
 *
 *       Filename:  sum.c
 *
 *    Description:  sum = 1+2+...+100
 *
 *        Version:  1.0
 *        Created:  09/09/2018 04:43:02 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<stdio.h>

int main()
{
    int i=1, sum=0;
    while(i <= 100)
    {
        sum += i;
        ++i;
    }
    printf("sum=%d\n", sum);
    return 0;
}
