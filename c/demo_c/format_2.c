/*
 * =====================================================================================
 *
 *       Filename:  format_2.c
 *
 *    Description:  format output learning
 *
 *        Version:  1.0
 *        Created:  09/09/2018 04:38:04 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<stdio.h>

int main()
{
    int x=235;
    double y=3.1415926;
    printf("x=%6d, y=%14.5f\n", x, y);
    return 0;
}
