
def _get_cost(self, logits, cost_name, cost_kwargs):
    """
    Constructs the cost function, either cross_entropy, weighted cross_entropy or dice_coefficient.
    Optional arguments are:
    class_weights: weights for the different classes in case of multi-class imbalance
    regularizer: power of the L2 regularizers added to the loss function
    """

    flat_logits = tf.reshape(logits, [-1, self.n_class])
    flat_labels = tf.reshape(self.y, [-1, self.n_class])
    if cost_name == "cross_entropy":
        class_weights = cost_kwargs.pop("class_weights", None)

        if class_weights is not None:
            class_weights = tf.constant(np.array(class_weights, dtype=np.float32))

            weight_map = tf.multiply(flat_labels, class_weights)
            weight_map = tf.reduce_sum(weight_map, axis=1)

            loss_map = tf.nn.softmax_cross_entropy_with_logits_v2(logits=flat_logits,
                                                                  labels=flat_labels)
            weighted_loss = tf.multiply(loss_map, weight_map)

            loss = tf.reduce_mean(weighted_loss)

        else:
            loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=flat_logits,
                                                                             labels=flat_labels))
    elif cost_name == "dice_coefficient":
        eps = 1e-5
        prediction = pixel_wise_softmax(logits)
        intersection = tf.reduce_sum(prediction * self.y)
        union = eps + tf.reduce_sum(prediction) + tf.reduce_sum(self.y)
        loss = -(2 * intersection / (union))

    else:
        raise ValueError("Unknown cost function: " % cost_name)

    regularizer = cost_kwargs.pop("regularizer", None)
    if regularizer is not None:
        regularizers = sum([tf.nn.l2_loss(variable) for variable in self.variables])
        loss += (regularizer * regularizers)

   return loss
