import os
import cv2
from grpc.beta import implementations
import numpy as np
import tensorflow as tf
from PIL import Image
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
import datetime

"""
# from common.dicts.enums import Host, Port, Model
# Filename: enums.py
def enum(**enums):
    return type('Enum', (), enums)


Host = enum(TF_SERVING="localhost")
Port = enum(TF_SERVING="9000")
Model = enum(SEGMENT="segment")

# Usage: Host.TF_SERVING
"""

Host = 'localhost'
Port = 8500
MODEL_NAME = 'segment' # Model.SEGMENT
SIZE = 513

channel = implementations.insecure_channel(Host, int(Port))
stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)
# Send request
request = predict_pb2.PredictRequest()
request.model_spec.name = MODEL_NAME    # 'segment'



def get_segment_result(image):
    # Generate test data
    # start_time = datetime.datetime.now()
    if not isinstance(image, Image.Image):
        image = Image.open(image)
    image = image.resize((SIZE, SIZE), Image.BILINEAR).convert('RGB') 
    data = np.expand_dims(np.uint8(image), 0)
    print data.size

    tf_data = tf.contrib.util.make_tensor_proto(data, shape=[1, 513, 513, 3])
    request.inputs['input'].CopyFrom(tf_data)
    result = stub.Predict(request, 10.0)  # 10 secs timeout
    img_int64 = result.outputs['prediction'].int64_val

    img_uint8 = np.uint8(img_int64).reshape((513, 513))
    img_array = cv2.resize(img_uint8, (1024, 1024), cv2.INTER_NEAREST)

    # print datetime.datetime.now() - start_time
    return img_array



if __name__ == '__main__':
    img_name = 'images/normal.jpg'
    result = get_segment_result(img_name)
    im = Image.fromarray(result * 19)
    im.save('images/normal_segment.png')
