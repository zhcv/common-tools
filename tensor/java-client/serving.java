String file = "文件地址"
//读取文件,强制修改图片大小,设置输出文件格式bmp(模型定义时输入数据是无编码的)
BufferedImage im = Thumbnails.of(file).forceSize(224, 224).outputFormat("bmp").asBufferedImage();
//转换图片到图片数组,匹配输入数据类型为Float
Raster raster = im.getData();
List<Float> floatList = new ArrayList<>();
float [] temp = new float[raster.getWidth() * raster.getHeight() * raster.getNumBands()];
float [] pixels  = raster.getPixels(0,0,raster.getWidth(),raster.getHeight(),temp);
for(float pixel: pixels) {
    floatList.add(pixel);
}

// 记个时
long t = System.currentTimeMillis();
// 创建连接,注意usePlaintext设置为true表示用非SSL连接
ManagedChannel channel = ManagedChannelBuilder.forAddress("192.168.2.24", 9000).usePlaintext(true).build();
//这里还是先用block模式
PredictionServiceGrpc.PredictionServiceBlockingStub stub = PredictionServiceGrpc.newBlockingStub(channel);
//创建请求
Predict.PredictRequest.Builder predictRequestBuilder = Predict.PredictRequest.newBuilder();
//模型名称和模型方法名预设
Model.ModelSpec.Builder modelSpecBuilder = Model.ModelSpec.newBuilder();
modelSpecBuilder.setName("classify_data");
modelSpecBuilder.setSignatureName("predict_image");
predictRequestBuilder.setModelSpec(modelSpecBuilder);
//设置入参,访问默认是最新版本,如果需要特定版本可以使用tensorProtoBuilder.setVersionNumber方法
TensorProto.Builder tensorProtoBuilder = TensorProto.newBuilder();
tensorProtoBuilder.setDtype(DataType.DT_FLOAT);
TensorShapeProto.Builder tensorShapeBuilder = TensorShapeProto.newBuilder();
tensorShapeBuilder.addDim(TensorShapeProto.Dim.newBuilder().setSize(1));
// 150528 = 224 * 224 * 3
tensorShapeBuilder.addDim(TensorShapeProto.Dim.newBuilder().setSize(150528));
tensorProtoBuilder.setTensorShape(tensorShapeBuilder.build());
tensorProtoBuilder.addAllFloatVal(floatList);
predictRequestBuilder.putInputs("image", tensorProtoBuilder.build());
//访问并获取结果
Predict.PredictResponse predictResponse = stub.predict(predictRequestBuilder.build());
System.out.println("classify is: " + predictResponse.getOutputsOrThrow("classify").getIntVal(0));
System.out.println("prob is: " + predictResponse.getOutputsOrThrow("pro").getFloatVal(0));
System.out.println("cost time: " + (System.currentTimeMillis() - t));
