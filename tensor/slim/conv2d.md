## slim.conv2d


slim.conv2d是对tf.conv2d的进一步封装。常见调用方式:
```
net = slim.conv2d(inputs, 256, [3, 3], stride=1, scope='conv1_1')
```

source code:
```python
@add_arg_scope
def convolution(inputs,num_outputs,
                kernel_size,
                stride=1,
                padding='SAME',
                data_format=None,
                rate=1,
                activation_fn=nn.relu,
                normalizer_fn=None,
                normalizer_params=None,
                weights_initializer=initializers.xavier_initializer(),
                weights_regularizer=None,
                biases_initializer=init_ops.zeros_initializer(),
                biases_regularizer=None,
                reuse=None,
                variables_collections=None,
                outputs_collections=None,
                trainable=True,
                scope=None)

    # padding : 补零的方式，例如'SAME'
    # activation_fn : 激活函数，默认是nn.relu
    # normalizer_fn : 正则化函数，默认为None，这里可以设置为batch normalization，函数用slim.batch_norm
    # normalizer_params : slim.batch_norm中的参数，以字典形式表示
    # weights_initializer : 权重的初始化器，initializers.xavier_initializer()
    # weights_regularizer : 权重的正则化器，一般不怎么用到
    # biases_initializer : 如果之前有batch norm，那么这个及下面一个就不用管了
    # biases_regularizer :
    # trainable : 参数是否可训练，默认为True
    # scope:你绘制的网络结构图中它属于那个范围内
```

## slim.max_pool2d

```net = slim.max_pool2d(net, [2, 2], scope='pool1')```
前两个参数分别为网络输入、输出的神经元数量，第三个同上.
source code
```python
def batch_norm(inputs,
               decay=0.999,
               center=True,
               scale=False,
               epsilon=0.001,
               activation_fn=None,
               param_initializers=None,
               param_regularizers=None,
               updates_collections=ops.GraphKeys.UPDATE_OPS,
               is_training=True,
               reuse=None,
               variables_collections=None,
               outputs_collections=None,
               trainable=True,
               batch_weights=None,
               fused=False,
               data_format=DATA_FORMAT_NHWC,
               zero_debias_moving_mean=False,
               scope=None,
               renorm=False,
               renorm_clipping=None,
               renorm_decay=0.99):
    """
    接下来说我在用slim.batch_norm时踩到的坑。slim.batch_norm里有moving_mean和moving_variance两个量，
    分别表示每个批次的均值和方差。在训练时还好理解，但在测试时，moving_mean和moving_variance的含义变了。
    在训练时，有一些语句是必不可少的：
    """
```


