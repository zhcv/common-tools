# Python implementation of picking annotations code. 
# Utility functions for creating TFRecord data sets.
# ===================================================================

import cPickle
import logging
import numpy as np
import os
import xml.etree.ElementTree as ET

logger = logging.getLogger(__name__)

def int64_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def int64_list_feature(value):
  return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def bytes_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def bytes_list_feature(value):
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def float_list_feature(value):
  return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def read_examples_list(path):
  """Read list of training or validation examples.

  The file is assumed to contain a single example per line where the first
  token in the line is an identifier that allows us to find the image and
  annotation xml for that example.

  For example, the line:
  xyz 3
  would allow us to find files xyz.jpg and xyz.xml (the 3 would be ignored).

  Args:
    path: absolute path to examples list file.

  Returns:
    list of example identifiers (strings).
  """
  with tf.gfile.GFile(path) as fid:
    lines = fid.readlines()
  return [line.strip().split(' ')[0] for line in lines]


def recursive_parse_xml_to_dict(xml):
  """Recursively parses XML contents to python dict.

  We assume that `object` tags are the only ones that can appear
  multiple times at the same level of a tree.

  Args:
    xml: xml tree obtained by parsing XML file contents using lxml.etree

  Returns:
    Python dictionary holding XML contents.
  """
  if not xml:
    return {xml.tag: xml.text}
  result = {}
  for child in xml:
    child_result = recursive_parse_xml_to_dict(child)
    if child.tag != 'object':
      result[child.tag] = child_result[child.tag]
    else:
      if child.tag not in result:
        result[child.tag] = []
      result[child.tag].append(child_result[child.tag])
  return {xml.tag: result}


def parse_line(line):
    """Parse a line file."""
    fileName, typeName, typeId, xmin, ymin, xmax, ymax = line.split(',')
    obj_struct = {}
    obj_struct['type'] = typeName
    obj_struct['typeId'] = int(typeId)
    obj_struct['bbox'] = [int(xmin), int(ymin), int(xmax), int(ymax)]

    return [obj_struct]
   

def pick_gt(imagesetfile, cachedir, imgs_list): 
    """
    assumes detections are in detpath.format(classname)
    assumes annotations are in annopath.format(imagename)
    assumes imagesetfile is a text file with each line an image name
    cachedir caches the annotations in a pickle file
    """

    # first load gt
    if not os.path.isdir(cachedir):
        os.mkdir(cachedir)
    cachefile = os.path.join(cachedir, 'tb_annots.pkl')
    # read list of images
    with open(imagesetfile, 'r') as f:
        lines = f.readlines()
    imagenames = [x.strip() for x in lines]

    if not os.path.isfile(cachefile):
        # load annotations
        recs = {}
        for i, line in enumerate(imagenames):
            imagename = line.split(',')[0]
            if imagename not in recs:
                recs[imagename] = parse_line(line)
            elif imagename in recs:
                recs[imagename].extend(parse_line(line))
            
            if i % 100 == 0:
                logger.info('Reading annotation for {:d}/{:d}'.format(i + 1, len(imagenames)))
        # save
        logger.info('Saving cached annotations to {:s}'.format(cachefile))
        with open(cachefile, 'w') as f:
            cPickle.dump(recs, f)
    else:
        # load
        with open(cachefile, 'r') as f:
            recs = cPickle.load(f)


if __name__ == '__main__':
    imagesetfile = 'lesion.csv'
    cachedir = '.'
    with open('tb_link.txt', 'r') as f:
        imgs_list = [os.path.splitext(l.strip())[0] for l in f.readlines()]
    pick_gt(imagesetfile, cachedir, imgs_list)
