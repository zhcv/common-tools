from __future__ import division
import io
import hashlib
import tensorflow as tf
import contextlib2
import json
import base64

import PIL.Image
from object_detection.utils import dataset_util
from object_detection.dataset_tools import tf_record_creation_util
from mysql import connector


flags = tf.app.flags
flags.DEFINE_string('output_path', './', 'Path to output TFRecord')
flags.DEFINE_string('input_path', 'images/', 'Path to input images')
FLAGS = flags.FLAGS


cnx = connector.connect(host="192.168.10.26", user="ai", passwd="hangzhou", db="ai")


def create_tf_example(example):
    patientId = str(example[0])
    filename = FLAGS.input_path + patientId + '.png'
    with tf.gfile.GFile(filename, 'rb') as fid:
        encoded_image = fid.read()
    encoded_image_io = io.BytesIO(encoded_image)
    image = PIL.Image.open(encoded_image_io)
    image_width, image_height = image.size
    crop1 = image.crop((0, 0, image_width/2, image_height/2))
    crop2 = image.crop((image_width/2, 0, image_width, image_height/2))
    crop3 = image.crop((0, image_height/2, image_width/2, image_height))
    crop4 = image.crop((image_width/2, image_height/2, image_width, image_height))
    
    key = hashlib.sha256(encoded_image).hexdigest()

    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []

    classes_text = []
    classes = []

    query = ("select x, y, width, height from kaggle_pneumonia where patientId = '" + patientId + "'")
    cursor = cnx.cursor(buffered=True)
    cursor.execute(query)
 
    for (x, y, width, height) in cursor:
        xmins.append(x/image_width)
        xmaxs.append((x+width)/image_width)
        ymins.append(y/image_height)
        ymaxs.append((y+height)/image_height)
        classes_text.append("Lung Opacity".encode("utf8"))
        classes.append(1)
    #print xmins, xmaxs, ymins,ymaxs, classes_text, classes
    cursor.close()
    tf_example = tf.train.Example(features=tf.train.Features(feature={
      'image/height': dataset_util.int64_feature(image_height),
      'image/width': dataset_util.int64_feature(image_width),
      'image/filename': dataset_util.bytes_feature(filename.encode('utf8')),
      'image/source_id': dataset_util.bytes_feature(patientId.encode('utf8')),
      'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
      'image/encoded': dataset_util.bytes_feature(encoded_image),
      'image/format': dataset_util.bytes_feature('png'.encode('utf8')),
      'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
      'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
      'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
      'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
      'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example

def create_tf_record(bag):
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path + bag + '.record')

    cursor = cnx.cursor(buffered=True)
    query = "SELECT patientId FROM kaggle_pneumonia_patient WHERE hasTarget = 1 and bag = '" + bag + "' limit 1"
    cursor.execute(query)
    i = 0
    for example in cursor:
        print i
        tf_example = create_tf_example(example)
        writer.write(tf_example.SerializeToString())
        i = i+1
   
    cursor.close()
    writer.close()


def main(_):
    create_tf_record('train')
    create_tf_record('val')
    cnx.close()

if __name__ == '__main__':
    tf.app.run()
