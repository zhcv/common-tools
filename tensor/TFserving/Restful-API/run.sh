#!/usr/bin/env bash


docker build -q -t damienpontifex/dogscats-serving .
docker run --rm -d -p 8500:8500 --name dogscats damienpontifex/dogscats-serving
