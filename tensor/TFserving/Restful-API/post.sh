#!/usr/bin/env bash

curl -X POST \
http://${DOCKER_HOST}:8501/v1/models/<modelname>/versions/<version>:classify \
-d '{"signature_name": <signature_name>,
"examples": [
    {
        "image": { "b64": "<use a converted base 64 encoding image> or $( base64 <image_path> )" },
    }
    ]
}'
