import tensorflow as tf

def serving_input_receiver_fn():
    feature_spec = {
        'image': tf.FixedLenFeature([], dtype=tf.string)
    }
    default_batch_size = 1

    serialized_tf_example = tf.placeholder(
        dtype=tf.string, shape=[default_batch_size],
        name='input_image_tensor')

    received_tensors = { 'images': serialized_tf_example }
    features = tf.parse_example(serialized_tf_example, feature_spec)

    fn = lambda image: _img_string_to_tensor(image, input_img_size)

    features['image'] = tf.map_fn(fn, features['image'], dtype=tf.float32)

    return tf.estimator.export.ServingInputReceiver(features, received_tensors)

estimator.export_savedmodel('export', serving_input_receiver_fn)

feature_spec = {
    'image': tf.FixedLenFeature([], dtype=tf.string)
}

default_batch_size = 1

serialized_tf_example = tf.placeholder(
    dtype=tf.string, shape=[default_batch_size],
    name='input_image_tensor')

received_tensors = { 'images': serialized_tf_example }
features = tf.parse_example(serialized_tf_example, feature_spec)

fn = lambda image: _img_string_to_tensor(image, input_img_size)

features['image'] = tf.map_fn(fn, features['image'], dtype=tf.float32)


def _img_string_to_tensor(image_string, image_size=(299, 299)):
    image_decoded = tf.image.decode_jpeg(image_string, channels=3)
    # Convert from full range of uint8 to range [0,1] of float32.
    image_decoded_as_float = tf.image.convert_image_dtype(image_decoded, dtype=tf.float32)
    # Resize to expected
    image_resized = tf.image.resize_images(image_decoded_as_float, size=image_size)

    return image_resized

