#!/usr/bin/env bash

saved_model_cli show --dir exports/1525524220

# The given SavedModel contains the following tag-sets: serve
saved_model_cli show --dir exports/1525524220 --tag_set serve

saved_model_cli show --dir exports/1525524220 --all
