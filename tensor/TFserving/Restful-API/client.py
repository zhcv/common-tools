import` requests
import base64
import json

image = r"./test2.jpg"
URL = "http://{HOST:port}/v1/models/<modelname>/versions/1:classify" 
headers = {"content-type": "application/json"}
image_content = base64.b64encode(open(image,'rb').read()).decode("utf-8")
body = {
    "signature_name": "serving_default",
    "examples": [
                {"image/encoded": {"b64":image_content}}
                ]
    }
r = requests.post(URL, data=json.dumps(body), headers = headers) 
print(r.text)
