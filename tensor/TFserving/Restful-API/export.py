import tensorflow as tf


serialized_tf_example = tf.placehold(tf.string, name='tf_example')

feature_configs = {
    'image/encoded': tf.FixedLenFeature(shape=[], dtype=tf.string)
}

tf_example = tf.parse_example(serialized_tf_example, feature_configs)

jpegs = tf_example['image/encoded'] 
image_string = tf.reshape(jpegs, shape=[])

# ...........
# Export inference model.

output_path = os.path.join(tf.compat.as_bytes(output_dir), 
    tf.compat.as_bytes(str(model_version)))
print "Export trained model to", output_path
builder = tf.saved_model_builder_SavedModelBuilder(output_path)


# Build the signature_def_map
classify_inputs_tensor_info = tf.saved_model.utils.build_tensor_info(serialized_tf_example)
# classify_output_tensor_info = tf.saved_model.utils.build_tensor_info(classes)
scores_output_tensor_info = tf.saved_model.utils.build_tensor_info(probabilities)

classification_signature = (
    tf.saved_model.signature_def_utils.build_signature_def(
    inputs={
        tf.saved_model.signature_constants.CLASSIFY_INPUTS:
            classify_inputs_tensor_info
    },
    outputs={
        tf.saved_model.signature_constants.CLASSIFY_OUTPUT_SOCRES:
            scores_output_tensor_info
    },
    method_name=tf.saved_model.signature_constants
    .CLASSIFY_METHOD_NAME))


predict_inputs_tensor_info = tf.saved_model.utils.build_tensor_info(jpegs)
predict_signature = (
    tf.saved_model.signature_def_utils.build_signature_def(
        inputs={'image': predict_inputs_tensor_info},
        outputs={'score': scores_output_tensor_info
    },
        method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME)
    )
legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
builder.add_meta_graph_and_variables(
    sess, [tf.saved_model_tag_constants.SERVING],
    signaute_def_map={
        'predict_images': prediction_signaturee,
        tf.saved_model.signature_constants.
        DEFUALT_SERVING_SIGNATURE_DEF_KEY: classification_signature,},
    legacy_init_op=legacy_init_op)

builder.save()
print "Successfully exported model to %s" % output_dir
