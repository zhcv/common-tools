import os
import urllib
import tensorflow as tf
from grpc.beta import implementations
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2, model_pb2


def make_request(stub, file_path):
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'default'
    
    if file_path.startswith('http'):
        data = urllib.request.urlopen(file_path).read()
    else:
        with open(file_path, 'rb') as f:
            data = f.read()
    
    feature_dict = {
        'image': tf.train.Feature(bytes_list=tf.train.BytesList(value=[data]))
    }
    example = tf.train.Example(features=tf.train.Features(feature=feature_dict))
    serialized = example.SerializeToString()
    
    request.inputs['inputs'].CopyFrom(tf.contrib.util.make_tensor_proto(serialized, shape=[1]))
    
    result_future = stub.Predict.future(request, 10.0)
    prediction = result_future.result()
    
    predicted_classes = list(zip(prediction.outputs['classes'].string_val, prediction.outputs['scores'].float_val))
    
    predicted_classes = list(reversed(sorted(predicted_classes, key = lambda p: p[1])))
    
    return predicted_classes

channel = implementations.insecure_channel('localhost', 8500)
stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

"""
With this setup, we can call our serving host with a couple of calls, 
each showing a local dog picture and a remote cat picture.
"""

dog_path = os.path.expanduser('~/Downloads/Dog_CTA_Desktop_HeroImage.jpg')
output = make_request(stub, dog_path)
print(output)
# [(b'dogs', 0.9998657703399658), (b'cats', 0.00013415749708656222)]

output = make_request(
    stub, 
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeclOl-kLClSo9SS0fH1dF2h35hABWBwQCYQMI2HWGZY4H6teBfg')
print(output)
# [(b'cats', 0.9999951124191284), (b'dogs', 4.922635525872465e-06)]
