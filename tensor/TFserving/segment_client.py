import os
import cv2
from grpc.beta import implementations
import numpy as np
import tensorflow as tf
from PIL import Image
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2

Host = "localhost"
Port = "9000"
SIZE = 513

def get_segment(image, rotation = 0):
    channel = implementations.insecure_channel(Host, int(Port))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)
    # Send request
    request = predict_pb2.PredictRequest()
    # request.model_spec.name = Model.SEGMENT  # "segment"
    request.model_spec.name = "segment"
    if not isinstance(image, Image.Image):
        image = Image.open(image)
    image = image.resize((SIZE, SIZE), Image.BILINEAR).convert('RGB')
    if rotation != 0:
        image = image.transpose(rotation + 1) 
    data = np.expand_dims(np.uint8(image), 0)

    tf_data = tf.contrib.util.make_tensor_proto(data, shape=[1, 513, 513, 3])
    request.inputs['input'].CopyFrom(tf_data)
    result = stub.Predict(request, 10.0)  # 10 secs timeout
    img_int64 = result.outputs['prediction'].int64_val

    img_uint8 = np.uint8(img_int64).reshape((513, 513))
    img_array = cv2.resize(img_uint8, (1024, 1024), cv2.INTER_NEAREST)

    return img_array


if __name__ == '__main__':
    image_name = 'images/normal.jpg'
    result = get_segment(image_name)
    im = Image.fromarray(result * 19)
    im.save('images/normal_segment.png')
