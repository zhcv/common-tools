import numpy as np
import json
import tensorflow as tf
from PIL import Image

from grpc.beta import implementations
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
from google.protobuf.json_format import MessageToJson

HOST = 'localhost'
PORT = 9000
MODEL_NAME = 'blur'
SIZE = 299   # INPUT IMAGE_SIZE
TH = 0.989   # Threshold to estimate the image blur


def model_prediction(host, port, model_name, input_data):
    model_input = {"image": input_data}

    channel = implementations.insecure_channel(host, int(port))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    request = predict_pb2.PredictRequest()
    request.model_spec.name = model_name

    for k, v in model_input.items():
        request.inputs[k].CopyFrom(v)
    result = stub.Predict(request, 10.0)    # 10 secs timeout
    return MessageToJson(result)


def blur_predict(image):
    # StatusCode = 0  # normal status code
    image = image.resize((SIZE, SIZE), Image.BILINEAR).convert('RGB')
    image = np.asarray(image, dtype=np.float32) / 128.0 - 1
    tf_data = np.expand_dims(image, 0)

    x = tf.contrib.util.make_tensor_proto(tf_data, shape=[1, SIZE, SIZE, 3])
    rst = model_prediction(HOST, PORT, MODEL_NAME, x)
    jchestpa = json.loads(rst)
    prob = jchestpa["outputs"]["prediction"]['floatVal'][1]
    
    return prob
