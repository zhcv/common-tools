Bazel rules to package the TensorFlow APIs in languages other than Python into
archives.

## C library

The TensorFlow [C
API](https://www.tensorflow.org/code/tensorflow/c/c_api.h)
is typically a requirement of TensorFlow APIs in other languages such as
[Go](https://www.tensorflow.org/code/tensorflow/go)
and [Rust](https://github.com/tensorflow/rust).

The following commands:

```sh
bazel test --config opt //tensorflow/tools/lib_package:libtensorflow_test
bazel build --config opt //tensorflow/tools/lib_package:libtensorflow
```

test and produce the archive at
`bazel-bin/tensorflow/tools/lib_package/libtensorflow.tar.gz`, which can be
distributed and installed using something like:

```sh
tar -C /usr/local -xzf libtensorflow.tar.gz
```



export TF_NEED_CUDA=1
export TF_NCCL_VER



bazel build --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package
./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg

