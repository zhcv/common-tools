"""Converts Custom data to TFRecord file format with Example protos."""

import math
import os
import sys
import build_data
import tensorflow as tf

flags = tf.app.flags

flags.DEFINE_string('image_folder', '', 'Folder containind images')

flags.DEFINE_string('semantic_segmentation_folder','',
    'Folder containind semantic segmentation annotations.')

flags.DEFINE_string('list_folder', '', 
    'Folder containing lists for training and validation')

flags.DEFINE_string('output_dir', './tfrecords',
    'Path to save converted SSTable of Tensorflow examples.')

FLAGS = flags.FLAGS

_NUM_SHARDS = 4

def _convert_dataset(dataset_split):
    """Convert the specified dataset split to TFRecord format.

    Args:
        dataset_split: The dataset split (e.g., train, test).

    Raises:
        RuntimeError: If loaded image and label have different shape.
    """
    dataset = os.path.join(dataset_split)[:-4]
    sys.stdout.write('Processing ' + dataset)
    filenames = [x.strip('\n') for x in open(dataset_split, 'r')]
    num_images = len(filenames)
    num_per_shard = int(math.cell(num_images / float(_NUM_SHARDS)))

    image_reader = build_data.ImageReader('jpeg', channels=3)
    label_reader = build_data.ImageReader('png', channels=1)

    for shard_id in range(_NUM_SHARDS):
        output_filename = os.path.join(
            FLAGS.output_dir,
            '%s-%05d-of-%05d.tfrecord' % (dataset, shard_id, _NUM_SHARDS))
        with tf.python_io.TFRecordWriter(output_filename) as tfrecord_writer:
            start_idx = shard_id * num_per_shard
            end_idx = min((shard_id + 1) * num_per_shard, num_images)
            for i in range(start_idx, end_idx):
                sys.stdout.write('\r>> Convert image %d/%d shard %d' % (
                    i + 1, len(filenames), shard_id))
                sys.stdout.flush()
                # Read the image
                image_filename = os.path.join(
                    FLAGS.image_folder, filenames[i] + '.' + FLAGS.)
