# -*- coding: utf-8 -*-
import os
import numpy as np
import tensorflow as tf


def parse_objects():
    feature_map = {
        'image/height': tf.FixedLenFeature([], tf.int64),
        'image/width': tf.FixedLenFeature([], tf.int64),
        'image/filename': tf.FixedLenFeature([], tf.string),
        'image/source_id': tf.FixedLenFeature([], tf.string),
        'image/key/sha256': tf.FixedLenFeature([], tf.string),
        'image/encoded': tf.FixedLenFeature([], tf.string),
        'image/format': tf.FixedLenFeature([], tf.string),
        'image/object/bbox/xmin': tf.FixedLenSequenceFeature([], tf.float32, True),
        'image/object/bbox/xmax': tf.FixedLenSequenceFeature([], tf.float32, True),
        'image/object/bbox/ymin': tf.FixedLenSequenceFeature([], tf.float32, True),
        'image/object/bbox/ymax': tf.FixedLenSequenceFeature([], tf.float32, True),
        'image/object/class/text': tf.FixedLenSequenceFeature([], tf.string, True),
        'image/object/class/label': tf.FixedLenSequenceFeature([], tf.int64, True),
    }
    return feature_map


def parse_tfrecords(filename_queue, is_batch):
    """Parse TFRecords"""

    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)  # return filename and file
    feature_map = parse_objects()
    features = tf.parse_single_example(serialized_example, feature_map)

    filename_tensor2 = features['image/filename']
    image_tensor = features['image/encoded']
    xmins_tensor = features['image/object/bbox/xmin']
    xmaxs_tensor = features['image/object/bbox/xmax']
    ymins_tensor = features['image/object/bbox/ymin']
    ymaxs_tensor = features['image/object/bbox/ymax']
    labels_tensor = features['image/object/class/label']

    # label = tf.cast(labels_tensor, dtype=tf.int32)
    print "label shape", labels_tensor.get_shape()

    # labels_tensor = tf.gather_nd(labels_tensor, [0]) # get value through index

    # image_png = tf.image.decode_png(image_tensor, channels=3, dtype=tf.uint8)
    image = tf.image.decode_png(image_tensor, channels=3, dtype=tf.uint8)
    _img = tf.image.resize_images(image, (1024, 1024))
    img = tf.cast(_img, tf.float32)

    label = tf.concat([1], 0)

    print "la shape", label.get_shape()
    if is_batch:
        batch_size = 5
        min_after_dequeue = 30
        capacity = min_after_dequeue + 3 * batch_size
        images, labels = tf.train.shuffle_batch(
            tensors=[img, label],
            batch_size=batch_size,
            num_threads=3,
            capacity=capacity,
            min_after_dequeue=min_after_dequeue)

    return images, labels_tensor
 
if __name__=='__main__':
    test_filename = 'val.record'
    filename_queue = tf.train.string_input_producer([test_filename], num_epochs=None)
    images, labels = parse_tfrecords(filename_queue, is_batch=True)

    with tf.Session() as sess:
        init_op = tf.global_variables_initializer()
        sess.run(init_op)
        coord=tf.train.Coordinator()
        threads= tf.train.start_queue_runners(coord=coord)
        try:
            while not coord.should_stop():
            # for i in range(15):
                image_name, la = sess.run([images, labels])
                print image_name.shape
        except tf.errors.OutOfRangeError:
            print('Done reading')
        finally:
            coord.request_stop()
 
        coord.request_stop()
        coord.join(threads)
