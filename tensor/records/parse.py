# -*- coding: utf-8 -*-
import tensorflow as tf


def read_and_decode(filename):
    # generate a queue according to tfrecords_filename
    filename_queue = tf.train.string_input_producer([filename])

    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)   # return filename and file
    features={
        'image/object/class/label': tf.FixedLenSequenceFeature([], tf.int64, True),
        'image/encoded': tf.FixedLenFeature([], tf.string),
    }
    features = tf.parse_single_example(serialized_example, features)

    img = tf.image.decode_png(features['image/encoded'], 3,  tf.uint8)
    _img = tf.image.resize_images(img, (1024, 1024))
    image = tf.cast(_img, tf.float32)
    # img = tf.cast(img, tf.float32) * (1. / 255) - 0.5
    # label = features['image/object/class/label']
    label = tf.concat([1], 0)

    return image, label


img, label = read_and_decode("val.record")
# print img.get_shape()
# print label.get_shape()

# shuffle_batch
img_batch, label_batch = tf.train.shuffle_batch([img, label],
                                                batch_size=4, capacity=200,
                                                min_after_dequeue=100)


init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    threads = tf.train.start_queue_runners(sess=sess)
    for i in range(200):
        val, l= sess.run([img_batch, label_batch])
        #l = to_categorical(l, 12)
        print val.shape
