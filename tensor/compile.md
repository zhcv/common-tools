bazel clean --expunge

CUDA support — YES
CUDA version — 10.0
cuDNN version — 7.3
NCCL — 1.3

```
export TF_CUDA_VERSION=10.0
export TF_NCCL_VERSION : The NCCL version.
export NCCL_INSTALL_PATH: The installation path of the NCCL library.
export NCCL_HDR_PATH=NCCL_H  the path of the nccl.h 
export NCCL_INSTALL_PATH=/usr/local/cuda/lib64
export TF_NEED_CUDA=1
bazel build --config=opt --config=cuda --cxxopt="-D_GLIBCXX_USE_CXX11_ABI=0" //tensorflow/tools/pip_package:build_pip_package

./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
