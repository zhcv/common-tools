# YOLOv3训练自己的数据详细步骤

1. 下载YOLOv3工程项目
```
git clone https://github.com/pjreddie/darknet
cd darknet
```
2. 修改Makefile配置，使用GPU训练，修改如下：
```
GPU=1 #如果使用GPU设置为1，CPU设置为0
CUDNN=1  #如果使用CUDNN设置为1，否则为0
OPENCV=0 #如果调用摄像头，还需要设置OPENCV为1，否则为0
OPENMP=0  #如果使用OPENMP设置为1，否则为0
DEBUG=0  #如果使用DEBUG设置为1，否则为0
```
CC=gcc
NVCC=/home/user/cuda-9.0/bin/nvcc   #NVCC=nvcc 修改为自己的路径
AR=ar
ARFLAGS=rcs
OPTS=-Ofast
LDFLAGS= -lm -pthread 
COMMON= -Iinclude/ -Isrc/
CFLAGS=-Wall -Wno-unused-result -Wno-unknown-pragmas -Wfatal-errors -fPIC
...
ifeq ($(GPU), 1) 
COMMON+= -DGPU -I/home/hebao/cuda-9.0/include/  #修改为自己的路径
CFLAGS+= -DGPU
LDFLAGS+= -L/home/hebao/cuda-9.0/lib64 -lcuda -lcudart -lcublas -lcurand  #修改为自己的路径
endif

保存完成后，在此路径下执行make，如果出现如下错误：

Loadingweights from yolo.weights...Done!
CUDA Error:invalid device function
darknet: ./src/cuda.c:21: check_error: Assertion `0' failed.
Aborted (core dumped)

这是因为配置文件Makefile中配置的GPU架构和本机GPU型号不一致导致的。更改前默认配置如下（不同版本可能有变）：

ARCH= -gencode arch=compute_30,code=sm_30 \
      -gencode arch=compute_35,code=sm_35 \
      -gencode arch=compute_50,code=[sm_50,compute_50] \
      -gencode arch=compute_52,code=[sm_52,compute_52]
#      -gencode arch=compute_20,code=[sm_20,sm_21] \ This one is deprecated?
# This is what I use, uncomment if you know your arch and want to specify
# ARCH= -gencode arch=compute_52,code=compute_52

CUDA官方说明文档：http://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/index.html#virtual-architecture-feature-list

3. 准备训练数据集

按下列文件夹结构，将训练数据集放到各个文件夹下面，生成4个训练、测试和验证txt文件列表
VOCdevkit
—VOC2007
——Annotations
——ImageSets
———Layout
———Main
———Segmentation
——JPEGImages
Annotations中是所有的xml文件
JPEGImages中是所有的训练图片
Main中是4个txt文件，其中test.txt是测试集，train.txt是训练集，val.txt是验证集，trainval.txt是训练和验证集。
3. 生成2007_train.txt和2007_val.txt文件
下载voc_label.py文件，将文件下载到VOCdevkit同级的路径下，生成训练和验证的文件列表
`wget https://pjreddie.com/media/files/voc_label.py`
```
修改sets为训练样本集的名称
sets=[('2007', 'train')]
修改classes为训练样本集的类标签
classes=[str(i) for i in range(10)]
运行python voc_label.py，生成2007_train.txt训练文件列表。
```
---
4.下载Imagenet上预先训练的权重
`wget https://pjreddie.com/media/files/darknet53.conv.74`

5. 修改cfg/voc.data
```
classes= 10  #classes为训练样本集的类别总数
train  = /home/user/darknet/2007_train.txt  #train的路径为训练样本集所在的路径
valid  = /home/user/darknet/2007_val.txt  #valid的路径为验证样本集所在的路径
names = data/voc.names  #names的路径为data/voc.names文件所在的路径
backup = backup
```
6. 在darknet文件夹下面新建文件夹backup
7. 修改data/voc.name为样本集的标签名
8. 修改cfg/yolov3-voc.cfg

关于cfg修改，以10类目标检测为例，主要有以下几处调整（蓝色标出）：
```
[net]
# Testing            ### 测试模式                                          
# batch=1
# subdivisions=1
# Training           ### 训练模式，每次前向的图片数目 = batch/subdivisions 
batch=64
subdivisions=16
width=416            ### 网络的输入宽、高、通道数
height=416
channels=3
momentum=0.9         ### 动量 
decay=0.0005         ### 权重衰减
angle=0
saturation = 1.5     ### 饱和度
exposure = 1.5       ### 曝光度 
hue=.1               ### 色调
learning_rate=0.001  ### 学习率 
burn_in=1000         ### 学习率控制的参数
max_batches = 50200  ### 迭代次数
policy=steps         ### 学习率策略 
steps=40000,45000    ### 学习率变动步长 
scales=.1,.1         ### 学习率变动因子



[convolutional]
batch_normalize=1    ### BN
filters=32           ### 卷积核数目
size=3               ### 卷积核尺寸
stride=1             ### 卷积核步长
pad=1                ### pad
activation=leaky     ### 激活函数

......

[convolutional]
size=1
stride=1
pad=1
filters=45  #3*(10+4+1)
activation=linear

[yolo]
mask = 6,7,8
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=10  #类别
num=9
jitter=.3
ignore_thresh = .5
truth_thresh = 1
random=0  #1，如果显存很小，将random设置为0，关闭多尺度训练；
......

[convolutional]
size=1
stride=1
pad=1
filters=45  #3*(10+4+1)
activation=linear

[yolo]
mask = 3,4,5
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=10  #类别
num=9
jitter=.3
ignore_thresh = .5
truth_thresh = 1
random=0  #1，如果显存很小，将random设置为0，关闭多尺度训练；
......

[convolutional]
size=1
stride=1
pad=1
filters=45  #3*(10+4+1)
activation=linear

[yolo]
mask = 0,1,2
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=10  #类别
num=9
jitter=.3  # 数据扩充的抖动操作
ignore_thresh = .5  #文章中的阈值1
truth_thresh = 1  #文章中的阈值2
random=0  #1，如果显存很小，将random设置为0，关闭多尺度训练；
```
---

9. 开始训练
`./darknet detector train cfg/voc.data cfg/yolov3-voc.cfg darknet53.conv.74 -gpus 0,1`

10. 识别
将训练得到的weights文件拷贝到darknet/weights文件夹下面
`./darknet detect cfg/yolov3-voc.cfg weights/yolov3.weights data/dog.jpg`
--------------------- 
