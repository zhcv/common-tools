import xml.etree.ElementTree as ET
import pickle
import os
from tqdm import tqdm

sets = [('2012', 'train'), ('2012', 'val'), ('2007', 'train'), ('2007', 'val'),
        ('2007_test', 'test')]

classes = ['person']


def convert(size, box):
    # bbox coordinate normalize
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = (box[0] + box[1]) / 2.0 - 1
    y = (box[2] + box[3]) / 2.0 - 1
    w = box[1] - box[0]
    h = box[3] - box[2]

    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return (x, y, w, h)


def convert_annotation(year, image_id):
    in_file = open('VOCdevkit/VOC%s/Annotations/%s.xml' % (year, image_id))
    out_file = open('VOCdevkit/VOC%s/labels/%s.txt' % (year, image_id), 'w')
    # Parse xml file
    tree = ET.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult) == 1:
            continue

        cls_id = classes.index(cls)
        xmlbox = obj.find('bndbox')
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text),
             float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))
        bb = convert((w, h), b)
        out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')



wd = os.getcwd()
for year, image_set in tqdm(sets):
    if not os.path.exists('VOCdevkit/VOC%s/labels' % year):
        os.makedirs('VOCdevkit/VOC%s/labels' % year)
    image_id_and_flags = open('VOCdevkit/VOC%s/ImageSets/Main/person_%s.txt' % 
        (year, image_set)).read().strip().split()
    num = len(image_ids_and_flag)[2*i]
    list_file = open('%s_%s.txt' % (year, image_set), 'w')
    for i in range(num):
        image_id = image_ids_and_flag[2 * i]
        flag = image_ids_and_flag[2*i + 1]
        if flag == '1':
            list_file.write('%s/VOCdevkit/VOC%s/JPEGImages/%s.jpg\n' % (wd, year, image_id))
    list_file.close()

os.system("cat 2007_train.txt 2007_val.txt 2012_train.txt 2012_val.txt > train_person.txt")
os.system("cat 2007_train.txt 2007_val.txt 2007_test_test.txt 2012_train.txt 2012_val.txt > train_person.all.txt")
