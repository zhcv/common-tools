import tensorflow as tf

# Define Graph
x = tf.placehold(tf.float32, shape=None, name='x')
y = tf.get_variable('y', initializer=10.0)
z = tf.log(x+y, name='z')


with tf.Session() as sess:
    sess.run(tf.global_variablesinitializer())

    # some training codes ......
    # xxxxxxxxxxxxxxxxxxx ......

    # show nodes into graph
    print([n.name for n in sess.graph.as_graph_def().node])

    frozen_graph_def = tf.graph_util.convert_variables_to_constants(
        sess,
        sess.graph,
        output_node_names=['z'])

    # sava graph pb file
    with open('model.pb', 'wb') as f:
        f.write(frozen_graph_def.SerializeToString())
