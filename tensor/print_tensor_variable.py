r"Export tensorflow Graph tensor variable"
import tensorflow as tf
import sys

model_dir = 'snapshots'

state = tf.train.get_checkpoint_state(model_dir)
try:
    checkpoint_file = state.all_model_checkpoint_paths[-1]
except IndexError:
    print "Cann't find checkpoint"
    sys.exit(1)

metagraph_file = checkpoint_file + '.meta'

with tf.Session() as sess:
    saver = tf.train.import_meta_graph(metagraph_file, checkpoint_file)
    graph = tf.get_default_graph()
    tensor_vars = graph.get_operations()
    restore_var = [v for v in tf.global_variables()]

    for v in tensor_vars:
        print v
