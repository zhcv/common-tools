import tensorflow as tf

def make_bottleneck_block(inputs, expantion, depth, kernel=(3, 3)):
    """Construct a bottleneck block from a block definition.
    There are three parts in a bottleneck block:
    1. 1x1 pointwise convolution with ReLU6, expanding channels to 'input_channel * expantion'
    2. 3x3 depthwise convolution with ReLU6
    3. 1x1 pointwise convolution
    """
    # The depth of the block depends on the input depth and the expantion rate.
    input_depth = inputs.get_shape().as_list()[-1]
    block_depth = input_depth * expantion

    # First do 1x1 pointwise convolution.
    block1 = tf.layers.conv2d(
        inputs=inputs,
        filters=block_depth,
        kernel_size=[1, 1],
        padding='same',
        activation=tf.nn.relu6
    )

    # Second, depthwise convolution.
    depthwise_kernel = tf.Variable(
        tf.truncated_normal(shape=[kernel[0], kernel[1], block_depth, 1], stddev=0.1))

    block2 = tf.nn.depthwise_conv2d_native(
        input=block1,
        filter=depthwise_kernel,
        strides=[1, 1, 1, 1],
        padding='SAME'
    )

    block2 = tf.nn.relu6(features=block2)

    # Third, pointwise convolution.
    block3 = tf.layers.conv2d(
        inputs=block2,
        filters=depth,
        kernel_size=[1, 1],
        padding='same'
    )

    block3 = tf.add_n([inputs, block3])

    return block3
