#!/usr/bin/env bash


export TF_NEED_CUDA=1

# bazel build --config=opt --config=cuda //tensorflow/tools/pip_package:build_pip_package
bazel build -c opt --config=cuda //tensorflow/tools/pip_package:build_pip_package

./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg

sudo pip install /tmp/tensorflow_pkg/tensorflow-version-cp27-cp27mu-linux_x86_64.whl
