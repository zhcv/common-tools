# mulity-label classification tasks loss func


If you are using keras, just put sigmoids on your output layer and 
`binary_crossentropy` on your cost function.

If you are using tensorflow, then can use `tf.nn.sigmoid_cross_entropy_with_logits` or `tf.losses.sigmoid_cross_entropy`. But for my case this direct loss function was not converging. 
So I ended up using explicit sigmoid cross entropy loss 

`loss = (y⋅ln(sigmoid(logits))+(1−y)⋅ln(1−sigmoid(logits)))`

. You can make your own like in this Example

Sigmoid, unlike softmax don't give probability distribution around nclasses
as output, but independent probabilities.
