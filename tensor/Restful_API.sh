#!/usr/bin/env bash

<<EOF
mkdir -p /tmp/tfserving
cd /tmp/tfserving

git clone --depth=1 https://github.com/tensorflow/serving
EOF

tensorflow_model_server --rest_api_port=8501 \
   --model_name=half_plus_three \
   --model_base_path=$(pwd)/tensorflow_serving/servables/tensorflow/testdata/saved_model_half_plus_three/


# Make REST API calls to ModelServer
# curl http://localhost:8501/v1/models/half_plus_three

# curl -d '{"instances": [1.0,2.0,5.0]}' -X POST http://localhost:8501/v1/models/half_plus_three:predict

# curl -d '{"signature_name": "tensorflow/serving/regress", "examples": [{"x": 1.0}, {"x": 2.0}]}' \
# -X POST http://localhost:8501/v1/models/half_plus_three:regress 


# curl -i -d '{"instances": [1.0,5.0]}' -X POST http://localhost:8501/v1/models/half:predict
