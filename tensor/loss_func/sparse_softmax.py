# -*- coding: utf-8 -*-

import tensorflow as tf
 
# 假设只有三个类，分别编号0,1,2，labels就可以直接输入下面的向量，不用转换与logits一致的维度
labels = [0,1,2]
 
logits = [[2,0.5,1],
          [0.1,1,3],
          [3.1,4,2]]
 
logits_scaled = tf.nn.softmax(logits)
result = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=labels, logits=logits)
 
with tf.Session() as sess:
    print(sess.run(result))
