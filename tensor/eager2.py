import tensorflow as tf
tf.enable_eager_execution()

from tensorflow.contrib import autograph

def fibonacci(n):
    a, b = 0, 1
    for i in xrange(n):
        a, b = b, a+b
    return b

tf_fibonacci = autograph.to_graph(fibonacci)

graph = tf.Graph()
with graph.as_default():
    res = tf_fibonacci(tf.constant(6))

with tf.Session(graph=graph) as sess:
    print res.eval()


'''
import tensorflow as tf

tf.enable_eager_execution()

"""
x = tf.constant(0.)
y = tf.constant(1.)

for i in range(50):
    x += y
    y /= 2.

print x.numpy()
"""

@tf.contrib.eager.defun
def square_sum(x, y):
    return tf.square(x + y)


print square_sum(2., 3.).numpy()
'''
