from __future__ import print_function
import tensorflow.contrib.slim as slim



@slim.add_arg_scope
def fn(name, add_arg, hel):
    print("name:", name)
    print("hel", hel)
    print("add_arg:", add_arg)

with slim.arg_scope([fn], add_arg='this is add', hel="hello"):
    fn('test')



# slim.arg_scope(*fns, **kwargs)
# *fns, scope functions by default parameters in **kwargs.
