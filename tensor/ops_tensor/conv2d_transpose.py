import tensorflow as tf

# convolution transpose
x = tf.reshape(tf.constant([[1, 2], [4, 5]], dtype=tf.float32), [1, 2, 2, 1])

kernel = tf.reshape(tf.constant([[1, 2, 3],
                                 [4, 5, 6],
                                 [7, 8, 9]], dtype=tf.float32), shape=[3, 3, 1, 1])

conv_transpose = tf.nn.conv2d_transpose(x, kernel, output_shape=[1, 4, 4, 1],
                                        strides=[1, 1, 1, 1],
                                        padding='VALID')

# equality to convolution
x2 = tf.reshape(tf.constant([[0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0],
                             [0, 0, 1, 2, 0, 0],
                             [0, 0, 4, 5, 0, 0],
                             [0, 0, 0, 0, 0, 0],
                             [0, 0, 0, 0, 0, 0]],dtype=tf.float32), [1, 6, 6, 1])

kernel2  = tf.reshape(tf.constant([[9,8,7],
                                   [6,5,4],
                                   [3,2,1]],dtype=tf.float32), [3, 3, 1, 1])

conv = tf.nn.conv2d(x2,kernel2,strides=[1,1,1,1],padding='VALID')



with tf.Session() as sess:
    print sess.run(tf.squeeze(x))
    print "conv_transpose"
    print sess.run(tf.squeeze(conv_transpose))
    print "\ndirectly convolution"
    print(sess.run(tf.squeeze(x2)))
    print(sess.run(tf.squeeze(kernel2)))
    print(sess.run(tf.squeeze(conv)))
