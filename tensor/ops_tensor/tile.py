import tensorflow as tf


a = tf.constant([[1, 2],
                 [3, 4],
                 [5, 6]], dtype=tf.float32)

multiple_a = tf.tile(a, [2, 3])

with tf.Session() as sess:
    print sess.run(multiple_a)

    print a.get_shape().as_list()
    print "multiple_a shape"
    print multiple_a.get_shape().as_list()
