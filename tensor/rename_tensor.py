import tensorflow as tf

# it seems it works also without tf.name_scope only with z = tf.identity(z, name="z_name"). 
# If you run additionally z = tf.identity(z, name="z_name_new") 
# then you can access the same tensor using both names: 
tf.get_default_graph().get_tensor_by_name("z_name:0")       # or next 
tf.get_default_graph().get_tensor_by_name("z_name_new:0")

tf.identify
tf.assign

x = tf.placeholder(tf.float32, [None, 2400], name="x")
y = tf.placeholder(tf.float32, [None, 2400], name="y")
a = tf.add(x, y, name="a")

# Is there an efficient way to refer a with a different name, like out?
# I am thinking a dummy operation like

b = tf.add(a, 0, name="out")

# I am asking this because I am trying different network architectures,
# and regardless of architectures, I would like to access the output 
# tensor with a consistent name like

tf.get_default_graph().get_tensor_by_name("out:0")


##
print tf.identity(a, name='out')
# graph_def.node[<index of the op you want to change>].name = 'out'
# // he index is usually -1 because final output tensors are in the end of the graph_def // 
