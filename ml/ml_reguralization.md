# 机器学习中使用正则化 L1范数和L2范数的原因

在机器学习中，损失函数后面通常都会添加一个额外项，用于防止过拟合提高算法的泛化能力，称为正则化惩罚（regularization penalty）。

为什么要添加这一项呢？大家可以想一下，原来没有这一项时，损失函数L

的公式可以表示如下：

\[ L = \frac{1}{n} \sum_{i=1}^n L_i  \]
L=1n∑ni=1Li

上面这个公式有一个问题，假设有一个数据集和一个权重集W
能够正确地分类每个数据（即所有的边界都满足，对于所有的i都有Li=0），那么这个W是不唯一的，可能有很多相似的W都能正确地分类所有的数据。一个简单的例子：如果W能够正确分类所有数据，即对于每个数据，损失值都是0。那么当λ>1时，任何数乘λW

都能使得损失值为0，因为这个变化将所有分值的大小都均等地扩大了，所以它们之间的绝对差值也扩大了。

换句话说，我们希望能向某些特定的权重W

添加一些偏好，对其他权重则不添加，以此来消除模糊性。这一点是能够实现的，方法是向损失函数增加一个正则化惩罚（regularization penalty），最常用的正则化惩罚就是L1范数和L2范数。

L1范数是对所有权重W

的绝对值求和：

R(W)=λn∑i,j|Wi,j|

L2范数是对所有权重W

进行逐元素的平方求和：

R(W)=λ2n∑i,jW2i,j

故损失函数的完整表达公式为：

L=1n∑ni=1Li+R(W)

关于L1范数和L2范数的具体细节可以参考下面两篇文章：

正则化方法：L1和L2 regularization、数据集扩增、dropout
![](https://blog.csdn.net/u012162613/article/details/44261657)

机器学习中正则化项L1和L2的直观理解
![](https://blog.csdn.net/jinping_shi/article/details/52433975)


避免过拟合的方法有多种: 
    early stopping, data augmentation, Regularization L1, L2(L2 regularization also named weight decay), 
dropout.

L2 regularization

L2 正则化就是在代价函数后面再加上一个正则项:
```\[ C = C_0 + \frac{\lamda}{2n} \sum _w w^2 \] ```

C0代表原始的代价函数，后面那一项就是L2正则化项，它是这样来的：所有参数w的平方的和，
除以训练集的样本大小n。λ就是正则项系数，权衡正则项与C0项的比重。
另外还有一个系数1/2，1/2经常会看到，主要是为了后面求导的结果方便，
后面那一项求导会产生一个2，与1/2相乘刚好凑整。

L2正则化项是怎么避免overfitting的呢？我们推导一下看看，先求导：

```
\[ \frac{\parts C}{\parts w} = \frac{parts C_0}{parts w}  + \frac{\lamda}{n} w \]
```


## Dropout 训练开始时，随机删除一定比例的神经云，视它们不存在。训练网络时使用的一种技巧(trike).
