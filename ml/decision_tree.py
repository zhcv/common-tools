

def createTree(dataSet, labels) :
     classList = [example[-1] for example in dataSet]
     if classList.count(classList[0] ) == len(classList): # 数据集全部属于同一个类别
          return classList[0] #返回这个类别，并作为叶子结点
     if len(dataSet[0]) == 1:  #没有特征可以选择
          return majorityCnt(classList)  #返回数目最多的类别，并作为叶子结点
     bestFeat = chooseBestFeatureToSplit(dataSet)  #选择信息增益最大的特征A
     bestFeatLabel = labels[bestFeat]
     myTree = {bestFeatLabel : {}}#建立结点
     del (labels[bestFeat])   #特征集中去除特征A
     featValues = [example[bestFeat] for example in dataSet] #特征A下的所有可能的取值
     uniqueVals = set(featValues)#去重
     for value in uniqueVals:    #对可能取值分别建立子树
          subLabels = labels[:]
          myTree[bestFeatLabel][value] = createTree(splitDataSet(dataSet, bestFeat, value), subLabels)#递归建立子树

    return myTree
