import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.datasets import load_iris

RF = RandomForestClassifier(n_estimators=100, n_jobs=4, oob_score=True)
# ET = ExtraTreesClassifier(n_estimators=100, n_jobs=4, oob_socre=True, boostrap=True)

iris = load_iris()
x = iris.data[:, :2]
y = iris.target
RF.fit(x, y)
# ET.fit(x, y)

h = .02  # step size in the mesh

# Create color maps
cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

for weights in ['uniform', 'distance']:
    # we create an instance of Neighbours Classifer and fit the data.
    # clf = neigbors.KNeighboursClassifier(n_neighbors, weights=weights)
    # clf.fit(X, y)

    # Plot the decision boundary, for that, we will assign a color to 
    # each point in the mesh [x_min, x_max] x [y_min, y_max]
    x_min, x_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    y_min, y_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    Z = RF.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

    # Plot also the training points
    plt.scatter(x[:, 0], x[:, 1], c=y, cmap=cmap_bold,
        edgecolor='k', s=20)
    plt.xlim(xx.min(), xx.max())
    plt.title("RandomForestClassifier")

plt.show()
print "RandomForestClassifier:", RF.score(x, y)
