# How To Delete Duplicate Rows in MySQL

## query 
* SELECT * FROM `shadow` WHERE hash in (SELECT hash FROM `shadow` GROUP by hash HAVING COUNT(hash)>1)


## 单一字段重复数量
```mysql
SELECT 
    email, COUNT(email)
FROM
    contacts
GROUP BY email
HAVING COUNT(email) > 1;)
```
```
delete from tb_table where id not in (select maxid from (select max(id) as maxid from tb_table group by sample_code) b)
```

## 统计多个字段重复Example:(name, code)
```
SELECT * from (SELECT *, CONCAT(name, code) as nameAndCode from tb_table) t WHERE t.nameAndCode in 
(
    SELECT nameAndCode from (SELECT CONCAT(name,code) as nameAndCode from tb_table) tt GROUP BY nameAndCode HAVING count(nameAndCode) > 1
)
```
<!--删除多个字段重复记录-->
```
DELETE from tb_table WHERE id not in 
(
    SELECT maxid from (SELECT MAX(id) as maxid, CONCAT(name,code) as nameAndCode from tb_table GROUP BY nameAndCode) t
)
```
## 查看语法格式，
SHOW CREATE VIEW 视图名

## How To Delete Duplicate Rows in MySQL

# UPDATE table_name SET field=REPLACE(field, 'old-string', 'new-string') [WHERE Clause]

# SELECT * FROM `label_json` t1  join label_info t2 on t1.label_value_uid = t2.id
# select user_name,count(*) as count from user group by user_name having count>1;

<!--join like query-->
SELECT * FROM Table1 INNER JOIN Table2 ON Table1.col LIKE CONCAT('%', Table2.col, '%')
