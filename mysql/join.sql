USE rmis_annotation;
SELECT t4.STUDY_UID, t4.SERIES_UID, t4.IMAGE_UID, t5.JSON_VALUE1
FROM `apply_worklist` t1 
join apply_series t2 on t1.study_uid = t2.study_uid 
join label_info_list t3 on t3.series_uid = t2.series_uid 
join label_info t4 on t4.uid = t3.LABEL_ACCNUM
join label_json t5 on t5.LABEL_VALUE_UID = t4.id

where t1.apply_org = '2ffb05ef-cfa0-11e8-8976-7cd30ad3aa0c'