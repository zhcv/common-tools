# 创建自增premary key



### `To let the AUTO_INCREMENT sequence start with another value, use the following SQL statement:`
`ALTER TABLE Persons AUTO_INCREMENT=100; `

```mysql 
CREATE TABLE Persons (
    ID int NOT NULL AUTO_INCREMENT,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);
```

```
1、创建表格时添加： create table tablename(id int auto_increment primary key,...) 
2、创建表格后添加： alter table tablename add id int auto_increment primary key
3、设置主键：alter table tablename add primary key(field_name);
4、重命名表： alter table table_old_name rename table_new_name;
5、改变字段的类型：alter table tableName modify field_name field_type;
6、重命名字段：alter table tableName change old_field_name new_field_name new_field_type;
7、删除字段：alter table tableName drop column field_name;
8、增加一个新字段：alter table tableName add new_field_name field_type;   
                                  alter table tableName add new_field_name field_type not null default '0';
9、新增一个字段，默认值为0，非空，自动增长，主键：alter table tabelname add new_field_name field_type default 0 not null   auto_increment ,add primary key (new_field_name);   

创建一个表
create table pre_common_usernotes (id int(8) not null primary key auto_increment, user_id char(20) not null, order_id char(20) not null, pub_id char(20) not null, ad_name char(20) , ad_id int(8), device char(20) , system_name char(20), channel int(8), price double(16,2), point int(8), ts int(10) not null default'0', sign char(30));

创建数据库并设置数据库默认字段编码格式
create database database_name default charset utf8 collate utf8_unicode_ci;

设置auto_increment字段的最小值
ALETER TABLE table_name AUTO_INCREMENT=100000

## remove primary key
ALTER TABLE `annotation_segment` DROP PRIMARY KEY

```

```mysql 
UPDATE `stage_1_train_labels` t1 JOIN `stage_1_detailed_class_info` t2  on t1.id = t2.id
set t1.category = t2.class
WHERE t1.id = t2.id
```
