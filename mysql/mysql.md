# CREATE TABLE img_all

[create-view]:
```
CREATE VIEW img_all as SELECT check_num, reasion, bodypart, studyUID, seriesUID, objectUID FROM `refuse_image` UNION SELECT check_num, 
reasion, bodypart, studyUID, seriesUID, objectUID FROM `refuse_img`;
```

# 在MySQL中去掉一个表格的主键需要分2中情况：

[1]:该列（column）不光设置为主键（primary key），还有自增长 (auto_increment);
* alter table `table_name` modify/change id int, drop primary key;

[2]:如果没有设置为自增长（auto_increment），那么可以直接删除主键 (primary key);
* alter table `tabel_name` drop primary key;


[3]:唯一约束也是索引。首先使用
* SHOW INDEX FROM tbl_name

[4]:找出索引的名称。 索引的名称存储在该查询结果中的键名称列中. 然后可以使用DROP INDEX 删除索引
* DROP INDEX index_name ON tbl_name
[5]:ALTER TABLE语法：
* ALTER TABLE tbl_name DROP INDEX index_name

[www-site](http://www.runoob.com/mysql/mysql-tutorial.html)
```mysql

CREATE TABLE IF NOT EXISTS `runoob_tbl`
(
    `runoob_id` INT UNSIGNED AUTO_INCREMENT,
    `run_oob_title` VARCHAR(100) NOT NULL,
    `runoob_author` VARCHAR(40) NOT NULL,
    `submission_date` DATE,
    PRIMARY KEY (`runoob_id`)
);
```

[revise-charset]:
```ALTER TABLE `runoob_tbl` DEFAULT CHARSET=utf8 COLLATE utf8_general_ci;
```
[modify-field-name]:
```ALTER TABLE `runoob_tbl` CHANGE `runoob_author` `runoob_author` VARCHAR(32) character SET utf8 collate utf8_general_ci NOT NULL;
```
[delete-table]:
``` DROP TABLE table_name;
```
[insert-into-sql]:
```mysql
INSERT INTO table_name ( field1, field2,...fieldN )
                       VALUES
                       ( value1, value2,...valueN );
```
[select-sql]:
```
SELECT column_name,column_name
FROM table_name
[WHERE Clause]
[LIMIT N][ OFFSET M]
```
SELECT field1, field2,...fieldN FROM table_name1, table_name2...
[WHERE condition1 [AND [OR]] condition2.....

# WHERE OPERATOR
=
<>, !=
>
<
>=
<=
```

# String comparisons for MySQL's WHERE clause are case-insensitive. You can use the BINARY keyword to set the string 
# comparison of the WHERE clause to be case-sensitive.
```
[case-insensitive]:
```SELECT * from runoob_tbl WHERE BINARY runoob_author='runoob.com';
```

[update]:
```
UPDATE table_name SET field1=new-value1, field2=new-value2
[WHERE Clause]
```
[delete]: 
```DELETE FROM table_name [WHERE Clause];
```

[like]: 
<!--like LIKE is usually used with %, similar to a metacharacter search-->
```
SELECT field1, field2, ...fieldN
FROM table_name
WHERE field1 LIKE condition1 [AND [OR]] field2 = 'somevalue'
```

[union]:
```
SELECT expression1, expression2, ... expression_n
FROM tables
[WHERE conditions]
UNION [ALL | DISTINCT]
SELECT expression1, expression2, ... expression_n
FROM tables
[WHERE conditions];
```



























































