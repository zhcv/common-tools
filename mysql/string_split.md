# SUBSTRING_INDEX FUNCTION


```mysql
SELECT 
    IMAGE_UID, 
    SUBSTRING_INDEX(bbox, ' ', 1 ) as x, 
    SUBSTRING_INDEX(SUBSTRING_INDEX(bbox, ' ', 2 ), ' ', -1) as y, 
    SUBSTRING_INDEX(SUBSTRING_INDEX(bbox, ' ', 3), ' ', -1) as width, 
    SUBSTRING_INDEX(bbox, ' ', -1) as height 
FROM 
    `nih_rsna_2` 
WHERE 
    bbox <> '' 

```

```
CREATE TABLE tablename (
  id INT,
  name VARCHAR(20));

INSERT INTO tablename VALUES
(1, 'a,b,c'),
(2, 'd');

CREATE TABLE numbers (
  n INT PRIMARY KEY);

INSERT INTO numbers VALUES (1),(2),(3),(4),(5),(6);
```

```
SELECT
  tablename.id,
  SUBSTRING_INDEX(SUBSTRING_INDEX(tablename.name, ',', numbers.n), ',', -1) name
FROM
  numbers INNER JOIN tablename
  ON CHAR_LENGTH(tablename.name)
     -CHAR_LENGTH(REPLACE(tablename.name, ',', ''))>=numbers.n-1
ORDER BY
  id, n
```
```
select
  tablename.id,
  SUBSTRING_INDEX(SUBSTRING_INDEX(tablename.name, ',', numbers.n), ',', -1) name
from
  (select 1 n union all
   select 2 union all select 3 union all
   select 4 union all select 5) numbers INNER JOIN tablename
  on CHAR_LENGTH(tablename.name)
     -CHAR_LENGTH(REPLACE(tablename.name, ',', ''))>=numbers.n-1
order by
  id, n
```
