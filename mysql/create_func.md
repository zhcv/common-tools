创建函数的语法
=============

创建一个自定义函数的语法：
```
CREATE [AGGREGATE] FUNCTION function_name
RETURNS {STRING|INTEGER|REAL|DECIMAL}
```

## split fun
```
CREATE FUNCTION SPLIT_STR(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
)
RETURNS VARCHAR(255)
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '');

```

```usage:
>>> SELECT SPLIT_STR(string, delimiter, position)

return:
SELECT SPLIT_STR('a|bb|ccc|dd', '|', 3) as third;

+-------+
| third |
+-------+
| ccc   |
+-------+
```

## update field
`f not set @@global.sql_mode=''`, will throw `Error Code: 1406. Data too long for column`
```
set @@global.sql_mode='';
UPDATE `rep_image` SET OBJECT_UID = SPLIT_STR(IMG_PAGE, '=', 4);
```
