# 直接使用mysql导出csv方法

<!--
我们可以使用 into outfile, fields terminated by, optionally enclosed by, line terminated by语句实现导出csv

语句的格式与作用

into outfile ‘导出的目录和文件名'
指定导出的目录和文件名

fields terminated by ‘字段间分隔符'
定义字段间的分隔符

optionally enclosed by ‘字段包围符'
定义包围字段的字符（数值型字段无效）

lines terminated by ‘行间分隔符'
定义每行的分隔符 
-->
mysql -u root
use test;
select * from table into outfile '/tmp/table.csv' fields terminated by ',' optionally enclosed by '"' lines terminated by '\r\n';
