#!/usr/bin/env bash

<<COMMENT
mongo dds-uf6593804ebd5d34-pub.mongodb.rds.aliyuncs.com:3717/admin

db.auth('root', 'Jf#12345')

use lesionmarker-v5

db.orders.aggregate([{$match:{studyOrgId:'79ZJAfmRhm4DZpFKf', reviewerId: '69jW7cYd88Ebpkxv3'}}, \
	{$lookup:{from: 'labels', localField: '_id', foreignField: 'orderId', as:'labels'}},{$out:'lwx'}])
COMMENT


mongoexport --host dds-uf6593804ebd5d34-pub.mongodb.rds.aliyuncs.com \
	    --port 3717 \
	    --username root \
	    --password Jf#12345 \
	    --db lesionmarker-v5 \
	    --collection lwx \
	    --authenticationDatabase admin  \
	    --out /home/zhp/labels.json
