MYSQL 创建索引
==============


在关系数据库中，如果有上万甚至上亿条记录，在查找记录的时候，想要获得非常快的速度，就需要使用索引。

索引是关系数据库中对某一列或多个列的值进行预排序的数据结构。通过使用索引，
可以让数据库系统不必扫描整个表，而是直接定位到符合条件的记录，这样就大大加快了查询速度。

> 创建了一个名称为`idx_score`，使用列score的索引
`ALTER TABLE students ADD INDEX idx_score( score )```

> 索引名称是任意的，索引如果有多列，可以在括号里依次写上，例如:
```ALTER TABLE students ADD INDEX idx_name_score (name, score);```

> 唯一索引, 通过UNIQUE关键字我们就添加了一个唯一索引
```ALTER TABLE students ADD UNIQUE INDEX uni_name (name);```

> 只对某一列添加一个唯一约束而不创建唯一索引:
  name列没有索引，但仍然具有唯一性保证。
```
ALTER TABLE students
ADD CONSTRAINT uni_name UNIQUE (name);
```


> mysql 分页查询， 分页实际上就是从结果集中“截取”出第M~N条记录。这个查询可以
  通过`LIMIT <M> OFFSET <N>`子句实现
```
SELECT id, name, gender, score
FROM students
ORDER BY score DESC
LIMIT 3 OFFSET 0;
```
在MySQL中，LIMIT 15 OFFSET 30还可以简写成LIMIT 30, 15
```

# 去掉索引
```ALTER TABLE employees.titles DROP INDEX emp_no;```
