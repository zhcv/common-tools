# How to retrieve JSON data from mysql?

[source](https://stackoverflow.com/questions/15701579/how-to-retrieve-json-data-from-mysql)

* Let's say you have table (named: JSON_TABLE) like this:
 
ID   CITY        POPULATION_JSON_DATA
-------------------------------------------------------------------
 1    LONDON      {"male" : 2000, "female" : 3000, "other" : 600}
 2    NEW YORK    {"male" : 4000, "female" : 5000, "other" : 500}
-------------------------------------------------------------------
* To select each json fields:
```mysql
SELECT 
    ID, CITY,
    json_extract(POPULATION_JSON_DATA, '$.male') AS POPL_MALE,
    json_extract(POPULATION_JSON_DATA, '$.female') AS POPL_FEMALE,
    json_extract(POPULATION_JSON_DATA, '$.other') AS POPL_OTHER
FROM JSON_TABLE;
```
* which result:
-----------------------------------------------------------------
ID  CITY      POPL_MALE  POPL_FEMALE   POPL_OTHER 
-----------------------------------------------------------------
1   LONDON    2000       3000          600
2   NEW YORK  4000       5000          500
-----------------------------------------------------------------
