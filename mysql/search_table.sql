SELECT 
    table_name,
    table_schema AS dbname
FROM 
    INFORMATION_SCHEMA.TABLES
WHERE
    table_name LIKE "%image%";
