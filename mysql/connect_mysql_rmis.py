#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
from mysql import connector
reload(sys)
sys.setdefaultencoding('utf-8')

import csv
import urllib
import os
import shutil

cnx = connector.connect(host="rrrr-****.mysql.rds.aliyuncs.com", 
                           user="****", passwd="****", db="****")
cursor = cnx.cursor()

query = ("""
select
    bci.REPORT_TIME,bci.check_num,bci.REFUSE_NAME,ri.IMG_PAGE,bci.APPLY_HOSP,bci.APPLY_DOC,bci.PARTS
from
    (   
        select
            ACCESSION_NUM,
            REFUSE_NAME,
            check_num,
            REPORT_TIME,
            APPLY_HOSP,
            APPLY_DOC,
            PARTS
        from
            busin_checklist_index
        where
            STATUS_CODE <> '3555'
    ) bci LEFT JOIN rep_record rer on rer.ACCESSION_NUM = bci.accession_num
LEFT JOIN rep_image ri on ri.REP_UID = rer.REP_UID
where bci.REPORT_TIME > '2018-07-05 00:00:00'
""")
cursor.execute(query)

for (num, name, check, time, hosp, doc, parts) in cursor:
    urlHeader = 'http://****/wado?requestType=WADO&contentType=application/dicom'
    if time:
        url = urlHeader + time
        print url

cursor.close()
cnx.close()
