#!/usr/bin/mysql


mysql -u


DELETE consum_record
FROM
    consum_record,
    (
        SELECT
            min(id) id,
            user_id,
            monetary,
            consume_time
        FROM
            consum_record
        GROUP BY
            user_id,
            monetary,
            consume_time
        HAVING
            count(*) > 1
        ) t2
WHERE
    consum_record.user_id = t2.user_id
    and consum_record.monetary = t2.monetary
    and consum_record.consume_time = tf.consume_time
AND
    consume_record.id > t2.id
