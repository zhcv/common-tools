mysql 修改 添加 删除 表字段
============================

添加表的字段    alter table 表名  add  字段名  字段的类型

 例子：        alter table table1 add transactor varchar(10) not Null;

               alter table   table1 add id int unsigned not Null auto_increment primary key

               在mysql数据库中怎样在指定的一个字段后面添加一个字段：

               alter table newexample add address varchar(110) after stu_id;

修改表的字段类型   ALTER TABLE 表名 MODIFY COLUMN 字段名 字段类型定义;

> 例子：`ALTER TABLE chatter_users MODIFY COLUMN ip VARCHAR(50);`

修改表的字段名    alter table 表名 change 原字段名  新字段名  字段的类型

> 例子： `alter table student change physics physisc char(10) not null`

删除表的字段    alter table 表名 drop column 字段名

> 例子：```alter table `user_movement_log` drop column Gatewayid```

调整表的顺序：  
```ALTER TABLE `user_movement_log` CHANGE `GatewayId` `GatewayId` int not null default 0 AFTER RegionID```

表的重命名   alter table 原表名 rename 现表名;

> 例子: `alter table t1 rename t2;`

删除表的数据   delete from 表名  where  （条件）    id 不是从1开始       ，truncate table 表名     id是从1 开始的

创建表的例子  
```mysql
CREATE TABLE hlh_message (

     id int(11) NOT NULL AUTO_INCREMENT COMMENT '健康表id',

      title varchar(40) NOT NULL COMMENT '健康标题',

      hlh_url text DEFAULT NULL COMMENT '图片地址',

      bewrite VARCHAR(350) NOT NULL COMMENT '描述',

      content VARCHAR(350) NOT NULL COMMENT '内容',

      type tinyint(1) NOT NULL DEFAULT '0' COMMENT '健康知识 0 健康咨询 1',

      create_time date DEFAULT NULL COMMENT '发布消息的时间',

      PRIMARY KEY (id)

  )ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT='健康表'
```

## 定义外键
```mysql
ALTER TABLE students
    ADD CONSTRAINT fk_class_id
    FOREIGN KEY (class_id)
    REFERENCES classes (id);
```

## 删除外键 ``` ALTER TABLE tableName DROP FOREIGN fk_class_id```

## 删除两个字段
```alter table id_name drop column age,drop column address;```

## 聚合查询
```
SELECT COUNT(*) FROM students;
// or 给列名设置一个别名，便于处理结果:
SELECT COUNT(*) [AS] num FROM students

SELECT COUNT(*) boys FROM students WHERE gender = 'M';
```

# 除了COUNT()函数外，SQL还提供了如下聚合函数：
SUM 	计算某一列的合计值，该列必须为数值类型
AVG 	计算某一列的平均值，该列必须为数值类型
MAX 	计算某一列的最大值
MIN 	计算某一列的最小值


每页3条记录，如何通过聚合查询获得总页数？ ```SELECT CEILING(COUNT(*) / 3) FROM students;```

### 多表查询 ```SELECT * FROM <表1> <表2>```
```
SELECT
    students.id sid,
    students.name,
    students.gender,
    students.score,
    classes.id cid,
    classes.name cname
FROM students, classes;
```

### 内连接——INNER JOIN来实现：
```
SELECT s.id, s.name, s.class_id, c.name class_name, s.gender, s.score
FROM students s
INNER JOIN classes c
ON s.class_id = c.id;
```

> RIGHT JOIN ON 
```
SELECT s.id, s.name, s.class_id, c.name class_name, s.gender, s.score
FROM students s
RIGHT OUTER JOIN classes c
ON s.class_id = c.id;
```

### JOIN SQL 
```
SELECT ... FROM tableA ??? JOIN tableB ON tableA.column1 = tableB.column2;
```

```
> 创建表使用CREATE TABLE语句，而删除表使用DROP TABLE语句：
mysql> DROP TABLE students;
Query OK, 0 rows affected (0.01 sec)

> 修改表就比较复杂。如果要给students表新增一列birth，使用：
ALTER TABLE students ADD COLUMN birth VARCHAR(10) NOT NULL;

> 要修改birth列，例如把列名改为birthday，类型改为VARCHAR(20)：
ALTER TABLE students CHANGE COLUMN birth birthday VARCHAR(20) NOT NULL;

> 要删除列，使用:
ALTER TABLE students DROP COLUMN birthday;
```

> 如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就先删除原记录，再插入新记录。此时，可以使用REPLACE语句，这样就不必先查询，再决定是否先删除再插入：
```
REPLACE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);
```

> 插入或更新
```
INSERT INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99) ON DUPLICATE KEY UPDATE name='小明', gender='F', score=99;
```

> 如果我们希望插入一条新记录（INSERT），但如果记录已经存在，就啥事也不干直接忽略，此时，可以使用INSERT IGNORE INTO ...语句：
```
INSERT IGNORE INTO students (id, class_id, name, gender, score) VALUES (1, 1, '小明', 'F', 99);
```
> 快照　复制表
```
CREATE TABLE students_of_class1 SELECT * FROM students WHERE class_id=1;
```
> 写入查询结果集
```
INSERT INTO statistics (class_id, average) SELECT class_id, AVG(score) FROM students GROUP BY class_id;
``
