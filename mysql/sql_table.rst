MySQL 删除数据表
----------------

>> 删除表内数据，```delete from table_name where conditions```

>> 清楚表内数据，保存表结构， ```truncate table table_name```

>> 完全删除表， ```drop table table_name```

>>

>>

>>


>> 插入数据 
```mysql 

INSERT INTO table_name (field1, field2, ... ) 
                    VALUES
                    (value1, value2, ... )
```
>> 插入多条数据
```
INSERT INTO table_name  (field1, field2,...fieldN)  
            VALUES  
            (valueA1,valueA2,...valueAN),
            (valueB1,valueB2,...valueBN),
            (valueC1,valueC2,...valueCN)......;
```

>> 如果所有的列都要添加数据可以不规定列进行添加数据:
```
INSERT INTO table_name VALUES (value1, value2, value3)
```

>> 查询数据
```mysql
 SELECT column_name,column_name
 FROM table_name
 [WHERE Clause]
 [LIMIT N][ OFFSET M]


 查询语句中你可以使用一个或者多个表，表之间使用逗号(,)分割，并使用WHERE语句来设定查询条件。
 SELECT 命令可以读取一条或者多条记录。
 你可以使用星号（*）来代替其他字段，SELECT语句会返回表的所有字段数据
 你可以使用 WHERE 语句来包含任何条件。
 你可以使用 LIMIT 属性来设定返回的记录数。
 你可以通过OFFSET指定SELECT语句开始查询的数据偏移量。默认情况下偏移量为0。
 limit N,M : 相当于 limit M offset N , 从第 N 条记录开始, 返回 M 条记录 

SELECT * FROM table LIMIT 5,10; // 检索记录行 6-15
SELECT * FROM table LIMIT 95,-1; // 检索记录行 96-last.
SELECT * FROM table LIMIT 5; //检索前 5 个记录行
```


>> 2.2 子查询的分页方式
```
SELECT * FROM articles WHERE  id >=
 (SELECT id FROM articles  WHERE category_id = 123 ORDER BY id LIMIT 10000, 1) LIMIT 10
```
>> 2.3 JOIN分页方式
```
SELECT * FROM `content` AS t1
JOIN (SELECT id FROM `content` ORDER BY id desc LIMIT ".($page-1)*$pagesize.", 1) AS t2
WHERE t1.id <= t2.id ORDER BY t1.id desc LIMIT $pagesize;
```

>> JOIN ON 语句
```select * from user left join orders on user.id = orders.user_id```

```select user.username,orders.id,count(*) from user right join orders on user.id = orders.user_id GROUP BY user.id```

>> JOIN ON 右关联
```select user.* ,orders.number from user right join orders on user.id = orders.user_id;```

>> 复杂查询sql
```mysql
SELECT user.id,user.username,user.address,orders.number,sum(items.price) 
    FROM user right JOIN orders ON user.id = orders.user_id 
    right JOIN orderdetail ON orders.id = orderdetail.orders_id 
    left JOIN items ON items.id = orderdetail.items_id 
    GROUP BY orders.number;
```

>> WHERE 子句
```
SELECT field1, field2,...fieldN FROM table_name1, table_name2...
[WHERE condition1 [AND [OR]] condition2.....

    查询语句中你可以使用一个或者多个表，表之间使用逗号, 分割，并使用WHERE语句来设定查询条件。
    你可以在 WHERE 子句中指定任何条件。
    你可以使用 AND 或者 OR 指定一个或多个条件。
    WHERE 子句也可以运用于 SQL 的 DELETE 或者 UPDATE 命令。
    WHERE 子句类似于程序语言中的 if 条件，根据 MySQL 表中的字段值来读取指定的数据。
```

>> MySQL UPDATE 查询 
```
UPDATE table_name SET field1=new-value1, field2=new-value2
[WHERE Clause]

    你可以同时更新一个或多个字段。
    你可以在 WHERE 子句中指定任何条件。
    你可以在一个单独表中同时更新数据。
```

>> UPDATE替换某个字段中的某个字符
```
UPDATE table_name SET field=REPLACE(field, 'old-string', 'new-string')
[WHERE Clause]
```

>> DELETE 语句
```
DELETE FROM table_name [WHERE Clause]
```
>> delete，drop，truncate 都有删除表的作用，区别:
* delete 和 truncate 仅仅删除表数据，drop 连表数据和表结构一起删除，打个比方，
  delete 是单杀，truncate 是团灭，drop 是把电脑摔了。
* delete 是 DML 语句，操作完以后如果没有不想提交事务还可以回滚，truncate 和 
  drop 是 DDL 语句，操作完马上生效，不能回滚，打个比方，delete 是发微信说分手，
  后悔还可以撤回，truncate 和 drop 是直接扇耳光说滚，不能反悔。
* 执行的速度上，drop>truncate>delete，打个比方，drop 是神舟火箭，truncate
  是和谐号动车，delete 是自行车。

>>


>> MySQL LIKE 子句
```
SELECT field1, field2,...fieldN
FROM table_name
WHERE field1 LIKE condition1 [AND [OR]] filed2 = 'somevalue'

    你可以在 WHERE 子句中指定任何条件。
    你可以在 WHERE 子句中使用LIKE子句。
    你可以使用LIKE子句代替等号 =。
    LIKE 通常与 % 一同使用，类似于一个元字符的搜索。
    你可以使用 AND 或者 OR 指定一个或多个条件。
    你可以在 DELETE 或 UPDATE 命令中使用 WHERE...LIKE 子句来指定条件。

>>> NOTE: 
'%a'     //以a结尾的数据
'a%'     //以a开头的数据
'%a%'    //含有a的数据
'_a_'    //三位且中间字母是a的
'_a'     //两位且结尾字母是a的
'a_'     //两位且开头字母是a的
```

>> MySQL UNION 操作符
```
SELECT expression1, expression2, ... expression_n
FROM tables
[WHERE conditions]
UNION [ALL | DISTINCT]
SELECT expression1, expression2, ... expression_n
FROM tables
[WHERE conditions];

[parameters]:
  expression1, expression2, ... expression_n: 要检索的列。

  tables: 要检索的数据表。

  WHERE conditions: 可选， 检索条件。

  DISTINCT: 可选，删除结果集中重复的数据。默认情况下 UNION 操作符已经
  删除了重复数据，所以 DISTINCT 修饰符对结果没啥影响。

  ALL: 可选，返回所有结果集，包含重复数据。

>>> 注意union前后字段须一致 
>>> UNION 语句：用于将不同表中相同列中查询的数据展示出来;（不包括重复数据）
>>> UNION ALL 语句：用于将不同表中相同列中查询的数据展示出来;（包括重复数据）
```

>> MySQL 排序
```
SELECT field1, field2,...fieldN table_name1, table_name2...
ORDER BY field1, [field2...] [ASC [DESC]]

 你可以使用任何字段来作为排序的条件，从而返回排序后的查询结果。
 你可以设定多个字段来排序。
 你可以使用 ASC 或 DESC 关键字来设置查询结果是按升序或降序排列。 默认情况下，它是按升序排列。
 你可以添加 WHERE...LIKE 子句来设置条件。
```

>> MySQL GROUP BY 语句 
```
SELECT column_name, function(column_name)
FROM table_name
WHERE column_name operator value
GROUP BY column_name;

>>>
>>> SELECT name, SUM(singin) as singin_count FROM  employee_tbl GROUP BY name WITH ROLLUP;
```
>> NOTE GROUP BY
* group by 可以实现一个最简单的去重查询，假设想看下有哪些员工，除了用 distinct,还可以用：
```SELECT name FROM employee_tbl GROUP BY name;```

* 分组后的条件使用 HAVING 来限定，WHERE 是对原始数据进行条件限制。几个关键字的
  使用顺序为 where 、group by 、having、order by ，例如：
```
SELECT name ,sum(*)  
    FROM 
        employee_tbl 
    WHERE 
        id<>1 GROUP BY name  HAVING sum(*)>5 ORDER BY sum(*) DESC;
```

>> MySQL 正则表达式
```
查找name字段中以'st'为开头的所有数据：
```mysql> SELECT name FROM person_tbl WHERE name REGEXP '^st';```

查找name字段中以'ok'为结尾的所有数据：
```mysql> SELECT name FROM person_tbl WHERE name REGEXP 'ok$';```

查找name字段中包含'mar'字符串的所有数据：
```mysql> SELECT name FROM person_tbl WHERE name REGEXP 'mar';```

查找name字段中以元音字符开头或以'ok'字符串结尾的所有数据：
```mysql> SELECT name FROM person_tbl WHERE name REGEXP '^[aeiou]|ok$';```
```

>> MySQL 排序
```
SELECT field1, field2,...fieldN table_name1, table_name2...
ORDER BY field1, [field2...] [ASC [DESC]]

 你可以使用任何字段来作为排序的条件，从而返回排序后的查询结果。
 你可以设定多个字段来排序。
 你可以使用 ASC 或 DESC 关键字来设置查询结果是按升序或降序排列。 默认情况下，它是按升序排列。
 你可以添加 WHERE...LIKE 子句来设置条件。

>>> NOTE:
如果字符集采用的是 gbk(汉字编码字符集)，直接在查询语句后边添加 ORDER BY：

```SELECT * FROM runoob_tbl ORDER BY runoob_title;```

如果字符集采用的是 utf8(万国码)，需要先对字段进行转码然后排序：

```SELECT * FROM runoob_tbl ORDER BY CONVERT(runoob_title using gbk);```
```

>>> MySQL 分组
group by 可以实现一个最简单的去重查询，假设想看下有哪些员工，除了用 distinct,还可以用：
```SELECT name FORM employ_tbl GROUP BY name;```

分组后的条件使用 HAVING 来限定，WHERE 是对原始数据进行条件限制。几个关键字的使用顺序
为 where 、group by 、having、order by ，例如:
```mysql
SELECT name, sum(*) FROM employ_tbl WHERE id <> 1 
    GROUP BY name HAVING sum(*) > 5 
    ORDER BY sum(*) DESC;
```

>>> MySQL ALTER命令
删除、添加或修改字段
ALTER TABLE table_name DROP field;
ALTER TABLE table_name ADD field int;
SHOW COLUMNS FROM table_name;
```
>>> 指定字段位置
ALTER TABLE table_name ADD i FIRST;
ALTER TABLE talbe_name ADD i AFTER c;

>>> 修改字段类型及名称
```ALTER TABLE table_name MODIFY c CHAR(10);
ALTER TABLE table_name CHAGE i j BIGINT;
ALTER TABLE table_name CHANGE j j INT;
```

>>> ALTER TABLE 对Null 值和默认值影响
指定字段j位NOT NULL且默认值位100
``` ALTER TABLE table_name MODIFY j BIGINT NOT NULL DEFAULT 100;```

>>> 修改表名　```ALTER TABLE table_name RENAME TO table_name_new;```
```
alter其他用途：
修改存储引擎：修改为myisam
alter table tableName engine=myisam;

删除外键约束：keyName是外键别名
alter table tableName drop foreign key keyName;

修改字段的相对位置：这里name1为想要修改的字段，type1为该字段原来类型，first和after
二选一，这应该显而易见，first放在第一位，after放在name2字段后面

alter table tableName modify name1 type1 first|after name2;
```

================================================================================
>>> MySQL 索引
普通索引
创建索引

这是最基本的索引，它没有任何限制。它有以下几种创建方式：

CREATE INDEX indexName ON mytable(username(length));

如果是CHAR，VARCHAR类型，length可以小于字段实际长度；如果是BLOB和TEXT类型，必须指定 length。
修改表结构(添加索引)

ALTER table tableName ADD INDEX indexName(columnName)

创建表的时候直接指定
```mysql 
CREATE TABLE mytable(
ID INT NOT NULL,
username VARCHAR(16) NOT NULL,
INDEX [indexName] (username(length))
);
```
删除索引的语法 ```DROP INDEX [indexName] ON mytable;```

唯一索引
它与前面的普通索引类似，不同的就是：索引列的值必须唯一，但允许有空值。如果是组合索引，则列值的组合必须唯一。它有以下几种创建方式：
创建索
CREATE UNIQUE INDEX indexName ON mytable(username(length))

修改表结构
```ALTER table mytable ADD UNIQUE [indexName] (username(length))```

### 创建表的时候直接指定
```
CREATE TABLE mytable(
ID INT NOT NULL,
username VARCHAR(16) NOT NULL,
UNIQUE [indexName] (username(length))
);
```
使用ALTER 命令添加和删除索引

有四种方式来添加数据表的索引：

1.ALTER TABLE tbl_name ADD PRIMARY KEY (column_list): 该语句添加一个主键，这意味着索引值必须是唯一的，且不能为NULL。

2.ALTER TABLE tbl_name ADD UNIQUE index_name (column_list): 这条语句创建索引的值必须是唯一的（除了NULL外，NULL可能会出现多次）。
3.ALTER TABLE tbl_name ADD INDEX index_name (column_list): 添加普通索引，索引值可出现多次。
4.ALTER TABLE tbl_name ADD FULLTEXT index_name (column_list):该语句指定了索引为 FULLTEXT ，用于全文索引。

以下实例为在表中添加索引。

mysql> ALTER TABLE testalter_tbl ADD INDEX (c);

你还可以在 ALTER 命令中使用 DROP 子句来删除索引。尝试以下实例删除索引:

mysql> ALTER TABLE testalter_tbl DROP INDEX c;

### 使用 ALTER 命令添加和删除主键

主键只能作用于一个列上，添加主键索引时，你需要确保该主键默认不为空（NOT NULL）。实例如下：
mysql> ALTER TABLE testalter_tbl MODIFY i INT NOT NULL;
mysql> ALTER TABLE testalter_tbl ADD PRIMARY KEY (i);

你也可以使用 ALTER 命令删除主键：
mysql> ALTER TABLE testalter_tbl DROP PRIMARY KEY;

删除主键时只需指定PRIMARY KEY，但在删除索引时，你必须知道索引名。

### 显示索引信息

你可以使用 SHOW INDEX 命令来列出表中的相关的索引信息。可以通过添加 \G 来格式化输出信息。
mysql> SHOW INDEX FROM table_name; \G



================================================================================
>> MySQL 复制表
mysql复制表的两种方式。

2.只复制表结构到新表
```create table 新表 select * from 旧表 where 1=2```
或者```create table 新表 like 旧表```

2.复制表结构及数据到新表
```create table新表 select * from 旧表```

>> 获取服务器元数据
```
SELECT VERSION(); 	服务器版本信息
SELECT DATABASE();	当前数据库名 (或者返回空)
SELECT USER();   	当前用户名
SHOW STATUS;    	服务器状态
SHOW VARIABLES;  	服务器配置变量
```

>> MySQL 序列使用
```
重置序列
如果你删除了数据表中的多条记录，并希望对剩下数据的AUTO_INCREMENT列进行重新排列，那么你可以
通过删除自增的列，然后重新添加来实现。 不过该操作要非常小心，如果在删除的同时又有新记录添加，
有可能会出现数据混乱。操作如下所示：
```
mysql> ALTER TABLE insect DROP id;
mysql> ALTER TABLE insect
    -> ADD id INT UNSIGNED NOT NULL AUTO_INCREMENT FIRST,
    -> ADD PRIMARY KEY (id);
```
设置序列的开始值
一般情况下序列的开始值为1，但如果你需要指定一个开始值100，那我们可以通过以下语句来实现：
```
mysql> CREATE TABLE insect
    -> (
    -> id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    -> PRIMARY KEY (id),
    -> name VARCHAR(30) NOT NULL,
    -> date DATE NOT NULL,
    -> origin VARCHAR(30) NOT NULL
)engine=innodb auto_increment=100 charset=utf8;
```
或者你也可以在表创建成功后，通过以下语句来实现：

mysql> ALTER TABLE t AUTO_INCREMENT = 100;
```

>> MySQL 处理重复数据
```
统计重复数据
以下我们将统计表中 first_name 和 last_name的重复记录数：

mysql> SELECT COUNT(*) as repetitions, last_name, first_name
    -> FROM person_tbl
    -> GROUP BY last_name, first_name
    -> HAVING repetitions > 1;

以上查询语句将返回 person_tbl 表中重复的记录数。 一般情况下，查询重复的值，请执行以下操作：

    确定哪一列包含的值可能会重复。
    在列选择列表使用COUNT(*)列出的那些列。
    在GROUP BY子句中列出的列。
    HAVING子句设置重复数大于1。

过滤重复数据

如果你需要读取不重复的数据可以在 SELECT 语句中使用 DISTINCT 关键字来过滤重复数据。

mysql> SELECT DISTINCT last_name, first_name
    -> FROM person_tbl;

你也可以使用 GROUP BY 来读取数据表中不重复的数据：

mysql> SELECT last_name, first_name
    -> FROM person_tbl
    -> GROUP BY (last_name, first_name);

删除重复数据
如果你想删除数据表中的重复数据，你可以使用以下的SQL语句：
mysql> CREATE TABLE tmp SELECT last_name, first_name, sex FROM person_tbl  GROUP BY (last_name, first_name, sex);
mysql> DROP TABLE person_tbl;
mysql> ALTER TABLE tmp RENAME TO person_tbl;

当然你也可以在数据表中添加 INDEX（索引） 和 PRIMAY KEY（主键）这种简单的方法来删除表中的重复记录。方法如下：
mysql> ALTER IGNORE TABLE person_tbl
    -> ADD PRIMARY KEY (last_name, first_name);
```
