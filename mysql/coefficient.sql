SELECT 
    (avg(snr * level) - avg(snr) * avg(level)) / 
    (sqrt(avg(snr * snr) - avg(snr) * avg(snr)) * sqrt(avg(level * level) - avg(level) * avg(level))) 
    AS 
        correlation_coefficient FROM test60

