# MySQL删除没有主键的表中的重复记录，只保留一条记录

```mysql

create table tmp as (select distinct sno, sname, age, sex from s);

delete from s;

insert into s select * from tmp;

drop table tmp;
```


# mysql delete 删除重复数据只保留id最大一条记录


```
DELETE
FROM
    t_4g_phone
WHERE
    id NOT IN (
    SELECT
        id
    FROM
        (
            SELECT
                max(b.id) AS id
            FROM
                t_4g_phone b
            GROUP BY
                b.SERIAL_NUMBER
        ) b
)
```
--------------------- 
<!-- mysql 不能先select出同一表中的某些值，再update这个表(在同一语句中) 
    也就是说将select出的结果再通过中间表select一遍，这样就规避了错误。
    注意，这个问题只出现于mysql，mssql和oracle不会出现此问题

同样， limit, 也需借助中间表 tmp
