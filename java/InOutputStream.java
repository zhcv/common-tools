import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.Writer;


public class InOutputStream {
    public static void main(String ... args) throws Exception {
    // 字节流
    byteOutStream();

    // 字符流
    charOutStream();
    }
    
    public static void charOutStream() throws Exception {
        File file = new File("./test.txt");
        if(!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        // prepare output stream
        Writer out = new FileWriter(file);
        out.write("test char stream, haha");
        out.close();
    }

    public static void byteOutStream() throws Exception {
        File file = new File("./test2.txt");
        if(!file.getParentFile().exists()) { // if file's parent dir not existed
            file.getParentFile().mkdirs();   // make dirs
        }
        //2: Instance OutputStream Object
        OutputStream output = new FileOutputStream(file);

        //3: prepare for print of implemented content
        String msg = "HelloWorld";
        byte data[] = msg.getBytes();
        output.write(data);
        output.close();
    }

}
