import java.awt.Color;
import java.awt.image.BufferedImage;

import com.attilax.io.filex;
import com.attilax.json.AtiJson;
import com.jhlabs.image.GrayscaleFilter;
 

public class DefinTest {
    /**
     * gray hsv 0,0,218 HSL 表示 hue（色相）、saturation（饱和度）、lightness（亮度），
     * HSV表示 hue、saturation、value(色调) 而 HSB 表示 hue、saturation、brightness（明度）。
     *
     * @param args
     */

    public static void main(String[] args) {
        String i = "C:\\00clr\\1_gray.jpg";
        long dfns = getDefinetionsSum(i);
        String blu5 = "C:\\00clr\\2_gray.jpg";
        long dfns_blu5 = getDefinetionsSum(blu5);
        String blu9 = "C:\\00clr\\blur9_gray.jpg";
        long dfns_blu9 = getDefinetionsSum(blu9);
        System.out.println("bl0:" + dfns + ",bl5:" + dfns_blu5 + ",bl9:" + dfns_blu9);
    }

    private static long getDefinetionsSum(String i) {
        BufferedImage src = imgx.toImg(i);
        int sum = 0;
        int wid = src.getWidth();
        int h = src.getHeight();
        for (int w = 0; w < wid; w++) {
            for (int y = 0; y < h; y++) {
                int v = (int) imgx.getHsv(src, w, y).v;

                try {

                    if (y + 1 > (h-1))
                    continue;

                    int clr_next = (int) imgx.getHsv(src, w, y + 1).v;

                    int tsa = Math.abs(v - clr_next);

                    sum = sum + tsa;

                } catch (Exception e) {

                    System.out.println(e.getMessage()+" w-h:"+w+"-"+y);

                }
            }
        }
    }
    return sum;
}
