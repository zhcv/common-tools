import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class MainClass extends JFrame {

  private BufferedImage bi;

  public static void main(String[] args) {
      new MainClass().setVisible(true);
  }

  public MainClass() {
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    try {
      bi = ImageIO.read(new File("lena.jpg"));
    } catch (Exception e) {
      e.printStackTrace();
    }

    setSize(bi.getWidth(), bi.getHeight());
  }

  public void paint(Graphics g) {
    g.drawImage(bi, 0, 0, getWidth(), getHeight(), this);
  }
}

