import org.opencv.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class MainOpencv {

    public static void main(String[] args) {
        System.out.println("Welcome to Opencv " + Core.VERSION);
        System.out.println(Core.NATIVE_LIBRARY_NAME);
        Mat m = Mat.eye(3, 3, cvType.CV_8UC1);
        System.out.println("m = " + m.dump());
    } 

}
