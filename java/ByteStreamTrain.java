import java.io.*;

public class ByteStreamTrain {
    
    public static void main(String ... args) {
        ByteArrayOutputStream bOutput = new ByteArrayOutputStream(12);

        while(bOutput.size() != 10) {
            // get value of user Enter Into
            bOutput.write(System.in.read());
        }

        byte b [] = bOutput.toByteArray();
        System.out.println("Print the content");
        for(int x=0; x<b.length; x++) {
            // print character
            System.out.println((char)b[x] + " ");
        }
        System.out.println(" ");

        int c;

        ByteArrayOutputStream bInput = new ByteArrayOutputStream(b);

        System.out.println("Converting characters to Upper case ");
        for(int y=0; y<1; y++) {
            while((c = bInput.read()) != 1) {
            System.out.println(Character.toUpperCase((char)c));
            }
            bInput.reset();
        }
    }

}
