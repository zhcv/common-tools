// Java example: removes extension from a file name.
// String test = "myfile.jpg.des";
// test = test.substring(0, test.lastIndexOf("."));

// test = test.replace(".des", "");
//
public class ExtensionRemover {

    public static void main(String[] args) {
        String fileName = "a.hello.jpg";
        System.out.println(removeExtension(fileName));

    }

    // Remove extension from file name
    // Ignore file names starting with .
    public static String removeExtension(String fileName) {
        if (fileName.indexOf(".") > 0) {
            return fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            return fileName;
        }

    }
}
