import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.junit.Test;

import com.jfhealthcare.ai.config.TfServingConfig;

public class MultiThread {
    long modelVersion = -1;
    
    public static List<File> getFileList(String strPath) {
        List<File> filelist = new ArrayList<File>();
        File dir = new File(strPath);
        File[] files = dir.listFiles();

        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                if (files[i].isDirectory()) {    // 判断是否为文件夹
                    getFileList(files[i].getAbsolutePath());  // 获取文件绝对路径
                } else if (fileName.endsWith("jpg")) {
                    filelist.add(files[i]);
                } else {
                    continue;
                }
            }
        }
        return filelist;
    }

    @Test
    public void testQc() throws Exception {
        File outFile = new File("/home/zhang/Desktop/QCResult.json");
        List<File> list = getFileList("/data/qa/unqualified");
        
        Thread t = new Thread() {
            public void run() {
                Semaphore sem = new Semaphore(1);
                Image image;
                @SuppressWarnings("unchecked")
                QualityControl client = new QualityControl(TfServingConfig.host_test, TfServingConfig.port, 8501);
                for (int i = 1000; i < list.size(); i++) {
                    File file = list.get(i);
                    System.out.println(">>>>>>>>>>>>> " + file + " >>>>>> " + i);
                    image = new Image("studyxxx", "seriesxxx", "imagexxx", file);
                    try {
                        sem.acquire();
                        client.begin(image, new IResultCallback<QualityControlResult>() {

                            @Override
                            public void success(String version, String studyUid, String seriesUid, String imageUid,
                                    QualityControlResult result) {
                                JSONObject object = new JSONObject();

                                object.put("clear", result.getClearScore());
                                object.put("waste", result.getWasteScore());
                                object.put("reason", result.getReason());
                                object.put("resotion", result.getResolutionScore());
                                object.put("bodypart", result.getBodyPart());
                                object.put("constract", result.getContrast());
                                object.put("score", result.getImgScore());
                                object.put("src", file.getName());

                                System.out.println(object);

                                try {
                                    FileUtils.writeStringToFile(outFile, object.toString() + "\n", true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                              client.shutdown();

                            }

                            @Override
                            public void fail(String version, String studyUid, String seriesUid, String imageUid, String message) {
                                System.out.println("--------------------------");
                                System.out.println(version + " fail:" + message);
                                sem.release();

                            }
                        }, -1);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                System.out.println("End of predict client");
            }
         };
         t.start();
         t.join();
    }
}
