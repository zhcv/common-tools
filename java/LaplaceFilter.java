/******************************************************************************
 *  Compilation:  javac LaplaceFilter.java
 *  Execution:    java LaplaceFilter filename
 *
 *  Reads in an image from a file, and applies a Laplace filter.
 *
 *  [ -1 -1 -1 ]
 *  [ -1  8 -1 ]
 *  [ -1 -1 -1 ]
 *
 *  % java LaplaceFilter image.jpg
 *
 *
 ******************************************************************************/

import java.awt.Color;

public class LaplaceFilter {

    public static void main(String[] args) {
        Picture picture1 = new Picture(args[0]);   // original
        Picture picture2 = new Picture(args[0]);   // filtered
        int width  = picture1.width();
        int height = picture1.height();


        picture1.show();
        picture2.show();

        for (int y = 1; y < height - 1; y++) {
            for (int x = 1; x < width - 1; x++) {
                Color c00 = picture1.get(x-1, y-1);
                Color c01 = picture1.get(x-1, y);
                Color c02 = picture1.get(x-1, y+1);
                Color c10 = picture1.get(x, y-1);
                Color c11 = picture1.get(x, y);
                Color c12 = picture1.get(x, y+1);
                Color c20 = picture1.get(x+1, y-1);
                Color c21 = picture1.get(x+1, y);
                Color c22 = picture1.get(x+1, y+1);
                int r = -c00.getRed() -   c01.getRed() - c02.getRed() +
                        -c10.getRed() + 8*c11.getRed() - c12.getRed() +
                        -c20.getRed() -   c21.getRed() - c22.getRed();
                int g = -c00.getGreen() -   c01.getGreen() - c02.getGreen() +
                        -c10.getGreen() + 8*c11.getGreen() - c12.getGreen() +
                        -c20.getGreen() -   c21.getGreen() - c22.getGreen();
                int b = -c00.getBlue() -   c01.getBlue() - c02.getBlue() +
                        -c10.getBlue() + 8*c11.getBlue() - c12.getBlue() +
                        -c20.getBlue() -   c21.getBlue() - c22.getBlue();
                r = Math.min(255, Math.max(0, r));
                g = Math.min(255, Math.max(0, g));
                b = Math.min(255, Math.max(0, b));
                Color color = new Color(r, g, b);
                picture2.set(x, y, color);
            }
        }
        picture2.show();
    }
}

