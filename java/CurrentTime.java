// package com.jfhealthcare.ai.util;

import java.sql.Timestamp;

public class CurrentTime {
	public static String formatTime(long miliseconds) {
		Timestamp timestamp = new Timestamp(miliseconds);
		return timestamp.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(formatTime(System.currentTimeMillis()));
	}
}
