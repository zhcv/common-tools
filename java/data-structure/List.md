# Java中list对象的三种遍历方式

1.增强for循环
```java
for(String str : list) {//其内部实质上还是调用了迭代器遍历方式，这种循环方式还有其他限制，不建议使用。
    System.out.println(str);
}
```java
2.普通for循环
```
for( int i = 0 ; i < list.size() ; i++) {//内部不锁定，效率最高，但在多线程要考虑并发操作的问题。
    System.out.println(list.get(i));
}
```
3.迭代器遍历
```java
Iterator<String> iter = list.iterator();
while(iter.hasNext()){  //执行过程中会执行数据锁定，性能稍差，若在循环过程中要去掉某个元素只能调用iter.remove()方法。
    System.out.println(iter.next());
}
```


