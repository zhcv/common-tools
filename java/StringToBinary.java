


public class StringToBinary {

    public void toBinary() throws Exception {    
        String str = "王雪";
        char[] strChar = str.toCharArray();
        String result="";
        for(int i=0; i<strChar.length; i++) {
            result += Integer.toBinaryString(strChar[i]) + " ";
        }
        System.out.println(result);
    }
    // 将二进制字符串转换成int数组
    public int[] binStrToIntArray(String binStr) {
        char[] temp = binStr.toCharArray();
        int[] result = new int[temp.length];
        for(int i=0; i<temp.length; i++) {
            result[i] = temp[i] - 48;
        }
        return result;
    }

    //将二进转换成字符
    public char binStrToChar(String binStr) {
        int[] temp = binStrToIntArray(binStr);
        int sum = 0;
        for(int i=0; i<temp.length; i++) {
            sum += temp[temp.length - 1 - i] << i;
        }
        return (char)sum;
    }
    public void binStrToStr() throws Exception {
        String binStr = "111001110001011 1001011011101010 ";
        String[] tempStr = binStr.split(" ");
        char[] tempChar = new char[tempStr.length];
        for(int i=0; i<tempStr.length;i++) {
            tempChar[i] = binStrToChar(tempStr[i]);
        }
        
        System.out.println(Sting.valueOf(tempChar));
    }

    public static void main(String[] args) {
    
        toBinary();
        binStrToChar();
    }
}
