// package Flie类中list和listFiles方法区别;

import java.io.File;

public class TraverseFolder {

    public static void main(String[] args) {
        String src = "/home/zhang/datasets/common-tools";
        File file = new File(src);
        // list() 方法存储的是文件名
        System.out.println("list()方法遍历结果: ");
        String[] strings = file.list();
        for (String str: strings) {
            System.out.println(str);
        }
        System.out.println();
        // listFils()方法存储的是文件的完整路径，因此在遍历文件夹及子文件夹中
        // 所有文件时必须使用listFiles()方法.
        System.out.println("listFiles()方法遍历的结果: ");
        File[] files = file.listFiles();
        for (File f: files) {
            System.out.println(f);
        }
    }

}
