// 当成员变量和局部变量重名时，在方法中使用this时，表示的是该方法所在类中的成员变量。
// （this是当前对象自己)

public class Hello {
    String s = "Hello";
 
    public Hello(String s) {
       System.out.println("s = " + s);
       System.out.println("1 -> this.s = " + this.s);
       this.s = s;//把参数值赋给成员变量，成员变量的值改变
       System.out.println("2 -> this.s = " + this.s);
    }
 
    public static void main(String[] args) {
       Hello x = new Hello("HelloWorld!");
       System.out.println("s = " + x.s);//验证成员变量值的改变
    }
}
