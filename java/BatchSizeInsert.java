public static void main(String[] args) {

        List<Integer> dataList = new ArrayList<Integer>();
        for(int i = 0; i < 1015; i++)
            dataList.add(i);

        int insertLimit = 100;
        Integer size = dataList.size();
        int maxStep = (size + insertLimit - 1) / insertLimit;
        int maxIndex;
        for(int i = 0; i < maxStep; i++) {
            maxIndex = (i + 1) * insertLimit;
            maxIndex = maxIndex > size ? size : maxIndex;
            System.out.println(dataList.subList(i * insertLimit, maxIndex));
        }

}

/*******************************************************************
--------------------

作者：江湖有草
原文：https://blog.csdn.net/u012125445/article/details/78121276
*******************************************************************/
