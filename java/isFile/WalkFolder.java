import java.io.File;


public class WalkFolder {

    public static void showDirectory(File folder) {
        File[] files = folder.listFiles();
        for (File a:files) {
            System.out.println(a.getAbsolutePath());
            if (a.isDirectory()) {
                showDirectory(a);
            }
        }
    }

    public static void main(String[] args) {
        File file = new File(".");
        showDirectory(file);
    }

}
