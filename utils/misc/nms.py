#coding:utf-8
import numpy as np

def iou(xminNp,yminNp,xmaxNp,ymaxNp,areas,lastInd,beforeInd,threshold):

    #将lastInd指向的box，与之前的所有存活的box指向坐标做比较
    xminNpTmp = np.maximum(xminNp[lastInd], xminNp[beforeInd])
    yminNpTmp = np.maximum(yminNp[lastInd], yminNp[beforeInd])
    xmaxNpTmp = np.maximum(xmaxNp[lastInd], xmaxNp[beforeInd])
    ymaxNpTmp = np.maximum(ymaxNp[lastInd], ymaxNp[beforeInd])

    #计算lastInd指向的box，与存活box交集的，所有width，height
    w = np.maximum(0.0,xmaxNpTmp-xminNpTmp)
    h = np.maximum(0.0,ymaxNpTmp-yminNpTmp)
    #计算存活box与last指向box的交集面积
    inter = w*h
    iouValue = inter/(areas[beforeInd]+areas[lastInd]-inter)
    
    indexOutput = [item[0] for item in zip(beforeInd,iouValue) if item[1] <= threshold ]
    return indexOutput

def nms(boxes,threshold):
    """boxes:n by 5的矩阵，n表示box个数，每一行分别为[xmin,ymin,xmax,ymax,score]
    """
    assert isinstance(boxes,numpy.ndarray),'boxes must numpy object'
    assert boxes.shape[1] == 5, 'the column Dimension should be 5'


    xminNp = boxes[:,0]
    yminNp = boxes[:,1]
    xmaxNp = boxes[:,2]
    ymaxNp = boxes[:,3]
    scores = boxes[:,4]
    #计算每个box的面积
    areas = (xmaxNp-xminNp)*(ymaxNp-yminNp)
    #对每个box的得分按升序排序
    scoresSorted = sorted(list(enumerate(scores)),key = lambda item:item[1])
    #提取排序后数据的原索引
    index = [ item[0] for item in scoresSorted ]
    pick = []
    while index:
        #将当前index中最后一个加入pick
        lastInd = index[-1]
        pick.append(lastInd)
        #计算最后一个box与之前所有box的iou
        index = iou(xminNp,yminNp,xmaxNp,ymaxNp,areas,lastInd,index[:-1],threshold)

    return pick



if __name__ == '__main__':

    nms(boxes,threshold)
