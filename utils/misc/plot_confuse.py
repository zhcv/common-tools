# -*- coding: utf-8 -*-
# %matplotlib inline 
import itertools 
import numpy as np 
import matplotlib.pyplot as plt 
from sklearn import svm, datasets 
from sklearn.model_selection import train_test_split 
from sklearn.metrics import (f1_score,accuracy_score,recall_score,classification_report,confusion_matrix)


def plot_confusion_matrix(cm, classes, 
			  normalize=True, 
			  title='Confusion matrix', 
			  cmap=plt.cm.Blues): 
    """ This function prints and plots the confusion matrix. Normalization
	can be applied by setting `normalize=True`. 
    """ 
    if normalize: 
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis] 
        print("Normalized confusion matrix") 
    else: 
        print('Confusion matrix, without normalization') 
        print(cm) 
    fig = plt.figure()
    plt.imshow(cm, interpolation='nearest', cmap=cmap) 
    plt.title(title) 
    plt.colorbar() 
    tick_marks = np.arange(len(classes)) 
    plt.xticks(tick_marks, classes, rotation=45) 
    plt.yticks(tick_marks, classes) 
    fmt = '.2f' if normalize else 'd' 
    thresh = cm.max() / 2. 
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])): 
        if cm[i, j] < 0.005:
            continue
        plt.text(j, i, format(cm[i, j], fmt), horizontalalignment="center", 
                color="white" if cm[i, j] > thresh else "black") 
        plt.tight_layout() 
        plt.ylabel('True label') 
        plt.xlabel('Predicted label') 
    # plt.subplots_adjust(top=0, bottom=0, right = 1, left = 0, hspace = 0, wspace = 0)
    plt.margins(0, 0)
    plt.tight_layout()
    plt.show()
    # plt.savefig('nh.png', format='png', transparent=False, dpi=fig.dpi, bbox_inches='tight')


def CalculationResults(val_y,y_val_pred,simple = False, 
        target_names = ['class_-2_Not_mentioned','class_-1_Negative','class_0_Neutral','class_1_Positive']): 
    # 计算检验 
    F1_score = f1_score(val_y,y_val_pred, average='macro') 
    if simple: 
        return F1_score 
    else: 
        acc = accuracy_score(val_y,y_val_pred) 
        recall_score_ = recall_score(val_y,y_val_pred, average='macro') 
    confusion_matrix_ = confusion_matrix(val_y,y_val_pred) 
    class_report = classification_report(val_y, y_val_pred, target_names=target_names) 
    print('f1_score:',F1_score,'ACC_score:',acc,'recall:',recall_score_) 
    print('\n----class report ---:\n',class_report) 
    #print('----confusion matrix ---:\n',confusion_matrix_) # 画混淆矩阵 
    # 画混淆矩阵图 
    plt.figure() 
    plot_confusion_matrix(confusion_matrix_, classes=target_names, title='Confusion matrix, without normalization') 
    plt.show() 
    return F1_score,acc,recall_score_,confusion_matrix_,class_report 


def CalclatetionCmatrix(result):
    cm = np.zeros((24, 24), np.uint64)
    with open(result, 'r') as f:
        for l in f.readlines():
            _, gtruth, pred = l.strip().split()
            y = int(gtruth)
            x = int(pred)
            cm[y, x] += 1
    cls = [str(i) for i in range(24)]
    return cm, cls

if __name__ == '__main__':
    """
    test_file = '/home/zhang/Desktop/online_test/bodypart/val_test.txt'
    cm, classes = CalclatetionCmatrix(test_file)
    plot_confusion_matrix(cm, classes)
    """

    plot_confusion_matrix(cm=np.array([[ 1098,  1934,   807],
                                       [  604,  4392,  6233],
                                       [  162,  2362, 31760]]), 
                          normalize=False,
                          classes = ['high', 'medium', 'low'],
                          title="Confusion Matrix")


