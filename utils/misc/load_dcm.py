#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import pydicom
import traceback
from mysql import connector
from os.path import join

cnx = connector.connect(host="180.167.46.105", user="ai", passwd="hangzhou", db="ai")

cursor1 = cnx.cursor()
cursor2 = cnx.cursor()
select = 'select localPath from dcm'
update = ('INSERT INTO dcm (PatientName, PatientSex, PatientAge, PatientBirthDate,' 
           'localPath, studyUid, seriesUid, imageUid) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)')

i = 0
root_dir = '/'

cursor1.execute(select)
loadedFiles = [str(t[0]) for t in cursor1]
for root, dirs, files in os.walk(root_dir):
    for f in files:
        if not f.endswith((".dcm", ".DCM")):
            continue
        localPath = join(root, f)
        if localPath in loadedFiles:
            continue
        try:
            ds = pydicom.dcmread(localPath, force=True)
            studyUID = str(ds.StudyInstanceUID) if hasattr(ds, 'StudyInstanceUID') else ''
            seriesUID = str(ds.SeriesInstanceUID) if hasattr(ds, 'SeriesInstanceUID') else ''
            instanceUID = str(ds.SOPInstanceUID) if hasattr(ds, 'SOPInstanceUID') else ''
            patientName = ds.PatientName.decode("GBK") if hasattr(ds, 'PatientName') else ''
            patientSex = ds.PatientSex.decode("GBK") if hasattr(ds, 'PatientSex') else ''
            patientAge = ds.PatientAge if hasattr(ds, 'PatientAge') else ''
            patientBirthDate = ds.PatientBirthDate if hasattr(ds, 'PatientBirthDate') else ''
            data = (patientName, patientSex, patientAge, patientBirthDate, localPath, 
                    studyUID, seriesUID, instanceUID)
            cursor2.execute(update, data)
            i += 1
            if i % 100 == 0:
                cnx.commit()
                print i
        except Exception, e:
            print "failed to load " + f
            traceback.print_exc()

cnx.commit()
cursor1.close()
cursor2.close()
cnx.close()
