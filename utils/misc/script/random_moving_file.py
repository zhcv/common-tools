import shutil
import random

import os

root = '/data/qa2/training_data/training/1'
npath = '/data/qa2/training_data/validation/1'


src = [os.path.join(root, f) for f in os.listdir(root)]

random.seed(len(src))

slices = random.sample(src, 100)

print len(slices)

for infile in slices:
    print infile
    shutil.move(infile, npath)
