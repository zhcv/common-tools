#!/bin/bash
#判断是否有参数
if [ $# != 1 ] ;then
        echo "The parameters you enter is not correct !";
        exit -1;
fi

#循环读出URL并判断状态码
while read line
do
{
    isok=`curl -I -o /dev/null -s -w %{http_code} $line`
    if [ "$isok" = "200" ]; then
        echo $line "OK"
    else
        echo $line "no"
    fi
}
done < $1
echo "执行结束"
