import pydicom as dicom
import traceback
from mysql import connector


url_head = 'http://v2.jfhealthcare.cn/v1/picl/viewer/?objects=%5B%7B%22StudyUid%22%3A%22'
sands = '%22%2C%22SeriesUid%22%3A%22'
sando = '%22%2C%22ObjectUid%22%3A%22'
url_tail = '%22%7D%5D'
# studyUid = '1.2.156.147522.44.410947.3622.20180710151414'
# seriesUid = '1.2.156.147522.44.410947.3622.1.20180710151432'
# objectUid = '1.2.156.147522.44.410947.3622.1.1.20180710151432'
dcmUrl = 'http://v2.jfhealthcare.cn/v1/picl/aets/piclarc/wado?requestType=WADO&studyUID=%s&seriesUID=%s&objectUID=%s&contentType=application/dicom'


cnx = connector.connect(host="180.167.46.105", user="ai", passwd="hangzhou", db="qa")
cursor = cnx.cursor()
update = ("insert into viewer_dcm (localPath, studyUid, seriesUid, "
          "objectUid, viewerUrl, dcmUrl) values (%s, %s, %s, %s, %s, %s)")

with open('total_dcms.txt', 'r') as f:
    dcms_list = [l.rstrip() for l in f.readlines()]


f = open('log_dcms.txt', 'a+')
cnt = 0

for localPath in dcms_list:
    try:
        dcm_object = dicom.read_file(localPath, force=True)
        studyUid = str(dcm_object.StudyInstanceUID)
        seriesUid = str(dcm_object.SeriesInstanceUID)
        objectUid = str(dcm_object.SOPInstanceUID)
        view_url = url_head + studyUid + sands + seriesUid + sando + objectUid + url_tail
        dcm_url = dcmUrl % (studyUid, seriesUid, objectUid)
        data = (localPath, studyUid, seriesUid, objectUid, view_url, dcm_url)
        cursor.execute(update, data)
        cnt += 1
    except Exception as e:
        traceback.print_exc(file=f)
        f.flush()
        print "Error dcm: %s" % localPath

    if cnt % 20 == 0:
        cnx.commit()
        print cnt

cnx.commit()
cursor.close()
cnx.close()
f.close()
