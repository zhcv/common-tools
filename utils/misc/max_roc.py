#!/usr/bin/env python
# -*- coding=utf-8 -*-


import pylab as pl
from math import log, exp, sqrt

evaluate_result = "ymlx.csv"
db = []  # [filename,score]
pos, neg = 0, 0
with open(evaluate_result, 'r') as fs:
    for line in fs:
        _, nonclk, clk, score = line.strip().split(',')
        nonclk = int(nonclk)
        clk = int(clk)
        score = float(score)
        db.append([score, nonclk, clk])
        pos += clk
        neg += nonclk

print "roc:pos: ", pos 

db = sorted(db, key=lambda x: x[0], reverse=True)

# 计算ROC坐标点
best_acc = []
xy_arr = []
tp, fp = 0., 0.
for i in range(len(db)):
    tp += db[i][2]
    fp += db[i][1]
    acc = tp / (tp + fp)
    # print "accuracy", acc, db[i][0]
    best_acc.append(acc)
    xy_arr.append([fp / neg, tp / pos])

# print "index", best_acc.index(max(best_acc))

# 计算曲线下面积
auc = 0.
prev_x = 0
for x, y in xy_arr:
    if x != prev_x:
        auc += (x - prev_x) * y
    prev_x = x

print "the auc is %s." % auc

x = [_v[0] for _v in xy_arr]
y = [_v[1] for _v in xy_arr]
pl.title("ROC curve of %s (AUC = %.4f)" % ('TB', auc))
pl.xlabel("False Positive Rate")
pl.ylabel("True Positive Rate")
pl.plot(x, y)            # use pylab to plot x and y
pl.show()                # show the plot on the screen
# pl.savefig('beijing_part_test_5.png')
