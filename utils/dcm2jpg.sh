#!/usr/bin/env bash

#src="/home/jiufeng/datasets/dcm/"
file="total_dcms.list"
des="/data/jpg/JPEG2/"


# make same directory structure with raw directory
# rsync -avz --progress -f "+ */" -f "- *" $src $des 
# rsync -av --include='*/' --exclude='*' $src $des

# for dname in $src/*.dcm

for dname in `cat $file`
    do
        # temp=${dname/$src/$dsc}
        # jname=`echo ${temp%.*}.jpg`
        temp=${dname##*/}
        jname=$des${temp%.*}.jpg
        # echo $jname

	    /opt/dcm4che-5.10.5/bin/dcm2jpg $dname $jname
    done


