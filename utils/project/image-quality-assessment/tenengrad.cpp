#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
 
using namespace std;
using namespace cv;
 
int main(int argc, char *argv[])
{
    Mat imageSource = imread(argv[1]);
    Mat imageGrey;
 
    cvtColor(imageSource, imageGrey, CV_RGB2GRAY);
    Mat imageSobel;
    Sobel(imageGrey, imageSobel, CV_16U, 1, 1);
 
    //图像的平均灰度
    double meanValue = 0.0;
    meanValue = mean(imageSobel)[0];
 
    //double to string
    stringstream meanValueStream;
    string meanValueString;
    meanValueStream << meanValue;
    meanValueStream >> meanValueString;
    meanValueString = "Articulation(Sobel Method): " + meanValueString;
    putText(imageSource, meanValueString, Point(20, 100), 
            CV_FONT_HERSHEY_COMPLEX, 0.8, Scalar(255, 255, 25), 2);
    imshow("Articulation", imageSource);
    waitKey();
}
