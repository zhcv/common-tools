# -*- coding: utf-8 -*-
"""Reference: https://github.com/zhcv/KneeLocalizer/blob/master/proposals.py"""

import pydicom as dicom
from scipy import io
import numpy as np
from joblib import Parallel, delayed


def read_dicom(filename, cut_min=5, cut_max=99):
    """
    The function tries to read the dicom file and convert it to a decent quality
    uint8 image.
    gamma_A-scaling factor, which is needed to make the values on image to be > 1
    gamma_B-gamma correction degree
    cut_min - lowest percentile which is used to cut the image histogram
    cut_max - highest percentile
    """
    try:
        data = dicom.read_file(filename, force=True)
        # img = data.pixel_array
        img = np.frombuffer(data.PixelData, dtype=np.uint16).copy()
        img = img.astype(np.float64)
        if hasattr(data, 'PhotometricInterpretation') \ 
                    and data.PhotometricInterpretation == 'MONOCHROME1':
        lim1, lim2 = np.percentile(img, [cut_min, cut_max])
        img[img < lim1] = lim1
        img[img > lim2] = lim2
        
        img -= lim1
        img /= img.max()
        img *= 255
        img = img.astype(np.uint8)
        
        img = img.reshape(data.Rows, data.Columns)
        return img, # data.ImagePixelSpacing[0]
    except:
        return None


if __name__ == '__main__':
    dcm_names = []
    res = Parallel(n_jobs=6)(delayed(read_dicom)(i) for i in dcm_names)
