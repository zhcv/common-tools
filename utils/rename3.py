import pydicom as dicom
import os
import SimpleITK as sitk

with open('last.list', 'r') as f:
    dcms_list = [img.strip() for img in f.readlines()]

# root = '/data/dcm/local_blur'
# dcms_list = [os.path.join(root, fdcm) for fdcm in os.listdir(root)]



for filename in dcms_list:
    fdir = os.path.dirname(filename)
    try:
        dcm_object = dicom.read_file(filename, force=True)
        basename = dcm_object.SOPInstanceUID + '.dcm'
        nname = os.path.join(fdir, basename)
        os.rename(filename, nname)
    # except Exception, e:
    except AttributeError, e:
        print "Error Occured:", e
        dcm_object = sitk.ReadImage(filename)
        SOPInstanceUID = dcm_object.GetMetaData('0008|0018')
        basename = SOPInstanceUID + '.dcm'
        nname = os.path.join(fdir, basename)
        os.rename(filename, nname)
    finally:
        print "read  error: ", filename
