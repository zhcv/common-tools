# -*- coding: utf-8 -*-

from __future__ import print_function

import SimpleITK as sitk
import os
from PIL import Image
import numpy as np
import traceback
from subprocess import call
import pydicom
import pydicom.uid
from multiprocessing import Pool
from functools import partial

def dcm2jpg(filename, jpg_path, cut_min=5, cut_max = 99):
    if filename.endswith(('dcm', 'img')):
        try:
            ds = sitk.ReadImage(filename)
            img_array = sitk.GetArrayFromImage(ds)
            frame_num, height, width = img_array.shape
            dcm_object = pydicom.read_file(filename, force=True)
            dif = filename.split('/')[-2]
            basename = dcm_object.SOPInstanceUID + '.jpg'
            # pixel_array = dcm_object.pixel_array
            img=None
            if frame_num==1:
                img = img_array[0].astype(np.float64)
                if dcm_object.PhotometricInterpretation=='MONOCHROME1':
                    img = img.max() - img
                # Histogram equalization of the original image
                lim1, lim2 = np.percentile(img, [cut_min, cut_max])
                img[img<lim1] = lim1
                img[img>lim2] = lim2

                img -= lim1
                img /= img.max()
                img *= 255
                img = img.astype(np.uint8)

                im = Image.fromarray(img)
                im.save(os.path.join(jpg_path, basename))
                # im.save(os.path.join(jpg_path, basename[:-4] + '.jpg'))
            else:
                print (filename,' >>> frame_num: ', frame_num)
        except:
            f=open("log.txt",'a')
            traceback.print_exc(file=f)
            f.flush()
            f.close()
            print('read error:', filename)


def dcm2pnm(filename, jpg_path, maxsize=1500):
    basename = os.path.basename(filename)
    if basename.endswith(('dcm', 'img')):
        try:
            """
            ds = sitk.ReadImage(filename)
            img_array = sitk.GetArrayFromImage(ds)
            frame_num, height, width = img_array.shape
            """
            dcm_object = pydicom.read_file(filename, force=True)
            dcm_object.file_meta.TransferSyntaxUID = pydicom.uid.ImplicitVRLittleEndian
            img_array = dicomobject.pixel_array
            height, width = img_array.shape
            jpg_filename=os.path.join(jpg_dir, basename[:-4]+'.jpg')
            """
            if height > width:
               sc = float(maxsize) / height
            else:
               sc = float(maxsize) / width
            sx=1500/float(width)
            sy=1500/float(height)
            """
            sc=1.0
            ret=call(["dcmj2pnm", "-d", "-v", "-im", "+oj", "+Jq", "100", "+Wi","1","-S",
                      "+Sxf", str(sc), "+Syf",str(sc), filename, jpg_filename])
            if ret==1:
                ret=call(["dcmj2pnm", "+oj", "+Jq", "100", "+Wh", "10","-S",
                          "+Sxf", str(sc), "+Syf", str(sc),filename, jpg_filename])
            assert ret==0
            img=Image.open(jpg_filename)
            if img.height != height or img.width != width:
                img=img.resize((width,height), Image.ANTIALIAS)
                img.save(jpg_filename)
            '''filesize=os.path.getsize(jpg_filename)/1024/1024
            if filesize>=2:
                ff=open('ss.txt','a')
                ff.write(jpg_filename+'\n')
                ff.close()
                size=(1500,1500)
                if width>height:
                    nw=1500
                    nh=int(1500.0/width*height)
                    size=(nw,nh)
                else:
                    nh=1500
                    nw=int(1500.0/height*width)
                    size=(nw,nh)
                img=Image.open(jpg_filename)
                img=img.resize(size)
                img.save(jpg_filename)'''
        except:
            f=open("log.txt",'a')
            traceback.print_exc(file=f)
            f.flush()
            f.close()
            print ('read error:', filename)

def worker(dcm_name, jpg_path, resize, cut_min=5, cut_max=99):
    ds = sitk.ReadImage(dcm_name)
    img_array = sitk.GetArrayFromImage(ds)
    # frame_num, height, width = img_array.shape
    assert frame_num == 1
    img = img_array[0]
    PIKEY = '0028|0107' #  image PhotometricInterpretation
    if ds.GetMetaData(PIKEY)=='MONOCHROME1':
        img = img.max() - img
        # Histogram equalization of the original image
        lim1, lim2 = np.percentile(img, [cut_min, cut_max])
        img[img < lim1] = lim1
        img[img > lim2] = lim2

        img -= lim1
        img /= img.max()
        img *= 255
        img = img.astype(np.uint8)

    im = Image.fromarray(img)
    im.thumbnail(resize, Image.ANTIALIAS)
    im_name = os.path.basename(dcm_name)[:-4] + '.jpg'
    # img_name = os.path.splitext(os.path.basename(dcm_name))[0] + '.jpg'
    print(im_name)
    img.save(join(jpg_path, im_name))
    plt.imshow(img, cmap='gray')
    plt.show()


def convert_images_jpg(dcm_names, jpg_dir):
    if not os.path.isdir(jpg_dir):
        os.mkdir(jpg_dir)
    pool = Pool(processes=None)
    partial_worker = partial(dcm2jpg, jpg_path=jpg_dir)
    pool.map(partial_worker, dcm_names)


def convert_images_pnm(dcm_names, jpg_dir):
    if not os.path.isdir(jpg_dir):
        os.mkdir(jpg_dir)
    pool = Pool(processes=None)
    partial_worker = partial(dcm2pnm, jpg_path=jpg_dir)
    pool.map(partial_worker, dcm_names)

if __name__=='__main__':
    #root = "/data/qa/dcm/bad/mh"
    jpg_path = '/data/jpg/nphistogram'
    #jpg_list = os.listdir(jpg_path)
    dcmlist = open('total_dcm.list', 'r').readlines()
    dcm_names = [f.strip() for f in dcmlist]
    """
    dcm_names=[]
    for rt, dirs, files in os.walk(root):
        for fn in files:
            filename = os.path.join(rt, fn)
            jpgname = filename.split('/')[-2] + '_' + fn[:-4] + '.jpg'
            if jpgname in jpg_list:
                continue
            dcm_names.append(filename)
            print(filename)
    #fi.close()
    """
    #dcm_names = [os.path.join(root, f) for f in os.listdir(root) if f.endswith('.dcm')]
    convert_images_jpg(dcm_names, jpg_path)





