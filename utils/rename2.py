import pydicom as dicom
import os


with open('images_names.list', 'r') as f:
    jpgs_list = [img.strip() for img in f.readlines()]

with open('names_instance.list', 'r') as f:
    dcms_list = [img.strip() for img in f.readlines()]

dcms_dict = {}

for infile in dcms_list:
    rname, nname = infile.split()
    basename = os.path.basename(rname)
    dcms_dict[rname] = nname

#
# for k, v in dcms_dict.iteritems():
#     print k, v



for filename in jpgs_list:
    try:
        basename = os.path.basename(filename)
        dcm_name = dcms_dict.get(basename)

        if dcm_name:
            fdir = os.path.dirname(filename)
            nname = os.path.join(fdir, dcm_name)
            os.rename(filename, nname)
            print filename, nname
    except:
        print "cannot find %s in this computer!" % filename
