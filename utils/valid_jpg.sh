identify -format '%f' whatever.jpg

find |grep ".jpg$"  |xargs identify -format '%f' 1>ok.txt 2>errors.txt
find -name '*.jpg' -exec identify -format "%f" {} \; 1>ok.txt 2>errors.txt
djpeg -fast -grayscale -onepass file.jpg > /dev/null
for f in *.jpg; do djpeg -fast -grayscale -onepass $f > /dev/null; done
find . -iname "*.jpg" -exec jpeginfo -c {} \; | grep -E "WARNING|ERROR"
find . -type f -iname "*.jpg" -o -iname "*.jpeg"| xargs jpeginfo -c | grep -E "WARNING|ERROR" | cut -d " " -f 1


#!/bin/sh
find . -type f \
\( -iname "*.jpg" \
 -o -iname "*.jpeg" \) \
-exec jpeginfo -c {} \; | \
grep -E "WARNING|ERROR" | \
cut -d " " -f 1

find . -type f -iname "*.jpg" -exec identify -format '%f' {} \; 1>/dev/nul;

#!/bin/bash

find /photos -name '*.jpg' | while read FILE; do
    if [[ $(identify -format '%f' "$FILE" 2>/dev/null) != $FILE ]]; then
        echo "$FILE"
    fi
done


#!/bin/bash

find /photos -name '*.jpg' | while read FILE; do
    if ! identify "$FILE" &> /dev/null; then
        echo "$FILE"
    fi  
done
