import pydicom as dicom
import os


with open('images_names.list', 'r') as f:
    jpgs_list = [img.strip() for img in f.readlines()]

with open('total_dcm.list', 'r') as f:
    dcms_list = [img.strip() for img in f.readlines()]

dcms_dict = {}

for infile in dcms_list:
    basename = os.path.basename(infile)[:-4]
    dcms_dict[basename] = infile

#
# for k, v in dcms_dict.iteritems():
#     print k



for filename in jpgs_list:
    try:
        basename = os.path.basename(filename)[:-4]
        dcm_name = dcms_dict.get(basename)
        if dcm_name:
            dcm_object = dicom.read_file(dcm_name, force=True)
            basename = dcm_object.SOPInstanceUID + '.jpg'
            fdir = os.path.dirname(filename)
            nname = os.path.join(fdir, basename)
            os.rename(filename, nname)
        print nname
    except:
        print "cannot find %s in this computer!" % filename
