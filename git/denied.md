# Could not open a connection to your authentication agent

## Error
git@gitlab.com: Permission denied (publickey).
=============================================
```
$ ssh-add ~/.ssh/id_rsa.pub
Could not open a connection to your authentication agent.
```

resolve method: restart ssh-agent
```
eval `ssh-agent -s`
ssh-add
```
``` ssh-add ~/.ssh/keyname```
