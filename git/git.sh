#!/usr/bin/env bash

# Filename: disable_enable_track.sh

# ignore untracked file
touch .gitignore 
echo filename >> .gitignore 

# ignore tracked file
git update-index --assume-unchanged filename
# unignore tracked file
git update-index --no-assume-unchanged filename 

