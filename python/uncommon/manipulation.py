# -*- coding:utf-8 -*-
"""
In python a += b doesn’t always behave the same way as a = a + b, same 
operands may give the different results under different conditions.

>>> expression list1 += [1, 2, 3, 4] modifies the list in-place, means it 
    extends the list such that `list1` and `list2` still have the reference 
    to the same list.
>>> expression list1 = list1 + [1, 2, 3, 4] creates a new list and changes 
    `list1` reference to that new list and `list2` still refer to the old list.
"""
from __future__ import print_function

# Example_1:  a += b
list1 = [5, 4, 3, 2, 1]
list2 = list1
list1 += [1, 2, 3, 4]

print(list1)
print(list2)


print()

# Example_2: a = a + b
list1 = [5, 4, 3, 2, 1]
list2 = list1
list1 = list1 + [1, 2, 3, 4]

# Contents of list1 are same as above program, but contents of list2 are different.
print(list1)
print(list2)
