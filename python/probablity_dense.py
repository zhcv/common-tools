from functools import partial
import numpy as np
from matplotlib import pyplot

# Define a PDF
x_samples = np.arange(-3, 3.01, 0.01)
PDF = np.empty(x_samples.shape)
PDF[x_samples < 0] = np.round(x_samples[x_samples < 0] + 3.5) / 3
PDF[x_samples >= 0] = 0.5 * np.cos(np.pi * x_samples[x_samples >= 0]) + 0.5
PDF /= np.sum(PDF)

# Calculate approximated CDF
CDF = np.empty(PDF.shape)
cumulated = 0
for i in range(CDF.shape[0]):
    cumulated += PDF[i]
    CDF[i] = cumulated

# Generate samples
generate = partial(np.interp, xp=CDF, fp=x_samples)
u_rv = np.random.random(10000)
x = generate(u_rv)

# Visualization
fig, (ax0, ax1) = pyplot.subplots(ncols=2, figsize=(9, 4))
ax0.plot(x_samples, PDF)
ax0.axis([-3.5, 3.5, 0, np.max(PDF)*1.1])
ax1.hist(x, 100)
pyplot.show()

