# Python flake8 静态语法检查

* install flake8 (default installed)
```
sudo pip install flake8
```

* install vim-pathogen
```
 mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
 ```

 * edit .vimrc file
 ## input follow content
 execute pathogen   #infect()
 syntax on
 filetype plugin indent on
 ```
 vim ~/.vimrc
 ```

* install vim-flake8 to ~/.vim/bundle
```
cd ~/.vim/bundle && git clone https://github.com/nvie/vim-flake8.git
```
* open python file and edit, then press F7 syntax check

REFFERENCE
![](https://github.com/nvie/vim-flake8)
![](https://github.com/tpope/vim-pathogen)
