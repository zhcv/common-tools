import urllib
import requests
from PIL import Image
from io import BytesIO
import os

#  picture address on the network
img_src = 'http://img.zcool.cn/community/010f87596f13e6a8012193a363df45.jpg@1280w_1l_2o_100sh.jpg'

# methods: use urllib.urlretrieve()
# download the remote data locally, the second parameter is the filename to be saved locally
urllib.urlretrieve(img_src,'./1.jpg')

# methods: PIL + requests
response = requests.get(img_src)
image = Image.open(BytesIO(response.content))
image.save(os.path.basename(img_src))
