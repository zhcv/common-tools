import gc

class A:
    def __del__(self):
        pass

class B:
    def __del__(self):
        pass


a = A()
b = B()

print "hex(id(a)):", hex(id(a))
print "hex(id(a.__dict__)):", hex(id(a.__dict__))

a.b = b
b.a = a
del a
del b

print gc.collect()
print gc.garbage
