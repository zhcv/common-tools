import io
import json


try:
    to_unicode = unicode
except NameError:
    unicode = str


with open('qc_labels.json') as f:
    ds = f.readline().strip()
    data = json.loads(ds)


with io.open('data3.json', 'w', encoding='utf8') as outfile:
    str_ = json.dumps(data, indent=4, sort_keys=True,
                      separators=(',', ': '), ensure_ascii=False)
    outfile.write(to_unicode(str_))
