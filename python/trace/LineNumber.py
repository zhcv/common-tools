"""This provides a lineno() function to make it easy to grab the line
number that we're on.

Reference with http://python.org/doc/lib/module-inspect.html 
print "here is :",__file__, sys._getframe().f_lineno => this is dedicated for output with
filename and line number. another way to represent line number instead of using  function method.
"""

import inspect

def lineno():
    """Returns the current line number in our program."""
    return inspect.currentframe().f_back.f_lineno

if __name__ == '__main__':
    print "hello, this is line number", lineno()
    print 
    print 
    print "and this is line", lineno()


