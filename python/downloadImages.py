#! /usr/bin/env python
# -*- coding:utf-8 -*-

import os
import re
import urllib

def get_html(url):
    page = urllib.urlopen(url)
    html = page.read()
    return html

def get_img(html):
    reg = r'src="(.*?\.jpg)" bdwater='
    imgre = re.compile(reg)
    imglist = re.findall(imgre, html)
    i = 0
    for imgurl in imglist:
        imgname = os.path.join('asserts', '%s.jpg' % i)
        urllib.urlretrieve(imgurl, imgname)
        i+=1

if __name__ == '__main__':
    html = get_html('http://tieba.baidu.com/p/2166231880')
    get_img(html)
