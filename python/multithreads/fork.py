#!/usr/bin/env python

import os

print "Process (%s) start ... " % os.getpid()
pid = os.fork()
if pid == 0:
    print "I am child process (%s) and ma parent is %s." % (os.getpid(), os.getppid())
    os._exit(1)
else:
    print "I (%s) just created a child process (%s)." % (os.getpid(), pid)
