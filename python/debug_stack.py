import sys




def find_caller(func):
    def wrapper(*args, **kwargs):
        f = sys._getframe()
        filename = f.f_back.f_code.co_filename
        lineno = f.f_back.f_lineno
        print "=" * 80
        print "caller filename is ", filename
        print "caller lineno is ", lineno
        print "the passed args is ", args, kwargs
        print "=" * 80
        func(*args, **kwargs)
    return wrapper


class A(object):
    def __init__(self, num=0):
        print num

    @find_caller
    def func(self, num=None):
        print num

if __name__ == '__main__':
    a1 = A()
    a2 = A(1)
    a3 = A(2)
    a2.func(3)
