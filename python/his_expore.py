import numpy as np
from skimage import exposure
from skimage import io
from PIL import Image

image = io.imread('10.11.1247.1147.123.149.20180814182116.978.jpg')

print image.shape
p2, p98 = np.percentile(image, (2, 98))
image = exposure.rescale_intensity(image, in_range=(p2, p98))


# Equalization
image = exposure.equalize_hist(image)

# Adaptive Equalization
# image = exposure.equalize_adapthist(image)

print image.max()
image = ((image-np.min(image))/(np.max(image)-np.min(image))*255).astype(np.uint8)
# io.imsave('333.jpg', image)
io.imshow(image)
io.show()
# Image.fromarray((np.uint8(image * 255)))

