#/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import namedtuple, deque, Counter, OrderedDict, defaultdict

websites = [
    ('Sohu', 'http://www.google.com/', '张朝阳'),
    ('Sina', 'http://www.sina.com.cn/', '王志东'),
    ('163', 'http://www.163.com/', '丁磊')
]
Website = namedtuple('Website', ['name', 'url', 'founder'])
for website in websites:
    website = Website._make(website)
    print (website.founder)


"""
=>deque(iterable,maxlen)
deque其实是double-ended queue的缩写，翻译过来就是双端队列，它最大的好处就是实现了从队列
头部快速增加和取出对象: .popleft(), .appendleft()。原生的List也能从头部添加和去除对象，
l.insert(0, v)、l.pop(0)。但是值得注意的是，list对象的这两种用法的时间复杂度是O(n)，也就
是说随着元素数量的增加耗时呈线性上升。而使用deque对象则是O(1) 的复杂度，所以当你的代码有
这样的需求的时候，一定要记得使用deque。

作为一个双端队列，deque还提供了一些其他的好用方法，比如 rotate等。

append(x):从队列末尾添加x
appendleft(x):从队列头部添加x
clear():清空队列
count(x):返回队列中x的数量
extend(iterable):在队列的尾部添加多个元素
extendleft(iterable):在队列的头部添加多个元素，反序插入（字母表相反顺序）
pop():将尾部一个元素移除
popleft():将头部一个元素移除
remove(x):将队列中第一次出现的x元素移除
reverse():将队列元素逆置
rotate(n):将队列尾部的n个元素添加到头部 rotate(1)等价于:d.appendleft(d.pop())
D.maxlen:返回队列的长度，如果是无界则返回None（定义的时候可以指定maxlen，否则默认是无界队列）
"""

deq = deque("gsgwqeasas")
print (deq)
print (list(deq))

"""
=>Counter(iterable or mapping)  #可迭代或者映射的对象

计数器是一个非常常用的功能需求。
elements():返回一个重复元素的迭代器，重复次数和计数的次数一样多。元素以任意顺序
返回。如果一个元素的计数小于1，elements()会忽略它。
most_common([n]):返回n个计数器元素，若未指定n，则返回计数器的所有元素
update(iterable or mapping):增加计数器元素，元素可以来源于迭代对象或者一个Counter对象
"""
print ">>>>>>>>> Counter Begin <<<<<<<<<<<<<<"
s = ['w', 'g', 'g', 's', 'a', 's', 'a', 'e', 'q']
c = Counter(s)

print list(c)
print dict(c)
print
print list(c.elements())

print ">>>>>>>>> Counter End <<<<<<<<<<<<<<"
print "\n ============== OrderedDict ================"
"""
=>OrderedDict(items)

在Python中，dict这个数据结构由于hash的特性，是无序的，这在有的时候会给我们带来一些麻烦，
幸运的是，collections模块为我们提供了OrderedDict，当你要获得一个有序的字典对象时，用它就对了。

popitem(last=True):移除字典对象元素，last=True时“后进先出”，last=False时“先进先出”

move_to_end(key,last=True):将字典中一个已经存在的键移动到头部或者尾部。last=True时，移动到
尾部，last=False时，移动到头部。
"""
d = {'w': 2, 'a': 4, 'f': 2, 'q': 2, 'g': 4, 'd': 2, 'e': 2, 's': 6}
n = OrderedDict(d)
print n
print "n.popitem() :", n.popitem()

# print "n.move_to_end('f'):", n.move_to_end('f')
print ">>> n"
print n
