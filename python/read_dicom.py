import os
import traceback
import logging
import uuid
import shutil
import numpy as np
import pydicom as dicom
import SimpleITK as sitk
from PIL import Image
from multiprocessing import Pool
from functools import partial

def read_dicom(dcm_data, jpg_path, cut_min=5, cut_max=99):
    """Read DICOM file and convert it to a decent quality unit8 image.

    Parameters:
    ----------
    filename: str
        Existing DICOM file filename.
    cut_min: int
        lowest percentile which is used to cut the image histogram.
    cut_max: int
        highest percentile which is used to cut the image histogram.
    """
    basename = os.path.basename(dcm_data).replace('.dcm', '.jpg')
    jpgname = os.path.join(jpg_path, basename)
    print jpgname
    # if load data from memory failed, then read data from the disk
    try:
        ds = dicom.read_file(dcm_data, force=True)
        ds.file_meta.TransferSyntaxUID = dicom.uid.ImplicitVRLittleEndian
        """
        img = np.frombuffer(ds.PixelData, dtype=np.uint16).copy().astype(np.float64)
        img = img.reshape((ds.Rows, ds.Columns))
        """
        img = ds.pixel_array.copy().astype(np.float64)
        # Rescale intercept and slope are a simple linear transform applied
        # to the raw pixel data before applying the window width/center.
        RescaleSlope = getattr(ds, 'RescaleScope', None)
        RescaleIntercept = getattr(ds, 'RescaleIntercept', None)
        if RescaleSlope and RescaleIntercept:
            img = (img * RescaleSlope) + RescaleIntercept

        center = getattr(ds, 'WindowCenter', None)
        width = getattr(ds, 'WindowWidth', None)
        # Calculate the pixels to display using the window center/width
        # lowest_visible_value and highest_visible_vale
        if center and width:
            if isinstance(center, dicom.multival.MultiValue):
                center = center[0]
            if isinstance(width, dicom.multival.MultiValue):
                width = width[0]
            minval = center - 0.5 - (width - 1) / 2.
            maxval = center - 0.5 + (width - 1) / 2.
        else:
            minval, maxval = np.percentile(img, [cut_min, cut_max])

        img[img < minval] = minval
        img[img > maxval] = maxval
        img -= minval

        if ds.PhotometricInterpretation == 'MONOCHROME1':
            img = img.max() - img

        # Rescale pixel value [0, 255]
        img /= img.max()
        img *= 255
        img_pil = Image.fromarray(img.astype(np.uint8))
        img_pil.save(jpgname)
    except Exception, e:
	ds = sitk.ReadImage(dcm_data)
        img_array = sitk.GetArrayFromImage(ds)
        frame_num, height, width = img_array.shape
        assert frame_num == 1
        img = np.float64(img_array[0])

        tags = ['0028|0004',    # PhotometricInterpretation
                '0028|1050',    # WindowCenter
                '0028|1051',    # WindowWith
                '0028|1052',    # RescaleIntercept
                '0028|1053',    # RescaleSlope
        ]
        # tags_all = ds.GetMetaDataKeys()
        if ds.HasMetaDataKey(tags[3]) and ds.HasMetaDataKey(tags[4]):
            RescaleIntercept = float(ds.GetMetaData(tags[3]))
            RescaleSlope = float(ds.GetMetaData(tags[4]))
            img = (img * RescaleSlope) + RescaleIntercept

        center = ds.GetMetaData(tags[1]) if ds.HasMetaDataKey(tags[1]) else None
        width = ds.GetMetaData(tags[2]) if ds.HasMetaDataKey(tags[2]) else None

        if center and width:
            center = float(center.split('\\')[0])
            width = float(width.split('\\')[0])
            minval = center - 0.5 - (width - 1) / 2.
            maxval = center - 0.5 + (width - 1) / 2.
        else:
            minval, maxval = np.percentile(img, [cut_min, cut_max])

        # Normalized img [0, maxval-minval]
        img[img < minval] = minval
        img[img > maxval] = maxval
        img -= minval
        if ds.GetMetaData(tags[0]) == 'MONOCHROME1 ':
            img = img.max() - img

        img /= img.max()
        img *= 255
        img = img.astype(np.uint8)
        img_pil = Image.fromarray(img.astype(np.uint8))
        img_pil.save(jpgname)

def convert_dcms(dcm_names, jpg_dir):
    if not os.path.isdir(jpg_dir):
        os.mkdir(jpg_dir)
    pool = Pool(processes=None)
    partial_worker = partial(read_dicom, jpg_path=jpg_dir)
    pool.map(partial_worker, dcm_names)


if __name__ == '__main__':
    root = '/media/zhang/Elements/dcm/qc_normal/20180901_20180908'
    jpg_path = '/data/dcm/18901908'

    dcm_names = [os.path.join(root, f) for f in os.listdir(root) if f.endswith('.dcm')]
    convert_dcms(dcm_names, jpg_path)
    # for dcm in dcm_names[365:]:
    # read_dicom(dcm, jpg_path)
