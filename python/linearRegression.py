"""Problem

In economics, the Cobb-Douglas functional form of production functions is widely used to represent the relationship of an output to inputs. it was proposed by Knut Wicksell (1851 - 1926), and tested again statistical evidence by Charles Cobb and Paul Douglas in 1928. In 1928 Charles Cobb and Paul Douglas published a study in which they modeled the growth of the American economy during the period 1899-1922. They considered a simplified view of the economy in which production output is determined by the amount of labor involved and the amount of capital invested:

${ P(L,K) = bL^ \alpha K^ {1-\alpha} }$

${P}$ : total production (the monetary value of all goods produced in a year)
${L}$: labor input (the total number of person-hours worked in a year)
${K}$: capital input (the monetary worth of all machinery, equipment, and buildings)
${b}$: total factor productivity
${\alpha}$: the output elasticities of labor
Questions
Using the data about the American economy in the table to find out ${b}$ and ${\alpha}$

The equatation can be re-written as:
${log(P) = log(b) + \alpha log(L) + (1-\alpha)log(K) = log(b) + \alpha (log(L) - log(K)) + log(K) }$
${log(P) - log(K) = log(b) + \alpha (log(L) - log(K))}$
Then:
${y = log(P) - log(K)}$
${x = log(L) - log(K)}$

Result:
log(b) = 0.00700559< = >b = e(0.00700559) = 1.007
${log(b) = 0.00700559 &lt;=&gt; b = e^{0.00700559} = 1.007}$
${\alpha = 0.77858937}$

"""
import tensorflow as tf
import numpy as np

origin_data = np.array([[100,100,100],
                 [101,105,107],
                 [112,110,114],
                 [122,117,122],
                 [124,122,131],
                 [122,121,138],
                 [143,125,149],
                 [152,134,163],
                 [151,140,176],
                 [126,123,185],
                 [155,143,198],
                 [159,147,208],
                 [153,148,216],
                 [177,155,226],
                 [184,156,236],
                 [169,252,244],
                 [189,156,266],
                 [225,183,298],
                 [227,198,335],
                 [223,201,366],
                 [218,196,387],
                 [231,194,407],
                 [179,146,417],
                 [240,161,431]])
transform_data = np.log(origin_data)

# print transform_data
# y = log(P) - log(K)
y_data = transform_data[:, 0:1] - transform_data[:, 2:]
print y_data, y_data.shape

# x = log(L) - log(K)
X_data = transform_data[:, 1:2] - transform_data[:, 2:]
# Append x_0 = 1 to x
X_data = np.append(np.tile(np.array([1]), (X_data.shape[0], 1)), X_data, axis=1)
print X_data, X_data.shape

theta = tf.Variable([[1], [2]], dtype=tf.float32)

sess = tf.Session()

X = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)

cost = tf.reduce_mean(tf.pow(tf.matmul(X, theta)-y, 2))/(2*y_data.shape[0])
# print session.run(tf.matmul(X, theta)-y_data, feed_dict={X: X_data}) 


train = tf.train.GradientDescentOptimizer(0.001).minimize(cost)
init = tf.global_variables_initializer()
session = tf.Session()
session.run(init)

num_iterations = 10000
for _ in range(num_iterations):
    session.run(train, feed_dict={X: X_data, y: y_data})
print(session.run(theta))
