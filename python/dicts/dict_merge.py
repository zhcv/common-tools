#!/usr/bin/env python3
import collections

dict1 = {'data1': 'Mon', 'data2': 'Tue'}
dict2 = {'data3': 'Wed', 'data1': 'Thu'}


res = collections.ChainMap(dict1, dict2)

# Creating a single dictionary
print(res.maps, '\n')

print('Keys = {}'.format(list(res.keys())))
print('Values = {}'.format(list(res.values())))
print()

# Print all the elements from the result
print('elements:')
for key, val in res.items():
    print('{} = {}'.format(key, val))
print()


# Find a specific value in the result
print('data3 in res: {}'.format(('data1' in res)))
print('data4 in res: {}'.format(('data4' in res)))
