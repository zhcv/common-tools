import os
import sys
import tarfile

from six.moves import urllib


def _download_and_uncompress_dataset(dataset_dir):
  """Downloads cifar10 and uncompresses it locally.

  Args:
    dataset_dir: The directory where the temporary files are stored.
  """
  filename = _DATA_URL.split('/')[-1]
  filepath = os.path.join(dataset_dir, filename)

  if not os.path.exists(filepath):
    def _progress(count, block_size, total_size):
      sys.stdout.write('\r>> Downloading %s %.1f%%' % ( 
          filename, float(count * block_size) / float(total_size) * 100.0))
      sys.stdout.flush()
    filepath, _ = urllib.request.urlretrieve(_DATA_URL, filepath, _progress)
    print()
    statinfo = os.stat(filepath)
    print('Successfully downloaded', filename, statinfo.st_size, 'bytes.')
    tarfile.open(filepath, 'r:gz').extractall(dataset_dir)


def _clean_up_temporary_files(dataset_dir):
  """Removes temporary files used to create the dataset.

  Args:
    dataset_dir: The directory where the temporary files are stored.
  """
  filename = _DATA_URL.split('/')[-1]
  filepath = os.path.join(dataset_dir, filename)
  os.remove(filepath)


# =============================================================================
import urllib

# urllib.urlretrieve(url[, filename[, reporthook[, data]]])


def callbackfunc(block_num, block_size, total_size):
    percent = 100.0 * block_num * block_size / total_size
    if percent > 100:
        percent = 100
    sys.stdout.write('\r>> Downloading %.2f%%' % percent)
    sys.stdout.flush()

url = "https://www.baidu.com"
local = "/home/username"
urllib.urlretrieve(url, local, callbackfunc)
