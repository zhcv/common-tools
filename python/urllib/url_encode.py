from urllib import quote, unquote

print quote(":")

print quote("http://www.baidu.com")

print unquote("http%3A//www.baidu.com")


# method2: urlencode just only accept key-value pair format data. 
# only for dict and does not currently provide the urldecode method

import urllib

params = urllib.urlencode({'spam': 1, 'eggs': 2, 'bacon': 0})

print params
print 
print "http://www.musi-cal.com/cgi-bin/query?%s" % params

f = urllib.urlopen("http://www.musi-cal.com/cgi-bin/query?%s" % params)

print f.read() 
