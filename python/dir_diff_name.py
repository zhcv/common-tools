import os

# src = 'another_data/sharpness'
# dst = 'release/sharpness'

src = 'another_data/blurry'
dst = 'release/blurry'

src_lists = os.listdir(src)

dst_lists = os.listdir(dst)

# method_1 difference set
ret_list = []
for item in src_lists:
    if item not in dst_lists:
        ret_list.append(item)
# method_2
# ret_list = [item for itme in src_lists if item not in dst_lists]

# method_3 
# ret_list = list(set(src_lists)^set(dst_lists))


## union set
# ret_list = list(set(src_lists).union(set(ds)))

## intersection 
# ret_list = list((set(a_list).union(set(b_list)))^(set(a_list)^set(b_list)))

for img in ret_list:
    print img
