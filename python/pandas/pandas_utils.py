#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Top3 Values Per Row in Pandas

"""
table-old
| ID | Var1 | Var2 | Var3 | Var4 | Var5 |
|----|------|------|------|------|------|
| 1  | 1    | 2    | 3    | 4    | 5    |
| 2  | 10   | 9    | 8    | 7    | 6    |
| 3  | 25   | 37   | 41   | 24   | 21   |
| 4  | 102  | 11   | 72   | 56   | 151  |


table-new
| ID | 1st Max | 2nd Max | 3rd Max |
|----|---------|---------|---------|
| 1  | Var5    | Var4    | Var3    |
| 2  | Var1    | Var2    | Var3    |
| 3  | Var3    | Var2    | Var1    |
| 4  | Var5    | Var1    | Var3    |

table-old ---> table-new
"""

import pandas as pd
import numpy as np


value = [1,  2,  3,  4,   5, 
        10,  9,  8,  7,   6,
        25, 37, 41, 24,  21,
       102, 11, 72, 56, 151]

arr = pd.Series(value).values.reshape(4, 5)

print arr

df = pd.DataFrame(data=arr, columns=['var1', 'var2', 'var3', 'var4', 'var5'])

for st in  dir(df):
    if st.startswith('s'):
        # print st
        pass

print "\ndf attributes"
print "df.index", df.index
print "df.columns", df.columns.values
