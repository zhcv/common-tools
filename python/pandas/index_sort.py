# coding=utf-8
import pandas as pd

obj=pd.Series([4,9,6,20,4],index=['d','a','e','b','c'])

print "obj"
print obj

# 按obj的索引排序,默认升序，降序可在括号加ascending=False
print "obj.sort_index()"
print obj.sort_index()

ds = pd.Series([3,5,2,6,9,23,12,34,12,15,11,0]).values.reshape(3,4)
frame = pd.DataFrame(data=ds, 
    columns=['c','f','d','a'], index=['C','A','B'])

print "frame", frame

#按frame的行索引进行排序
print "frame.sort_index()"
print frame.sort_index()

# 按frame的列索引进行排序
print "frame.sort_index(axis=1)"
print frame.sort_index(axis=1)


# 按frame的一个列或多个列的值进行排序
print "frame.sort_index(by=['a', 'c'])"
print frame.sort_values(by=['a', 'c'])

print "..."
print "Convert DataFrame to numpy ndarray\n"
a = pd.DataFrame([[1,2,3],[4,5,6]])
print "\npd.DataFrame([[1,2,3],[4,5,6]])\n", a
b = a.values.reshape(3,2)
print "\na.values.reshape(3, 2)\n", b
a = pd.DataFrame(b)
print "\na\n", a
