import datetime as dt
import pandas as pd

import scipy as s


if __name__ == '__main__':
    base = dt.datetime.today().date()
    print base
    dates = [base - dt.timedelta(days=x) for x in range(10)]
    dates.sort()

    valdict = {}
    symbols = ['A', 'B', 'C']
    for symb in symbols:
        valdict[symb] = pd.Series(s.zeros(len(dates)), dates)

    for thedate in dates:
        if thedate > dates[0]:
            for symb in valdict:
                valdict[symb][thedate] = 1 + valdict[symb][thedate - dt.timedelta(days=1)]

    print valdict
