Warning or remind func deprecated in the future
===============================================


```python
import warnings

warnings.warn("contents", FutureWarning, stacklevel=2)
```
