#/usr/bin/env python
# -*- coding:utf-8 -*-

import numpy as np
import os
from scipy import misc

fileslist = os.listdir('gt_images')

cnt = len(files)
print cnt
toc = np.zeros(14, dtype=float)

for infile in fileslist:
    arr = misc.imread(os.path.join('gt_image', infile))
    h_arr = np.histogram(arr, density=True, bins=np.arange(15))[0]
    toc += h_arr
    print infile

print "weight: ", toc / cnt
