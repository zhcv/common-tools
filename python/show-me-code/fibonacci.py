"""https://github.com/taizilongxu/interview_python#python%E8%AF%AD%E8%A8%80%E7%89%B9%E6%80%A7"""

# method_1
fib = lambda n: n if n <= 2 else fib(n - 1) + fib(n - 2)


# method_2
def fib_(n):
    a, b = 0, 1
    for _ in range(n):
        a, b = b, a + b

    return b


# method_3
def memo(func):
    cache = {}
    def wrap(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrap


@memo
def fib3(i):
    if i < 2:
        return 1
    return fib(i-1) + fib(i-2)
