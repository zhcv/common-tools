from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


def add_num(img):
    draw = ImageDraw.Draw(img)
    myfont = ImageFont.truetype("lato/Lato-Medium.ttf", size=40)
    fillcolor = "#ff0000"
    width, height = img.size

    draw.text((20, 50), '99', font=myfont, fill=fillcolor)
    img.show()


if __name__ == '__main__':
    image = Image.open('lena.jpg')
    add_num(image)
