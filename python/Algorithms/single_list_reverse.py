#!/usr/bin/env python
# -*- coding: utf-8 -*-


class ListNode(object):
    def __init__(self, x):
        self.val = x
        self.next = None


def recurse(head, new_head):
    if not head:
        return 
    if not head.next:
        new_head = head
    else:
        new_head = recurse(head.next, new_head)
        head.next.next = head
        head.next = None
    return new_head

    
if __name__ == '__main__':
    head = ListNode(1)  # test code
    p1 = ListNode(2)
    p2 = ListNode(3)
    p3 = ListNode(4)

    head.next = p1
    p1.next = p2
    p2.next = p3
    new_head = None

    p = recurse(head, new_head)
    while p:
        print p.val
        p = p.next
