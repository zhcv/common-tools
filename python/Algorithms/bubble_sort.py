

def bubble_sort(ary):
    n = len(ary)
    for i in range(n):
        flag = True
        for j in range(1, n):
            if ary[j-1] > ary[j]:
                ary[j-1], ary[j] = ary[j], ary[j-1]
                flag = False
        if flag:
            break   # After each element sorted, break loop
    return ary


print bubble_sort([3, 1, 4, 5, 11, 3, 4, 5, 1, 2])
