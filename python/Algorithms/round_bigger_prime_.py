import math

def get_prime_than(n):
    if isprime(n+1):
        return n+1
    return get_prime_than(n+1)


def isprime(m):
    if m <= 1:
        return False
    for i in range(2, int(math.sqrt(m)) + 1):
        if m % i == 0:
            return False
        return True

# l1 = filter(isprime,range(2,100))
# print(list(l1))
print get_prime_than(101)
