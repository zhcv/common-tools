#写一个函数, 输入一个字符串, 返回倒序排列的结果
#1).利用字符串本身的翻转
 
str = 'abcdef'
# def string_reverse(string):
#     return string[::-1]
#
# if __name__ =="__main__":
#     print(str)
#     print(string_reverse(str))
 
#2).把字符串变成列表，用列表的reverse函数
# def string_reverse2(string):
#     new_str = list(string)
#     new_str.reverse()
#     return ''.join(new_str)
# if __name__ =="__main__":
#     print(str)
#     print(string_reverse2(str))
 
 
#3).新建一个列表，从后往前取
# def string_reverse3(string):
#     new_str = []
#     for i in range(1,len(string)+1):
#         new_str.append(string[-i])
#
#     return ''.join(new_str)
#
# if __name__ =="__main__":
#     print(str)
#     print(string_reverse3(str))
 
#4).利用双向列表deque中的extendleft函数
# from collections import deque
#
# def string_reverse4(string):
#     d = deque()
#     d.extendleft(string)
#     return ''.join(d)
#
# if __name__ =="__main__":
#     print(str)
#     print(string_reverse4(str))
 
#5).递归
# def string_reverse5(string):
#     if len(string)<=1:
#         return string
#     else:
#         return string_reverse5(string[1:])+string[0]
#
# if __name__ =="__main__":
#     print(str)
#     print(string_reverse5(str))
