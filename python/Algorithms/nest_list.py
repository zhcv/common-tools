# -*- coding: utf-8 -*-
import itertools


def spread_list(lst):
    """
    Expand the nested list
    >>> spread_list([1, 3, [5, 6, [9, 10], [11, [12, [13, 14]]], 15]])
    [1, 3, 5, 6, 9, 10, 11, 12, 13, 14, 15]
    """
    return sum([spread_list(x) if isinstance(x, list) else [x] for x in lst], [])


#################################################################
#########              艾氏筛选求质数                  ##########
#################################################################
def _odd_iter():
    n = 1
    # while True:
    #   n += 2
    #   yield n
    return itertools.count(3, 2)


def not_divisable(n):
    return lambda x: x % n > 0

def primes():
    yield 2
    it = _odd_iter()
    while True:
        n = next(it)
        yield n
        it = filter(not_divisable(n), it)


if __name__ == '__main__':
    lst = [1, 3, [5, 6, [9, 10], [11, [12, [13, 14]]], 15]]
    print spread_list(lst)

    print "primes"
    p = primes()
    for i in range(10):
        print next(p)
