# -*- coding: utf-8 -*-

# Two List reassignment: minimize(sum(lst1) - sum(lst2))

# 有两个序列a,b，大小都为n,序列元素的值任意整形数，无序；要求：通过交换a,b中的
# 元素, 使[序列a元素的和]与[序列b元素的和]之间的差最小。
# 原理：通过排序倒序找到每一个值last,通过sum大小判断放入small_list中，这其中有
# 可能某个list先添加满，所以要判断length


def diff(lst, length):
    if not lst:
        return ([], [])
    last = lst[-1]
    blst, slst = diff(lst[:-1], length)
    blst_sum = sum(blst)
    slst_sum = sum(slst)

    if blst_sum > slst_sum:
        if len(slst) >= length:
            blst.append(last)
        else:
            slst.append(last)
        return (blst, slst)
    else:
        if len(blst) >= length:
            slst.append(last)
        else:
            blst.append(last)
        return (slst, blst)

a = [1,13,4,9]
b = [3,44,800,700]
c= []
c.extend(a)
c.extend(b)
p = sorted(c,reverse=True)
w = diff(p, len(a))
print(w)
