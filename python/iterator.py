# -*- coding: utf-8 -*-

from collections import Iterator, Iterable

print "=" * 48
print "isinstance([], Iterable)", isinstance([], Iterable)
print "isinstance([], Iterator)", isinstance([], Iterator)
print
print "isinstance(iter{}, Iterator)", isinstance(iter({}), Iterator)
print "isinstance({}, Iterable)", isinstance({}, Iterable)
print "isinstance({}, Iterator)", isinstance({}, Iterator)
print
print "isinstance('abc', Iterable)", isinstance('abc', Iterable)
print "isinstance('abc', Iterator)", isinstance('abc', Iterator)
print
print "isinstance(range(5), Iterable)", isinstance(range(5), Iterable)
print "isinstance(range(5), Iterator)", isinstance(range(5), Iterator)
print
print "isinstance((x for x in range(5)), Iterable)", isinstance((x for x in range(5)), Iterable)
print "isinstance((x for x in range(5)), Iterator)", isinstance((x for x in range(5)), Iterator)

print "=" * 48
print

## Python的for循环本质上就是通过不断调用next()函数实现的
nums = [1, 2, 3, 4, 5]
it = iter(nums)

print "=" * 48
# method1 <====> method2
# method1
for n in nums: print n
print "=" * 48
#methods
while True:
    try:
        x = next(it)
        print x
    except StopIteration:
        # break when occurs StopIteration 
        break
print "=" * 48


## 使用zip 和 iterator生成滑动窗口(n-grams)
from itertools import islice

def n_grams(a, n):
    z = (islice(a, i, None) for i in range(n))
    return zip(*z)

print list(n_grams(range(1,7), 3))
