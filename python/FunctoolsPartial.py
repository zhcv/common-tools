


class partial:
    """New function with partial application of the given arguments
    and keywords.
    """
    ### __SLOTS__ # Only chass has attr, cannot dynamic add other attr
    __slots__ = "func", "args", "keywords", "__dict__", "__weakref__"
    ### __new__ method 生成实例对象
    
    def __new__(*args, **keywords):
        if not args:
            raise TypeError("descriptor '__new__' of partial needs and argument")
        if len(args) < 2:
            raise TypeError("Type 'partial' takes at least one argument")
        cls, func, *arg = args    # args = (cls, func, *args)
        if not callable(func):
            raise TypeError("the first argument must be callable")

        args = tuple(args)
        #  hasattr这块我也没有咋个明白，不知道可以应用到什么地方，从使用方法来看，
        # 传入的函数func要有属性或是方法，如果知道请告知我一下
        if hasattr(func, "func"):
            args = func.args + args
            tmpkw = func.keywords.copy()
            tmpkw.update(keywords)
            keywords = tmpkw
            del tmpkw
            func = func.func
        # 创建一个实例对象本身
        self = super(partial, cls).__new__(cls)
        # 动态的添加属性
        self.func = func
        self.args = args
        self.keywords = keywords
        return self


        # 上面的代码创建了一个实例对象（p=partial(func,*args,**kw)），并给对象本身添加了属性。
    # ###在使用p()时会自动调用__call__方法
    def __call__(*args, **keywords):
        if not args:
            raise TypeError("descriptor '__call__' of partial needs an argument")
        self, *args = args
        # 将位置参数和关键字参数分别合在一起，在使用p（）的时候只传入了部分的参数，
        # 这是为了我们的方便，不重复传入不变的参数，而在__call__方法中会将func所需的参数全部传入	
        newkeywords = self.keywords.copy()
        newkeywords.update(keywords)
        # self.args是partial（func，*args，**kw）中的*args
        return self.func(*self.args, *args, **newkeywords)


    def partial(func, *args, **keywords):
        def newfunc(*fargs, **fkeywords):
            newkeywords = keywords.copy()
            newkeywords.update(fkeywords)
            return func(*args, *fargs, **newkeywords)
        newfunc.func = func
        newfunc.args = args
        newfunc.keywords = keywords
        return newfunc
