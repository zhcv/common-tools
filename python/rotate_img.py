# -*- coding: utf-8 -*-
import cv2
import os
import numpy as np
from PIL import Image
from PIL import ImageEnhance
from skimage import exposure

# 去除黑边的操作
crop_image = lambda img, x0, y0, w, h: img[x0:x0+w, y0:y0+h]  # 定义裁切函数，后续裁切黑边使用


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))


def rotate_image(img, angle, crop):
    """
    angle: 旋转的角度
    crop: 是否需要进行裁剪，布尔向量
    """
    w, h = img.shape[:2]
    # 旋转角度的周期是360°
    angle %= 360
    # 计算仿射变换矩阵
    M_rotation = cv2.getRotationMatrix2D((w / 2, h / 2), angle, 1)
    # 得到旋转后的图像
    img_rotated = cv2.warpAffine(img, M_rotation, (w, h))

    # 如果需要去除黑边
    if crop:
        # 裁剪角度的等效周期是180°
        angle_crop = angle % 180
        if angle > 90:
            angle_crop = 180 - angle_crop
        # 转化角度为弧度
        theta = angle_crop * np.pi / 180.
        # 计算高宽比
        hw_ratio = float(h) / float(w)
        # 计算裁剪边长系数的分子项
        tan_theta = np.tan(theta)
        numerator = np.cos(theta) + np.sin(theta) * np.tan(theta)

        # 计算分母中和高宽比相关的项
        r = hw_ratio if h > w else 1 / hw_ratio
        # 计算分母项
        denominator = r * tan_theta + 1
        # 最终的边长系数
        crop_mult = numerator / denominator

        # 得到裁剪区域
        w_crop = int(crop_mult * w)
        h_crop = int(crop_mult * h)
        x0 = int((w - w_crop) / 2)
        y0 = int((h - h_crop) / 2)
        print x0, y0, w_crop, h_crop

        img_rotated = crop_image(img_rotated.copy(), x0, y0, w_crop, h_crop)

    return img_rotated


def image_enhancement(image):
    # 直方图均衡
    array = np.uint8(image)
    imageh= exposure.equalize_adapthist(array) * 255
    im_hist = Image.fromarray(np.uint8(imageh))
    im_hist.show("hist")

    #亮度增强
    enh_bri = ImageEnhance.Brightness(image)
    brightness = 2
    image_brightened = enh_bri.enhance(brightness)
    image_brightened.show("brightness")

    #色度增强
    enh_col = ImageEnhance.Color(image)
    color = 2
    image_colored = enh_col.enhance(color)
    image_colored.show("color")

    #对比度增强
    enh_con = ImageEnhance.Contrast(image)
    contrast = 2
    image_contrasted = enh_con.enhance(contrast)
    image_contrasted.show("2 times more contrast")


if __name__ == "__main__":
    img = cv2.imread('0d8cddcf-45bf-415e-b365-1dcb29baa0de.png', 0)
    image_rotated = rotate_image(img, 7, True)
    # image_rotated = rotate_bound(img, 5)
    # img_rotated = crop_image(image_rotated, 50, 50, 1024, 1024)
    # cv2.imshow("test2", image_rotated)
