#! /usr/bin/env python
# -*- coding: utf-8 -*-

from scipy import misc
import os
import numpy as np
from img_utils import mls_affine_deformation_inv


"""
1: mls_affine_deformation_inv       train/png_warp1 trainannot/png_warp1
2: mls_rigid_deformation_inv        train/png_warp2 trainannot/png_warp2
3: mls_similarity_deformation_inv   train/png_warp3 trainannot/png_warp3

raw image_path train/png  trainannot/png
img_list: img_list.txt

"""


#img_lab_path = os.path.join(linux_data_dir, all_img_lab)


def generate_ctrl_pts(height, width, size=5, range=5):
    ''' Generate control points p and q. 
        p: size x size grids
        q: size x size grids with noise
        noise: from uniform distribution

    ### Params:
        * height - integer: height of the image
        * width - integer: width of the image
        * size - integer: grid size
        * range - bound of the uniform dirtribution, [-range, range]
    ### Return:
        A tuple of p and q
    '''
    x, y = np.meshgrid(np.linspace(0, height, size), np.linspace(0, width, size))
    p = np.concatenate((x.reshape(size**2, 1), y.reshape(size**2, 1)), axis=1)
    noise = np.random.uniform(-range, range, (size, size, 2))
    noise[[0, -1], :, :] = 0
    noise[:, [0,-1], :] = 0
    q = p + noise.reshape(size**2, 2)
    return p, q


if __name__ == "__main__":

    train_dir = "../training/image_2/"
    trainannot_dir = "../training/gt_image_2/"
    
    f = os.listdir(train_dir)
    for nu in f:
        
        train_png = os.path.join(train_dir,nu)
        trainannot_png = os.path.join(trainannot_dir,nu)
        train_png_new = '../training/image_p1/p1_' + nu
        trainannot_png_new = '../training/gt_image_p1/p1_' + nu
        #print(train_png," ",train_png_new)
        #print(trainannot_png," ",trainannot_png_new)
        
        train_img = misc.imread(train_png)
        trainannot_img = misc.imread(trainannot_png)

        height = 1024
        width = 1024
    
        p, q = generate_ctrl_pts(height, width, size=5, range=20)
        train_img_new = mls_affine_deformation_inv(train_img, p, q)
        trainannot_img_new = mls_affine_deformation_inv(trainannot_img, p, q)
        
        train_img_new = np.uint8(train_img_new * 255)
        trainannot_img_new = np.uint8(trainannot_img_new * 255)

        misc.imsave(train_png_new,train_img_new)
        misc.imsave(trainannot_png_new,trainannot_img_new)
        print nu



