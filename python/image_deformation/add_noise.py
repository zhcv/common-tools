#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np                                                                      
import os
import shutil
from PIL import Image, ImageEnhance                                                                                    
from scipy.misc import imread,imsave
from skimage.filters import gaussian


def add_noise(img_path)
    img = imread(img_path)
    lr = 0.2 * np.random.random()
    noisy = img + img.std() * lr
    
    return noise



def add_noise2(img, sigma):
    """
    Args: img: PIL.Image
    """
    sigma = 0.15 + random.random() * 1.15
    smooth = gaussian(np.array(img), sigma=sigma, multichannel=True)
    smooth *= 255
    noise = Image.fromarray(smooth.astype(np.uint8))

    return noise
