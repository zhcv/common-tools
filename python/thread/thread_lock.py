""" multi-threading execute take turns"""
import threading
import time


lockA = threading.Lock()
lockB = threading.Lock()


def printA(n):
    if n<0:
      return
    lockA.acquire()
    print("===" * 10)
    lockB.release()
    time.sleep(0.1)
    printA(n-1)
  

def printB(n):
    if n<0:  
      return
    lockB.acquire()
    print(">>>" * 10)
    lockA.release()
    time.sleep(0.2)
    printB(n-1) 

 
lockB.acquire()
t1 = threading.Thread(target=printA, args=(10,))
t2 = threading.Thread(target=printB, args=(10,))
t1.start()
t2.start()
t1.join()
t2.join()
