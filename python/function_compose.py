import functools

# one augment condation
def compose(*functions):
    def compose2(f, g):
        return lambda x: f(g(x))
    return reduce(compose2, functions, lambda x: x)


def compose(*functions):
    return reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)



def second(*args):
    return args[1]


def second_wrapper(lst):
    return second(*lst)


pipeline = compose(second_wrapper, list, range)
print pipeline(5)

def sub(a, b):
    return a - b

