# -*- coding: utf-8 -*-

import gc
import time

__author__ = "zhp9005@gmail.com"


class A():
    def __init__(self):
        print "object born, id: %s" % str(hex(id(self)))

    def __del__(self):
        print "object del, id: %s" % str(hex(id(self)))


def fn():
    """垃圾回收机制, 进程占用的内存基本不会变动"""
    cnt = 1
    while cnt < 10:
        c1 = A()
        del c1
        cnt += 1

def fn2():
    """循环引用导致内存泄露"""
    while True:
        c1 = A()
        c2 = A()
        c1.t = c2
        c2.t = c1
        del c1
        del c2


def fn3():
    """垃圾显式回收"""
    c1 = A()
    c2 = A()
    c1.t = c2
    c2.t = c1
    del c1
    del c2

    print gc.garbage
    print gc.collect()  # 显式执行垃圾回收
    print gc.garbage
    time.sleep(10)
"""
垃圾回收后的对象会放在gc.garbage列表里面
gc.collect()会返回不可达的对象数目，4等于两个对象以及它们对应的dict

有三种情况会触发垃圾回收：
    1.调用gc.collect(),
    2.当gc模块的计数器达到阀值的时候。
    3.程序退出的时候

gc模块提供一个接口给开发者设置垃圾回收的选项
采用引用计数的方法管理内存的一个缺陷是循环引用，而gc模块的一个主要功能就是解决循环引用的问题。
gc module 常用功能解析:
c.set_debug(flags)
设置gc的debug日志，一般设置为gc.DEBUG_LEAK
gc.collect([generation])
显式进行垃圾回收，可以输入参数，0代表只检查第一代的对象，1代表检查一，二代的对象，
2代表检查一，二，三代的对象，如果不传参数，执行一个full collection，也就是等于传2。
返回不可达（unreachable objects）对象的数目
gc.set_threshold(threshold0[, threshold1[, threshold2])
设置自动执行垃圾回收的频率。
gc.get_count()
获取当前自动执行垃圾回收的计数器，返回一个长度为3的列表

gc模块唯一处理不了的是循环引用的类都有__del__方法，所以项目中要避免定义__del__方法，
如果一定要使用该方法，同时导致了循环引用，需要代码显式调用gc.garbage里面的对象的__del__来打破僵局
"""


if __name__ == '__main__':
    gc.set_debug(gc.DEBUG_LEAK)  # 设置gc模块的日志
    fn3()
