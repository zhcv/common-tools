#!/usr/bin/env bash
# -*- coding: utf-8 -*-

import sys


def func(c):
    print "in func fucntion", sys.getrefcount(c) - 1
    for attr in dir(func):
        print attr, getattr(func, attr)


print "init", sys.getrefcount(11) - 1

a = 11
print "after a = 11", sys.getrefcount(11) -1
b = a
print "after b = 1", sys.getrefcount(11) - 1
func(a)
print "after func(a)", sys.getrefcount(11) - 1
list1 = [a, 12, 14]
print "after list1 = [a, 12, 14]", sys.getrefcount(11) - 1
a = 12
print "after a = 12", sys.getrefcount(11) - 1

del a
print "after del a", sys.getrefcount(11) - 1

del b
print "after del b", sys.getrefcount(11) - 1

# list1.pop(0)
# print "after pop list1", sys.getrefcount(11) - 1
del list1
print "after del list1", sys.getrefcount(11) -1


# 为什么调用函数会令引用计数+2 ?
# 函数也是对象，即使不在函数体内我们也可以调用函数的属性、方法
# 运行之可以发现func.__globals__属性中记录了全局变量键值对 {'a': 11} 这样，
# 这就是额外的计数来历：局部变量和全局变量的值是相同的，这导致计数+2.
