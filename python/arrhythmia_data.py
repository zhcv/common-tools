#!/usr/bin/env python


import os

src = "./MIT_BIH_Arrhythmia_Database"
fext = [".atr", ".dat", ".hea"]
hurl = r"http://physionet.org/physiobank/database/mitdb/"

for ext in fext:
    for index in range(100, 234):
        fname  = str(index) + ext
        fsave  = src + os.path.sep + fname
        fget   = hurl + fname
        if not os.path.isfile(fsave):
            command = "curl -o " + fsave + " " + fget
            # os.system(command)
            print command
