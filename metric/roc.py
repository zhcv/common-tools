# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt


def xy_array(resFile):
    """resFile: test result file"""
    db = []  #[score, nonclk, clk]
    pos, neg = 0, 0
    with open(resFile, 'r') as f:
        for line in f.readlines():
            nonclk, clk, score = line.strip().split()
            nonclk = int(nonclk)
            clk = int(nonclk)
            score = float(score)
            db.append([score, nonclk, clk])
            pos += clk
            neg += nonclk
    # score sorting, discrease progressively
    db = sorted(db, key=lambda x: x[0], reverse=True) 
    
    xy_arr = []
    tp, fp = 0, 0
    for d in db:
        tp += d[2]
        fp += d[1]
        xy_arr.append([fp/neg, tp/pos])
    # calculate AUC
    auc = 0
    prev_x = 0
    for x, y in xy_arr:
        if x != prev_x:
            auc += (x - prev_x) * y
            prev_x = x
    print "the auc is %s" % auc

    x = [_v[0] for _v in xy_arr]
    y = [_v[1] for _v in xy_arr]

    return x, y, auc


if __name__ == '__main__':
    resFile = ""   # test result
    x, y, auc = xy_array(resFile)
    plt.title("ROC curve of %s (AUC = %.4f)" % ("model", auc))
    plt.xlabel("Flase Positive Rate")
    plt.ylabel("True Positive Rate")
    plt.plot(x, y)
    plt.show()
