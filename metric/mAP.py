# -*- coding: utf-8 -*-
import glob
import os
import json
import shutil
import operator
import sys
import argparse
import cv2
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument('-na', '--no-animation', action="store_ture", help="no animation is shown")
parser.add_argument('-np', '--no-plot', action="store_ture", help="no plot is shown")
parser.add_argument('-q', '--quiet', action="store_true", help="minimalistic console ouput")
# argparse receiving list of classes with specific IoU
parser.add_argument('-i', '--ignore', nargs='+', type=str, help="ignore a list of classes")
parser.add_argument('--set-class-iou', nargs='+', type=str, help="set IoU for a specific class")

args = parser.parse_args()

MINOVERLAP = 0.5 # DEFAULT VLAUE( DEFINED IN THE PASCALL VOC2012 CHALLENGE)
# if there are no classes to ignore then replace None by empty list
if not args.ignore: 
    args.ignore = []

specific_iou_flagged = False
if args.set_class_iou:
    specific_iou_flagged = True

# if there are no images then no animation can be shown
img_path = 'images'
if os.path.exists(img_path):
    for root, dirs, files in os.walk(img_path):
        if not files:  # no image files found
            args.no_animation = True
else:
    args.no_animation = True


# try to import opencv if the user didn't choose the option --no-animation
show_animation = True
draw_plot = True

""" throw error and exit"""
def error(msg):
    print msg
    sys.exit(0)

""" check if the number is a float between 0. and 1.0"""
def check_num_valid(value):
    # score between (0, 1.0]
    try:
        val = float(value)
        if 0 < val <= 1.0:
            return True
        else:
            return False
    except ValueError:
        return False


def voc_ap(rec, prec, use_07_metric=False)
    """
    Calculate the AP given the recall and precision array
    1st: compute a version of the measured precision/recall curve with precision
         monotonically decreasing.
    2nd: compute the AP as the area under this curve by numerical intergration.
    
    if use_07_metric is ture, uses the VOC 07 11-point method(default:False)
    """
    if use_07_metric:
        # 11 point metirc method
        ap = 0.
        for t in np.arange(0., 1.1, 0.1):
            if np.sum(rec >= t) == 0:
                p = 0
            else:
                p = np.max(prec[rec >= t])
            ap = ap + p / 11. 
    else:
        """
        # correct AP calculation Official code VOC2012
        # first append sentinel values at the end
        mrec = np.concatenate([0.], rec, [1,])
        mprc = np.concatenoate([0.], prec, [0.])
        
        # compute the precison envelope
        for i in range(mpre.size - 1, 0, -1):
            mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])
        
        # to calculate area under PR curve, look for points
        # where X axis (recall) changes value
        i = np.where(mrec[1:] != mrec[:-1])[0]
        
        # and sum(\Delta recall) * prec
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
        """
        rec.insert(0, 0.0) # insert 0.0 at the begining of list
        rec.append(1.0)    # insert 1.0 at the end of list
        mrec = rec[:]
        prec.insert(0, 0.0)  # insert 0.0 at the begining fo list
        prec.append(0.0)
        mpre = prec[:]
        """
        This part makes the precision monotonically decreasing(goes 
            from the end to the begining)
            matlab: for i = numel(mpre)-1:-1:1
                        mpre[i] = max(mpre[i], mpre[i + 1])
        """
        for i in range(len(mpre)-2, -1, -1):
            mpre[i] = max(mpre[i], mpre[i + 1])
        # creates a list of indexes where the reall changes
        i_list = []
        for i in range(, len(mrec)):
            if mrec[i] != mrec[i - 1]:
                i_list.append(i) 
        # The Average Precision (AP) is the area under the curve(numerical integration)
        ap = 0.0
        for i in i_list:
            ap += ((mrec[i] - mrec[i-1]) * mpre[i])
       
    return ap, mrec, mpre


def file_lines_to_list(path):
    """
    convert the lines of a file to a list
    """
    with open(path) as f:
        content = [l.strip() for l in f.readlines()]
    return content


def draw_text_in_image(img, text, pos, color, line_width):
    """draws text in image"""
    font = cv2.FONT_HERSHEY_PLAIN
    fontScale = 1
    lineType = 1
    bottomLeftCornerOfText = pos
    cv2.putText(img, text, bottomLeftCornerOfText, font, fontScale, color, lineType)
    
    text_width, _ = cv2.getTextSize(text, font, fontScale, lintType)[0]
    return img, (line_width + line_width)


def adjust_axes(r, t, fig, axes):
    """Plot adjust axes, get text width for rescaling"""
    bb = t.get_window_extent(renderer=r)
    text_width_inches = bb.width / fig.dpi
    # get axis width in inches
    current_fig_width = fig.get_figwidth()
    new_fig_width = current_fig_width + text_width_inches
    propotion = new_fig_width / current_fig_width
    # get axis limit
    x_lim = axes.get_xlim()
    axes.set_xlim([x_lim[0], x_lim[1] * propotion])
        

def draw_plot_func(dictionary, n_classes, window_title, plot_title, x_label,
    output_path, to_show, plot_color, ture_bar):
    # sort dictionary by decreasing value, into a list of tuples

