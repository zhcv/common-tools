def my_preprocessing(train_data): 
    from sklearn import preprocessing 
    X_normalized = preprocessing.normalize(train_data ,norm = "l2",axis=0)#使用l2范式，对特征列进行正则 
    return X_normalized 

def my_feature_selection(data, target): 
    from sklearn.feature_selection import SelectKBest 
    from sklearn.feature_selection import chi2 
    data_new = SelectKBest(chi2, k= 50).fit_transform(data,target) 
    return data_new 


def my_PCA(data):
    #data without target, just train data, withou train target. 
    from sklearn import decomposition 
    pca_sklearn = decomposition.PCA() 
    pca_sklearn.fit(data) 
    main_var = pca_sklearn.explained_variance_ 
    print sum(main_var)*0.9 
    import matplotlib.pyplot as plt 
    n = 15 
    plt.plot(main_var[:n]) 
    plt.show() 

def clf_train(data,target): 
    from sklearn import svm 
    #from sklearn.linear_model import LogisticRegression 
    clf = svm.SVC(C=100,kernel="rbf",gamma=0.001) 
    clf.fit(data,target) 

    #clf_LR = LogisticRegression() 
    #clf_LR.fit(x_train, y_train) 
    #y_pred_LR = clf_LR.predict(x_test) 
    return clf 

def my_confusion_matrix(y_true, y_pred): 
    from sklearn.metrics import confusion_matrix 
    labels = list(set(y_true)) 
    conf_mat = confusion_matrix(y_true, y_pred, labels = labels) 
    print "confusion_matrix(left labels: y_true, up labels: y_pred):" 
    print "labels\t", 
    for i in range(len(labels)): 
        print labels[i],"\t", 
    print 
    for i in range(len(conf_mat)): 
	print i,"\t", 
	for j in range(len(conf_mat[i])): 
	    print conf_mat[i][j],'\t', 
	print 
    print 

def my_classification_report(y_true, y_pred): 
    from sklearn.metrics import classification_report 
    print "classification_report(left: labels):" 
    print classification_report(y_true, y_pred)
