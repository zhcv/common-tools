#include <unistd.h>
#include <iostream>

using namespace std;

int test(int argc, char **argv) {
    // start at 1 to skip the program name
    for (int i=1; i<argc; i++) {
        for (int j=0; argv[i][j] != '\0'; ++j) {
            cout << *(argv + i) << '\n'; // this and next line are the same
            cout << argv[i] << endl;
            cout << *(*(argv + i) + j) << endl;
        }
    }
}

int main(int argc, char** argv) {
    // how to pass pointers
    cout << argc << " parameters" << endl;
    test(argc, argv);

    return 0;
}
