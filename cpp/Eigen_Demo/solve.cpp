#include <Eigen/LU>
#include <Eigen/Dense>
#include <iostream>

// g++ -I/path/to/eigen/dir/include/eigen3 filename.cpp
// http://eigen.tuxfamily.org/dox/GettingStarted.html

using namespace Eigen;

int main()
{
  MatrixXd A = MatrixXd::Random(4,4);
  VectorXd b = VectorXd::Random(4);
  VectorXd x;
  x = A.lu().solve(b);
  std::cout << "A=" << A << std::endl << std::endl;
  std::cout << "x=" << x << std::endl << std::endl;
  std::cout << "b=" << b << std::endl << std::endl;
}
