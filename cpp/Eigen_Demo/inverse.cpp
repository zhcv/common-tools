#include <Eigen/LU> 
#include <Eigen/Dense> 
#include <iostream> 
using namespace Eigen; 
int main() 
{ 
  MatrixXd A = MatrixXd::Random(4,4); 
  MatrixXd Ainv; 
  Ainv = A.lu().inverse(); 
  std::cout << "A=" << A << std::endl << std::endl; 
  std::cout << "Ainv=" << Ainv << std::endl << std::endl; 
}
