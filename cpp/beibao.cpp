/*  求出将1元，5元，10元，20元凑成n(n>50)的方法的个数，并输出组合结果。
 *  F(N,M)=F(N,M-1)+F(N-VAL[M],M)
 *  F(N,M)表示 用不超过第M个值的数来表示N 的所有组合方案
 */
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main()  
{  
    int val[7] = { 0,1,2,5,10,20,50 };  
    int f[101][7];  
    memset(f, 0, sizeof(f));  
      
    for (int j = 0; j <= 6; j++)  
             f[0][j] = 1;  
      
    for (int j = 1; j <= 6; j++)  
    {  
        for (int i = 1; i <= 100; i++)  
        {  
            if (i - val[j] < 0)  
                f[i][j] = f[i][j - 1];  
        else  
                f[i][j] = f[i - val[j]][j] + f[i][j - 1];  
        }  
    }  
  
    cout << f[100][6] << endl;  
    system("pause");  
    return 0;  
}  
