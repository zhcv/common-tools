// 使用STL库sort函数对vector进行排序，vector的内容为对象的指针，而不是对象。

#include <stdio.h>
#include <vector>
#include <algorithm>

using namespace std;

class Elm
{
    public:
        int m_iSortProof;

    private:
        int __m_iValue;
        static int __m_iCnt;

    public:
        Elm();
        int getValue(int iX);
        void printElm();
};

int Elm::__m_iCnt = 0;

Elm::Elm()
{
    __m_iCnt ++;
    __m_iValue = __m_iCnt;
    m_iSortProof = getValue(__m_iCnt);
}

/* (x-10.3)^2 + 0.6*/
int Elm::getValue(int iX)
{
    float fX = (float)iX - 10.3;

    float fY = fX * fX + 0.6;

    return (int)fY;
}

void Elm::printElm()
{
    printf("value : %3d, proof : %3d\n", __m_iValue, m_iSortProof);
}

/*z -> a*/
bool compare(const Elm * a, const Elm * b)
{
    return a->m_iSortProof > b->m_iSortProof;
}

int main(int argc, char * argv[])
{
    vector<Elm *> vecpList;
    int i = 0;
    for(i = 0; i < 20; i++)
    {
        Elm * pElm = new Elm;
        vecpList.push_back(pElm);
    }
    for(vector<Elm *>::iterator pE = vecpList.begin(); pE != vecpList.end(); pE++)
    {
        (*pE)->printElm();
    }
    /*使用sort对vector进行排序*/
    sort(vecpList.begin(), vecpList.end(), compare);
    printf("\033[0;34m----------------sorted----------------\033[0m\n");
    for(vector<Elm *>::iterator pE = vecpList.begin(); pE != vecpList.end(); pE++)
    {
        (*pE)->printElm();
    }

    return 0;
}

