#include <boost/filesystem.hpp>
#include <iostream>

using namespace boost::filesystem;    


int main(int args, char** argv)
{
    struct recursive_directory_range
    {
        typedef recursive_directory_iterator iterator;
        recursive_directory_range(path p) : p_(p) {}

        iterator begin() { return recursive_directory_iterator(p_); }
        iterator end() { return recursive_directory_iterator(); }

        path p_;
    };

    // for (auto it : recursive_directory_range(dir_path))
    for (auto it : recursive_directory_range("../"))
    {
        std::cout << it << std::endl;
    }
    
    return 0;
}
