/*
 * =====================================================================================
 *
 *       Filename:  cin_string.cpp
 *
 *    Description:  cin with string
 *
 *        Version:  1.0
 *        Created:  02/21/2019 06:08:47 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
// cin with strings

#include <iostream>
#include <string>

using namespace std;

int main ()
{
  string mystr;
  cout << "What's your name? ";
  getline (cin, mystr);
  cout << "Hello " << mystr << ".\n";
  cout << "What is your favorite team? ";
  getline (cin, mystr);
  cout << "I like " << mystr << " too!\n";
  return 0;
}
