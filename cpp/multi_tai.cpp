#include <iostream>
#include <stdlib.h>

using namespace std;

class Father
{
public:
    void Face()
    {
        cout << "Father's face" << endl;
    }

    virtual void Say()
    {
        cout << "Father say hello" << endl;
    }
};


class Son: public Father
{
    public:     
        void Say()
        {
            cout << "Son say hello" << endl;
        }
};

int main(int argc, char *argv[])
{
    Son son;
    Father *pFather = &son; // 隐式类型转换
    pFather->Say();

    return 0;
}

/*  Default call Father's func if Fahter's func does't has vitual before func's name
 *  if key virtual before Father's func name, default call Son's func(The premise 
 *  is that son Class has this func, if son does't has this func , 
 *  also call father's func)
 *  */
