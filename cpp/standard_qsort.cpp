#include <stdio.h>
#include <stdlib.h>
typedef struct
{
    int date;
    int value;
}X_S;

/* a->z */
int cmpfunA2Z(const void * a, const void * b)
{
    X_S * p1 = (X_S *)a;
    X_S * p2 = (X_S *)b;

    return p1->value > p2->value;
}

/* z -> a */
int cmpfunZ2A(const void * a, const void * b)
{
    X_S * p1 = (X_S *)a;
    X_S * p2 = (X_S *)b;

    return p1->value < p2->value;
}

int quickSortOfCpp()
{
    X_S Xlist[100];
    int Ilist[100];
    int i = 0;
    for(i = 0; i < 100; i++)
    {
        Xlist[i].date = i+1;
        double t = (double)i - 50.3;
        Xlist[i].value = (int)(t * t + 5.6);
    }

    for(i = 0; i < 100; i++)
    {
        printf("num : %3d, value : %4d\n", Xlist[i].date, Xlist[i].value);
    }

    qsort(Xlist, 100, sizeof(X_S), cmpfunA2Z);
    printf("\033[034m-------------sorted-------------\033[0m\n");

    for(i = 0; i < 100; i++)
    {
        printf("num : %3d, value : %4d\n", Xlist[i].date, Xlist[i].value);
    }

    return 0;
}

int main(int argc, char * argv[])
{
    quickSortOfCpp();

    return 0;
}
