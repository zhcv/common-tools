
Node *SelectSort(Node *L)
{
    Node *p, *q, *small;
    int temp;

    for(p->L-next; p->next != NULL; p=p->next) 
    {   // 每次循环找出最小值，将最小值交换到第一位，然后指针向后移动一位
        small = p;
        for (q=p->next; q; q=q->next) // 由前向后遍历，找出最小的节点
        {   
            if(q->data < small->data)
            small = q;
        }
    }
    if(small != p)
    {
        temp = p->data;
        p->data = small->data;
        small->data = temp;
    }

    return L;
}
