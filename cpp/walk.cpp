#include <stdio.h>
#include <stdlib.h>
#include <sys/io.h>
#include <string>

using namespace std;

int main()
{
	//目标文件夹路径
	string inPath = "*.cpp"; //遍历文件夹下的所有.jpg文件
	//用于查找的句柄
	long handle;
	struct _finddata_t fileinfo;
	//第一次查找
	handle = _findfirst(inPath.c_str(), &fileinfo);
	if (handle == -1)
		return -1;
	do
	{
		//找到的文件的文件名
		printf("%s\n", fileinfo.name);
 
	} while (!_findnext(handle, &fileinfo));
 
	_findclose(handle);
	pause();
	return 0;
}
