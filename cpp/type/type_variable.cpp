/*
 * =====================================================================================
 *
 *       Filename:  type_variable.cpp
 *
 *    Description:  see variable type
 *
 *        Version:  1.0
 *        Created:  02/21/2019 04:40:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include<typeinfo>
#include<iostream>
 
using namespace std;
 
int main(){
    int a;
    char b;
    double d;
    // Look variable defualt value
    cout << "a = " << a << endl;
    cout << "b = " << b << endl;
    cout << "d = " << d << endl;

    cout<<"a: "<<typeid(a).name()<<endl;
    cout<<"b: "<<typeid(b).name()<<endl;
    cout<<"d: "<<typeid(d).name()<<endl;
}
