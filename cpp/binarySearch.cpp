/* 
归的二分查找 
arrat：数组 ， low:上界；  high:下界;  target:查找的数据； 返回target所在数组的下标  
*/  
int binarySearch(int array[], int low, int high, int target) {  
    int middle = (low + high)/2;  
    if(low > high) {  
        return -1;  
    }  
    if(target == array[middle]) {  
        return middle;  
    }  
    if(target < array[middle]) {  
        return binarySearch(array, low, middle-1, target);  
    }  
    if(target > array[middle]) {  
        return binarySearch(array, middle+1, high, target);  
    }   
}  


/* 
非递归的二分查找  
arrat：数组 ， n:数组的大小;  target:查找的数据； 返回target所在数组的下标  
*/  
int binarySearch2(int array[], int n, int target) {  
    int low = 0, high = n, middle = 0;  
    while(low < high) {  
        middle = (low + high)/2;  
        if(target == array[middle]) {  
            return middle;  
        } else if(target < array[middle]) {  
            high = middle;  
        } else if(target > array[middle]) {  
            low = middle + 1;  
        }  
    }  
    return -1;  
}  
