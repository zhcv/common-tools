#include <stdio.h>
#include <stdlib.h>


void I_hope(char heart)
{
    float x,y,a;
    for(y = 1.5f; y>-1.5f; y-=0.1f)
    {
        for(x=-1.5f; x<1.5f; x+=0.05f)
        {
            a = x*x+y*y-1;
            putchar (a*a*a-x*x*y*y*y <= 0.0f ? heart : ' ');
        }
        putchar('\n');
    }
}


int main(int argc, char **argv)
{
    char *me;
    me = "u";
    I_hope(*me);
    return 0;
}
