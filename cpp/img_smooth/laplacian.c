#include<iostream>
#include<opencv2/core/core.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<cv.h>

using namespace cv;

// g++ `pkg-config opencv --cflags` laplacian.c  -o lap `pkg-config opencv --libs`

//拉普拉斯锐化图像
IplImage *Laplacian_cx(IplImage *src){
    IplImage *dst = cvCreateImage(cvGetSize(src),IPL_DEPTH_8U,0);
    cvLaplace(src,dst);
    return dst;
}

int main(int argc, char* argv[]){
    // char filename[] =  argv[1]
    IplImage * srcImage = cvLoadImage(argv[1], 0);
    IplImage * lapImage = Laplacian_cx(srcImage);    
    // CvArr *dst;
    IplImage *dstImage = cvCreateImage(cvGetSize(srcImage),IPL_DEPTH_8U,0);
    cvSub(srcImage, lapImage, dstImage);    

    //窗口定义函数
    cvNamedWindow("源灰度图像",CV_WINDOW_AUTOSIZE);
    //显示源图像
    cvShowImage("源灰度图像",srcImage);

    //显示Laplace变换后的灰度图像
    cvNamedWindow("Laplacian变换后的灰度图像",CV_WINDOW_AUTOSIZE);
    cvShowImage("Laplacian变换后的灰度图像",Laplacian_cx(lapImage));

    //显示Laplace变换后差分图像
    // cvNamedWindow("Laplacian变换后的差分灰度图像",CV_WINDOW_AUTOSIZE);
    // cvShowImage("Laplacian变换后的差分灰度图像", dstImage);
    cvSaveImage("demo.jpg", dstImage);
    // 等待60000 ms后窗口自动关闭
    waitKey(60000);

}
