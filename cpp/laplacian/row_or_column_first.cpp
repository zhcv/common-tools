#include <iostream>
#include <Eigen/Dense>

// Matlab，Fortune 列优先
// c/c++默认行优先，CUDA, numpy,scipy 都是行优先
// 对于行优先遍历的，使用行优先存储，对于列优先遍历的，使用列优先存储。
// Eigen默认使用列优先存储。

using namespace Eigen;
using namespace std;
int main() {
    Matrix<int, 2, 3, ColMajor> Acolmajor;
    // Acolmajor << 8, 2, 2, 9,
    //              9, 1, 4, 4,
    //              3, 5, 4, 5;
    Acolmajor << 1, 2, 3, 4, 5, 6;
    cout << Acolmajor << endl;
    cout << "In memory" << endl;
    for (int i = 0; i < 6; i++)
        cout << *(Acolmajor.data() + i) << endl;

    Matrix<int, 2, 3, RowMajor> Arowmajor;
    Arowmajor << 1, 2, 3, 4, 5, 6;
    cout << Arowmajor << endl;
    cout << "In memory" << endl;
    for (int i = 0; i < 6; i++)
    cout << *(Arowmajor.data() + i) << endl;

    return 0;
}

