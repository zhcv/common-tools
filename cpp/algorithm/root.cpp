#include <iostream>
#include <math.h>

using namespace std;

float sqrRoot(float a)
{
  if (a == 1 || a == 0)
      return a;
  if (a < 0)
  {
      cout << "input data sholud be nonnegative" << endl;
      return -1;
  }
  float eps = 0.0001;
  float left, mid , right, diff;
  if (a > 1)
  {
      left = 1;
      right = a;
  }
  else
  {
    left = a;
    right = 1;
  }
  do {
    mid = (left + right) / 2;
    diff = mid * mid - a;
    if (diff > 0)
      right = mid;
    else
      left = mid;
  } while(fabs(diff) > eps);
  return mid;
}

// To execute C++, please define "int main()"
int main() {
  auto words = { "Hello, ", "World!", "\n" };
  for (const string& word : words) {
    cout << word;
  }
  float r;
  cout << "Please number:";
  cin >> r;
  cout << "sqrt " << r << " = " << sqrRoot(r) << endl;
  return 0;
}
