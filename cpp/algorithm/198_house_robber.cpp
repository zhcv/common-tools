/*
 * Example 1:
 * 
 * Input: [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 *              Total amount you can rob = 1 + 3 = 4.
 * 
 * Example 2:
 * 
 * Input: [2,7,9,3,1]
 * Output: 12
 * Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house 5 (money = 1).
 *              Total amount you can rob = 2 + 9 + 1 = 12.
 */
// Time complexity = O(n)
// Space complexity  = O(n)
 
// CPP program to find the maximum stolen value 
#include <iostream> 
using namespace std; 

// calculate the maximum stolen value 
int maxLoot(int *hval, int n) 
{ 
	if (n == 0) 
		return 0; 
	if (n == 1) 
		return hval[0]; 
	if (n == 2) 
		return max(hval[0], hval[1]); 

	// dp[i] represent the maximum value stolen 
	// so far after reaching house i. 
	int dp[n]; 

	// Initialize the dp[0] and dp[1] 
	dp[0] = hval[0]; 
	dp[1] = max(hval[0], hval[1]); 

	// Fill remaining positions 
	for (int i = 2; i<n; i++) 
		dp[i] = max(hval[i]+dp[i-2], dp[i-1]); 

	return dp[n-1]; 
} 

// Driver to test above code 
int main() 
{ 
	int hval[] = {6, 7, 1, 3, 8, 2, 4}; 
	int n = sizeof(hval)/sizeof(hval[0]); 
	cout << "Maximum loot possible : "
		 << maxLoot(hval, n)
         << endl;
	return 0; 
} 
