#include<iostream>  
#include<cmath>  

using namespace std;  

int findMinDiff(int a[], int n){
    int sum =0;
    for(int i=1;i<=n;i++){
        sum += a[i];
    }
    int res[sum+1] = {0};
    for(int i=1;i<=n;i++){
        for(int j = sum/2;j>=a[i];j--)
            res[j] = max(res[j],res[j-a[i]]+a[i]);
    }
    return abs(sum - res[sum/2] - res[sum/2]);
}
int main()
{
    int n;
    cin >> n;
    int a[n+1];
    a[0]=0;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    cout << findMinDiff(a,n) << endl;;
    return 0;
}
