// 'a' --> 0; 'B' -- > 1; ; length[0:end]
// C++ program to demonstrate that we can construct bitset using 
// alternate characters for set and unset bits. 
#include <bitset> 
#include <string> 
#include <iostream> 
  
int main()  
{ 
    // string constructor using custom zero/one digits 
    std::string alpha_bit_string = "aBaaBBaB"; 
    std::bitset<8> b1(alpha_bit_string, 0, alpha_bit_string.size(), 
                      'a', 'B');         // [0,1,0,0,1,1,0,1] 
  
    std::cout << b1 << '\n'; 
} 

