#include <iostream>
#include <cstdio>
#include <cstring>                  //for memset 
#include <algorithm>                //for max
using namespace std;
const int M = 100 + 10;
int w[M];
int dp[M*100];
bool state[M][M];
int main()
{
    int n;
    while (scanf("%d", &n) != EOF) {
        int sum = 0;
        for (int i = 0; i < n; ++i) {
            scanf("%d", &w[i]);
            sum += w[i];
        }
        memset(dp, 0, sizeof(dp));
        memset(state, 0, sizeof(state));
        for (int i = 0; i < n; ++i)
            for (int j = sum/2; j >= w[i]; --j) {
                if (dp[j] < dp[j-w[i]] + w[i]) {
                    dp[j] = dp[j-w[i]] + w[i];
                    state[i][j] = true;
                }
            }
        printf("%d\n", sum - dp[sum/2]*2);
        int i = n, j = sum/2;
        while (i--) {
            if (state[i][j]) {
                printf("%d ", w[i]);
                j -= w[i];
            }
        }
        printf("\n");
    }
    return 0;
}
