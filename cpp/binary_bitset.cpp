// b(42): binary: 00101010 
// C++ program to demonstrate that we can convert contents 
// of bitset to a string. character replace.
#include <iostream> 
#include <bitset> 
  
int main() 
{ 
    std::bitset<8> b(42); 
    std::cout << b.to_string() << '\n'
              << b.to_string('*') << '\n'
              << b.to_string('O', 'X') << '\n'; 
} 

