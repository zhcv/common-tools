# method2
\Figure[t!](topskip=0pt,botskip=0pt, midskip=0pt)[scale=0.24]{fig279.png}
{The categorization of local feature descriptors and algorithms.\label{fig1}}


# method2
\begin{figure}
\begin{center}
%\vspace{10cm}
\includegraphics[scale=0.5]{fig279.png}
\end{center}
\caption{xxxx } \label{fig2}
\end{figure}



# LaTeX 设置字体颜色

```latex
\usepackage{color}


{\color{red} text}

\textcolor[rgb]{1,0,0}{text}
```
另外，使用宏包xcolor也可以实现：
```\textcolor{red/blue/green/black/white/cyan/magenta/yellow}{text}```
其中textcolor{}中包含的是系统定义好的颜色。
