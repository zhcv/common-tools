# Checking the integrity of all JPG files in a directory

```if not install jpeginfo: sudo apt-get install jpeginfo```

```bash
cd photos
find -iname “*.jpg” -print0 | xargs -0 jpeginfo -c | grep -e WARNING -e ERROR
```

```
find . -name "*jpg" -exec jpeginfo -c {} \; | grep -E "WARNING|ERROR"
```


Finds all corrupted jpeg files in current directory and its subdirectories. 
Displays the error or warning found. The jpeginfo is part of the jpeginfo 
package in debian. Should you wish to only get corrupted filenames, use cut 
to extract them:
```
find ./ -name *jpg -exec jpeginfo -c {} \; | grep -E "WARNING|ERROR" | cut -d " " -f 1

Simple Output:
./0520-185324.jpg  Corrupt JPEG data: 132 extraneous bytes before marker 0xc0 
1024 x 768  24bit Exif  N  341535  Quantization table 0x00 was not defined  [ERROR]
```

```
find . -iname '*jpg' -print0 | xargs -0 exiftool -warning; find . -iname '*jpg' -print0 | xargs -0 jpeginfo -c
find . -iname '*.jpg' -print0 | xargs -0 jpeginfo -c
find . -iname '*.jpg' -print0 | xargs -0 exiftool -warning
```
