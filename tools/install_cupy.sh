#!/usr/bin/env bash

# install chainer details, https://cupy.chainer.org/

export CHAINER_BUILD_CHAINERX=1
export CHAINERX_BUILD_CUDA=1
export CUDNN_ROOT_DIR=/usr/local/cuda/lib64
export MAKEFLAGS=-j8    # Using 8 parallel jobs.

# pip install -U setuptools pip

pip install --pre cupy-cuda90
pip install --pre chainer
