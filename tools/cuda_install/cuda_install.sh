#!/usr/bin/env bash

<<!
--silent — this will force installer to do everything in a silent mode without any 
           interactive prompt. Really useful for the automation

--toolkit — install only the toolkit, majority of users probably indeed need only toolkit

--toolkitpath — this is where all the magic starts, each cuda that we’re going to install 
                needs to be installed in its own separate folder, in our example CUDA9 is 
                installed in /usr/local/cuda-9.0, therefore CUDA8 will be installed in 
                /usr/local/cuda-8, CUDA9.1 can go to /usr/local/cuda-9.1 , etc
!


sudo sh cuda-9.1.run --silent --toolkit --toolkitpath=/usr/local/cuda-9.1
