#!/usr/bin/env bash


LIB=/usr/local/cuda/lib64/
INCLUDE=/usr/local/cuda/include/

# sudo rm -rf $LIB/libcudnn*
sudo rm -rf #INCLUDE/cudnn.h


sudo cp -r cudnn_v$1/cuda/lib64/* $LIB
sudo cp -r cudnn_v$1/cuda/include/* $INCLUDE
