#!/usr/bin/env bash

# Usage: source cuda_env.sh

# source /home/zhp/tf2/bin/activate

alias nvcc='/usr/local/cuda-10.1/bin/nvcc'

export CUDA_HOME="/usr/local/cuda-10.1/bin"

export LD_LIBRARY_PATH="/usr/local/cuda-10.1/lib64":"/usr/local/cuda-10.1/extras/CUPTI/lib64"

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/usr/local/cuda-10.0/bin
