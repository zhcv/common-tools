# =============================================================================
r"""
# Window Width & Window Level, image enhancement
win_min = (2*window_center - window_width)/2.0 + 0.5  
win_max = (2*window_center + window_width)/2.0 + 0.5    

# here, mapping: 8bit 0~255 gray image
nPixelVal = [(pixel_val - win_min)/(win_max - win_min) ]*255 

窗位窗宽可根据数据特征和自身需求手动设置，本帖中博主使用为CT数据，故选择常用窗宽
400，窗位1200参数。

# Image normalization processing
# [1] linear function, express formula
# y=(x-MinValue)/(MaxValue-MinValue)
说明：x、y分别为转换前、后的值，MaxValue、MinValue分别为样本的最大值和最小值。
(2)对数函数转换，表达式如下：
y=log10(x)
说明：以10为底的对数函数转换。
(3)反余切函数转换，表达式如下：
y=atan(x)*2/PI
(4)一个归一化代码.
I=double(I);
maxvalue=max(max(I)) % max
在把矩阵每列的最大值找到，并组成一个单行的数组，转置一下就会行转换为列，再max就
求一个最大的值，如果不转置，只能求出每列的最大值。
f = 1 - I/maxvalue; %为什么要用1去减？
Image1=f;

# computer ww, wl    
windowCenter = (min + max + 1) / 2 * rescaleSlope + rescaleIntercept
windowWidth = Math.abs((max + 1 - min) * rescaleSlope)



"""
# =============================================================================
import numpy as np
import matplotlib.pyplot as plt
import random
import math

#-----------data-----------#
rawData = np.fromfile('CDH_Low.raw', dtype=np.int16) #read .raw data
# window-level Processing
rawData_win=np.zeros(rawData.shape,dtype='float32')
window_center=1200 # window-level
window_width=400  # window-width
win_min = (2*window_center - window_width)/2.0 + 0.5
win_max = (2*window_center + window_width)/2.0 + 0.5
dFactor = 255.0/(win_max - win_min)
nNumPixels=rawData.shape[0]
for i in range(nNumPixels):
    pixel_val = rawData[i]
    if pixel_val < win_min:
        disp_pixel_val = 0
        continue
    if pixel_val > win_max:
        disp_pixel_val = 25
        continue
    nPixelVal = (pixel_val - win_min)*dFactor
    if nPixelVal < 0:
        disp_pixel_val = 0
    elif nPixelVal > 255:
        disp_pixel_val = 255
    else:
        disp_pixel_val = nPixelVal
    rawData_win[i]=disp_pixel_val
# normalize processing
rawData_win = (rawData_win - np.min(rawData_win)) / (np.max(rawData_win) - np.min(rawData_win))
