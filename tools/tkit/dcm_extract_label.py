# -*- coding: utf-8 -*-
import dicom
import os, json, argparse, sys


class File:
    def __init__(self, fileName, **kwargs):
        self._fileName = fileName
        self._ds = None
        self._data = None
        try:
            self._ds = dicom.read_file(self._fileName, **kwargs)
        except:
            raise FileNotFoundError('Failed to open ' + self._fileName)

        if [0x6000, 0x0800] in self._ds:
            self._data = Data(self._ds[0x6000, 0x0800].value)

    @property
    def data(self):
        return self._data
    
    def __repr__(self):
        if not self._ds:
            return "Couldn't open %s" % self._fileName
        text = "%s: %d rows, %d columns, " % (self._fileName, self._ds.Rows, self._ds.Columns)

        if not self._data or len(self.data) == 0:
            return text + "no focus data"
        return text + "%d focuses: %s" % (len(self.data), self.data.focusNames())

class FileList:
    def __init__(self, listFileName):
        self._listFileName = listFileName
        pass

    def build(self, dcmRootDir, overwrite=False, reportFunc=None, foundFileFunc=None):
        if not overwrite and os.path.exists(self._listFileName):
            if reportFunc:
                reportFunc('No build due to %s already exists!' % self._listFileName)
            return False

        if reportFunc:
            reportFunc('Building DCM file list from ' + dcmRootDir)

        try:
            file = open(self._listFileName, 'wt', encoding='utf-8')
        except:
            if reportFunc:
                reportFunc('Failed to write to list file ' + self._listFileName)
            return False

        tfc, mfc = 0, 0
        for root, dirs, files in os.walk(dcmRootDir):
            for f in files:
                if f.lower().endswith('.dcm'):
                    fn = os.path.join(root, f)

                    if not os.path.isfile(fn):
                        print("ABORT! ", fn)
                        sys.exit()

                    jf = File(fn, stop_before_pixels=True)

                    tfc += 1    # total file count
                    if reportFunc and tfc % 100 == 0:
                        reportFunc("...... %d DCM files found so far, %d files with focuses" % (tfc, mfc))

                    if not jf.data or len(jf.data) == 0:
                        if foundFileFunc:
                            foundFileFunc(fn, False)    # found a file without focus data
                        file.write(fn + '\n')
                    else:
                        if foundFileFunc:
                            foundFileFunc(fn, True)  # found a file with focus data
                        mfc += 1    # count of file with focusest
                        file.write(fn + '|' + jf.data._json + '\n')

        if reportFunc:
            reportFunc('Found %d DCM files, %d files with focus data, list \
                saved to %s' % (tfc, mfc, self._listFileName ))

        file.close()
        return True

    def listAll(self, withFocusOnly=True):
        with open(self._listFileName, 'rt', encoding='utf-8') as file:
            for line in file:
                ix = line.find('|')
                if ix < 0:
                    if not withFocusOnly:
                        yield (line, None)
                else:
                    yield (line[0:ix], line[ix+1:])

class Data:
    def __init__(self, jsonStr):
        self._json = jsonStr
        self._index = 0
        self._data = None

        try:
            self._data = json.loads(jsonStr)
        except:
            self._json = ''  # reset data on error
            raise TypeError('%s is invalid JSON data' % jsonStr)

    def __len__(self):
        return int(self._data['markCount']) if self._data and 'markCount'in self._data else 0

    def __getitem__(self, index):
        if index < 0 or index >= len(self):
            raise StopIteration
        return Focus(self._data['marks'][index])

    def __iter__(self):
        return self

    def __next__(self):
        o = self.__getitem__(self._index)
        self._index += 1
        return o

    def focusNames(self):
        s = ''
        for m in self:
            s += m.name + ' '
        return s

    def __repr__(self):
        return "%d focuses: %s" % (len(self), self.focusNames()) if self._ds else "No focus data"

class Focus:
    _namesDict = {
        'ChenJiuBingZao': '陈旧病灶',
        'FeiBuGanRan': '肺部感染',
        'FeiDaPao': '肺大泡',
        'FeiQiZhong': '肺气肿',
        'JieJieBingZao(<3cm)': '结节病灶 < 3cm',
        'FeiBuZhongKuai(>=3cm)': '肺部肿块 >= 3cm',
        'LeiGuGuZhe': '肋骨骨折',   ''
        'QiXiong': '气胸',
        'SuLiBingZao': '粟粒性病灶',
        'KeYiBingBian': '可疑病变',
        'JianZhiXingGaiBian': '间质性改变',
        'KongDong': '空洞',
        'KongQiang': '空腔',       
        'ZhiQiGuanFeiYan': '支气管肺炎',
        'XinYingZengDa': '心影增大',
        'YeQiXiong': '液气胸',
        'FeiBuZhang': '肺不张',
        'SuoGuGuZhe': '锁骨骨折',
        'ManZhiBanGanRan': '慢支伴感染',
        'FeiQiZhongBanGanRan': '肺气肿伴感染',
        'JianZhiXingGaiBianBanGanRan': '间质性改变伴感染',
        'ChenJiuBingZaoBanGanRan': '陈旧病灶伴感染',
        'XiongKuoTaXian': '胸廓塌陷',
        'XiongMoZengHou': '胸膜增厚',
        'XiongQiangJiYe': '胸腔积液',
    }
    
    def __init__(self, focus):
        self._focus = focus

    @property
    def typeName(self):
        return self._focus['type']
    
    @property
    def name(self):
        return Focus._namesDict[self.typeName] if self.typeName in Focus._namesDict else ''
    
    @property
    def angle(self):
        return float(self._focus['angle'])
    
    @property
    def topLeft(self):
        return self._focus['tl']
        
    @property
    def topRight(self):
        return self._focus['tr']
    
    @property
    def bottomLeft(self):
        return self._focus['bl']
    
    @property
    def bottomRight(self):
        return self._focus['br']
    
    @property
    def width(self):
        return self.topRight['x'] - self.topLeft['x']
    
    @property
    def height(self):
        return self.bottomLeft['y'] - self.topLeft['y']

    @staticmethod
    def getName(typeName):
        return Focus._namesDict[typeName] if typeName and typeName in Focus._namesDict else ''

    @staticmethod
    def findName(name):
        if not name or len(name) == 0:
            return (None, None)

        if name.lower() == 'null':
            return ('null', 'null')

        for n in Focus._namesDict.keys():
            if n.lower() == name.lower() or n.lower() == Focus._namesDict[n].lower():
                return (n,  Focus._namesDict[n])
            else:
                if name.upper() == ''.join([a for a in n if a.isupper()]):
                    return (n,  Focus._namesDict[n])
        return (None, None)

if __name__ == '__main__':
    pass
    
