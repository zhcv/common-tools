from __future__ import absolute_import
from __future__ import division
# from __future__ import print_function

import socket
import datetime
import tempfile
import os
import uuid
import hashlib
import urllib
# pylint:disable=g-import-not-at-top
try:
  import urllib.request as url
  import urllib.parse as urlparse
  from urllib.parse import urlencode
except ImportError:
  import urllib2 as url
  from urllib import urlencode
  import urlparse

import tensorflow as tf

_COMPRESSED_FORMAT_QUERY = ("tf-hub-format", "compressed")
_CACHE_DIR = os.path.join(tempfile.gettempdir(), "tfhub_modules")


def create_local_module_dir(cache_dir, module_name):
  """Creates and returns the name of directory where to cache a module."""
  tf.gfile.MakeDirs(cache_dir)
  return os.path.join(cache_dir, module_name)



def _module_dir(handle):
  """Returns the directory where to cache the module."""
  return create_local_module_dir(
      _CACHE_DIR,
      hashlib.sha1(handle.encode("utf8")).hexdigest())



def _append_compressed_format_query(hub_site):
    # convert the tuple from urlparse into list so it can be updated in place.
    parsed = list(urlparse.urlparse(hub_site))
    qsl = urlparse.parse_qsl(parsed[4])
    qsl.append(_COMPRESSED_FORMAT_QUERY)
    parsed[4] = urlencode(qsl)
    return urlparse.urlunparse(parsed)

def atomic_write_string_to_file(filename, contents, overwrite):
  """Writes to `filename` atomically.

  This means that when `filename` appears in the filesystem, it will contain
  all of `contents`. With write_string_to_file, it is possible for the file
  to appear in the filesystem with `contents` only partially written.

  Accomplished by writing to a temp file and then renaming it.

  Args:
    filename: string, pathname for a file
    contents: string, contents that need to be written to the file
    overwrite: boolean, if false it's an error for `filename` to be occupied by
      an existing file.
  """
  temp_pathname = (tf.compat.as_bytes(filename) +
                   tf.compat.as_bytes(".tmp") +
                   tf.compat.as_bytes(uuid.uuid4().hex))
  with tf.gfile.GFile(temp_pathname, mode="w") as f:
    f.write(contents)
  try:
    tf.gfile.Rename(temp_pathname, filename, overwrite)
  except tf.errors.OpError:
    tf.gfile.Remove(temp_pathname)
    raise

def write_module_descriptor(handle, module_dir):
    module_descriptor = module_dir + '.descriptor.txt'
    readme = os.path.join(_CACHE_DIR, module_descriptor)
    readme_content = (
        "Module: %s\nDownload Time: %s\nDownloader Hostname: %s (PID:%d)" %
        (handle, str(datetime.datetime.today()), socket.gethostname(),
        os.getpid()))
    """
    The descriptor file has no semantic meaning so we allow 'overwrite' since
    there is a chance that another process might have written the file (and
    crashed), we just overwrite it.)
    """
    atomic_write_string_to_file(readme, readme_content, overwrite=True)

if __name__ == '__main__':
    tar_name = "1.tar.gz"
    # download link (redirect): hub_site >> google_api
    google_api = "https://storage.googleapis.com/tfhub-modules/google/imagenet/resnet_v2_101/classification/1.tar.gz"
    google_api_head = "https://storage.googleapis.com/tfhub-modules/"
    module_dict = {
        "mobilenet":"https://tfhub.dev/google/imagenet/mobilenet_v1_100_224/classification/1",
        "inception":"https://tfhub.dev/google/imagenet/inception_v3/classification/1",
        "resnet_v2":"https://tfhub.dev/google/imagenet/resnet_v2_101/classification/1",
    }

    hub_site = module_dict['resnet_v2']
    tar_url = _append_compressed_format_query(hub_site)
    module_dir = _module_dir(hub_site)
    write_module_descriptor(hub_site, module_dir)    
    
    module_tar_url = google_api_head + hub_site.split('tfhub.dev/')[-1] + '.tar.gz'
    print tar_url
    print module_tar_url
    print module_dir
