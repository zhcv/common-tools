# dicom infomation


* Tag (0028, 0004) Keyword PhotometricInterpretation (MONOCHROME1 and MONOCHROME2)
* Tag (0028, 1050) Keyword WindowCenter
* Tag (0028, 1051) Keyword WindowWidth

* Tag (0028, 1056) VOI LUT Function 


## Note, thest attributes are applied according to the following pseudo-code, where x is
## is the input value, y is an output value with a range from ymin to ymax, c is Window
## Center(0028, 1050) and w is Window Width(0028, 1051):
* if(x < c-0.5-(w-1)/2),then y = ymin
* else if(x > c-0.5 + (w-1)/2),then y = ymax
* else y = ((x - (c-0.5))/(w-1) + 0.5) * (ymax-ymin) + ymin

# Note, For example, for an output range 0 to 255:
* c=2048, w=4096
if(x <=0 ) then y = 0
else if(x>4095) then y = 255
else y = ((x-2047.5) / 4095 + 0.5) * (255.0) + 0

* c=2048, w=1 becomes:
if(x<=2047.5) then y =0
else if(x>2047.5) then y = 255
else /* not reached */ 

* c=0, w=100 becomes:
if (x<=-50) then y = 0
else if(x>49) then y = 255
else y = ((x+0.5)/99+0.5) * (255.0) + 0

* c=0, w=1 becomes:
if(x <= -0.5) then y = 0
else if(x >-0.5) then y = 255
else /* not reached */

# 将所有数据映射到a~-b, 
* y = (x -min) / (max -min) * (b - a) + a

standard method

y = (x - x_mean) / standerr
