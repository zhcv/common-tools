"""Convert DICOM images using Python image Library (PIL)

Usage:
>>> import pydicom
>>> from pydicom.contrib.pydicom_PIL import show_PIL
>>> ds = pydicom.read_file("filename")

"""
# Available at https://github.com/pydicom/pydicom
from __future__ import division

import os
import traceback
import numpy as np
from PIL import Image
import pydicom as dicom
from multiprocessing import Pool
from functools import partial



def convert_dcm_image(dcmName, imgDir):
    """Get Image object from Python Imaging Library(PIL)"""
    try:
        basename = os.path.basename(dcmName).replace('.dcm', '.jpg')
        imgName = os.path.join(imgDir, basename)
        print imgName
        dataset = dicom.read_file(dcmName, force=True)
        if 'PixelData' not in dataset:
            # raise TypeError("%s does not have pixel data" % dcmName)
            data = np.asarray(data.pixel_array, dtype=np.uint16)
            if dataset.PhotometricInterpretation == 'MONOCHROME1':
                data = data.max() - data
            # Histogram equalization of the raw image
            minval, maxval = np.percentile(data, [5, 99])
            
            win = maxval - minval     # window/level
            lvl = (minval + maxval) / 2.0
            
            e = [
                0, 255,
                lambda data: ((data - (lvl - 0.5)) / (win -1) + 0.5) * 255
            ]
            img_arr = np.piecewise(data, [data <= minval, data > maxval], e)
            Image.fromarray(np.uint8(img_arr)).save(imgName)

        # can only apply LUT if these window info exists
        if ('WindowWidth' not in dataset) or ('WindowCenter' not in dataset):
            bits = dataset.BitsAllocated
            samples = dataset.SamplesPerPixel
            if bits == 8 and samples == 1:
                mode = "L"
            elif bits == 8 and samples == 3:
                mode = "RGB"
            elif bits == 16:
                # not sure about this -- PIL source says is 'experimental'
                # and no documentation. Also, should bytes swap depending
                # on endian of file and system??
                mode = "I;16"
            else:
                raise TypeError("Don't know PIL mode for %d BitsAllocated "
                                "and %d SamplesPerPixel" % (bits, samples))

            size = (dataset.Columns, dataset.Rows)

            # Recommended to specify all details
            # by http://www.pythonware.com/library/pil/handbook/image.htm
            im = Image.frombuffer(mode, size, dataset.PixelData, "raw", mode, 0, 1)
            data = np.asarray(im, dtype=np.uint16)
            if dataset.PhotometricInterpretation == 'MONOCHROME1':
                data = data.max() - data
            # Histogram equalization of the raw image
            minval, maxval = np.percentile(data, [5, 99])
            
            win = maxval - minval     # window/level
            lvl = (minval + maxval) / 2.0
            
            e = [
                0, 255,
                lambda data: ((data - (lvl - 0.5)) / (win -1) + 0.5) * 255
            ]
            img_arr = np.piecewise(data, [data <= minval, data > maxval], e)
            Image.fromarray(np.uint8(img_arr)).save(imgName)

        else:
            data = dataset.pixel_array
            window = int(dataset.WindowWidth)
            level = int(dataset.WindowCenter)
            if dataset.PhotometricInterpretation == 'MONOCHROME1':
                data = data.max() - data
            img_arr = np.piecewise(data, 
                                   [data <= (level - 0.5 - (window - 1) / 2),
                                    data > (level - 0.5 + (window - 1) / 2)],
                                   [0, 255, lambda data: ((data - (level - 0.5)) / 
                                    (window - 1) + 0.5) * (255 - 0)]) 

            # http://www.pythonware.com/library/pil/handbook/image.htm
            Image.fromarray(np.uint8(img_arr)).convert('L').save(imgName)
    except:
        traceback.print_exc()

def convert_dcms(dcm_names, jpg_dir):
    if not os.path.isdir(jpg_dir):
        os.mkdir(jpg_dir)
    pool = Pool(processes=None)
    partial_worker = partial(convert_dcm_image, imgDir=jpg_dir)
    pool.map(partial_worker, dcm_names)
    

if __name__ == '__main__':
    root = '/media/zhang/Elements/dcm/qc_normal/20180813_20180822'
    jpg_path = '/data/dcm/20180822'

    dcm_names = [os.path.join(root, f) for f in os.listdir(root) if f.endswith('.dcm')]
    convert_dcms(dcm_names, jpg_path)
    # convert_dcm_image(dcm_names[0], jpg_path)

