#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import pydicom as dicom
import SimpleITK as sitk
import numpy as np
from PIL import Image

def convert_dcm_to_jpg(filename, savedir='.', cut_min=5, cut_max=99):
    ds = sitk.ReadImage(filename)
    img_array = sitk.GetArrayFromImage(ds)
    frames, height, width = img_array.shape
    dcm_object = dicom.read_file(filename, force=True)
    # dcm_array = dcm_object.pixel_array
    assert frames == 1, "file: %s has more than one frames"
    img = img_array[0].astype(np.float64)
    if hasattr(dcm_object, 'PhotometricInterpretation') \
            and dcm_object.PhotometricInterpretation == 'MONOCHROME1':
        img = img.max() - img
    # Histogram equalization of the image
    lim1, lim2 = np.percentile(img, [cut_min, cut_max])
    img[img < lim1] = lim1
    img[img > lim2] = lim2

    img -= lim1
    img /= img.max()
    img *= 255
    img = img.astype(np.uint8)

    im = Image.fromarray(img)
    basename = os.path.basename(filename).replace('.dcm', '.jpg')
    jpg_name = os.path.join(savedir, basename)
    im.save(jpg_name)


if __name__ == '__main__':
    filename = sys.argv[1]
    savedir = '.'
    if len(sys.argv) > 2:
        savedir = sys.argv[2]
    convert_dcm_to_jpg(filename, savedir)
