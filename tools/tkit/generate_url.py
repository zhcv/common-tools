#!/usr/bin/env python
# -*- coding:utf-8 -*-

import pydicom as dicom
import traceback
import webbrowser as web # use default browser

url_head = 'http://v2.jfhealthcare.cn/v1/picl/viewer/?objects=%5B%7B%22StudyUid%22%3A%22'
sands = '%22%2C%22SeriesUid%22%3A%22'
sando = '%22%2C%22ObjectUid%22%3A%22'
url_tail = '%22%7D%5D'


# studyUid = '1.2.156.147522.44.410947.3622.20180710151414'
# seriesUid = '1.2.156.147522.44.410947.3622.1.20180710151432'
# objectUid = '1.2.156.147522.44.410947.3622.1.1.20180710151432'




dcmUrl = "http://v2.jfhealthcare.cn/v1/picl/aets/piclarc/wado?requestType=WADO&studyUID=%s&seriesUID=%s&objectUID=%s&contentType=application/dicom"
jpgUrl = "http://v2.jfhealthcare.cn/v1/picl/aets/piclarc/wado?requestType=WADO&studyUID=%s&seriesUID=%s&objectUID=%s"


def generate_viewer_url(localPath):
    try:
        dcm_object = dicom.read_file(localPath, force=True)
        studyUid = dcm_object.StudyInstanceUID
        seriesUid = dcm_object.SeriesInstanceUID
        objectUid = dcm_object.SOPInstanceUID
        view_url = url_head + studyUid + sands + seriesUid + sando + objectUid + url_tail
        dcm_url = dcmUrl % (studyUid, seriesUid, objectUid)
        jpg_url = jpgUrl % (studyUid, seriesUid, objectUid)
    except IOError as e:
        print "\033[1;31mIOError: [Errno 2] No such file or directory: %s \033[0m" % localPath
    except Exception as e:
        traceback.print_exc()
        #print "Error dcm: %s" % localPath
    return view_url, jpg_url, dcm_url


def run_to_use_default_browser_open_url(url):
    web.open_new_tab(url)
    print "\033[1;32mrun to use default browser open url open url ending ...\033[0m"


if __name__ == "__main__":
    from sys import argv
    if len(argv) < 2:
        print "\033[1;32mUsage:./generate_url.py Filename\033[0m"
    else:
        filename = argv[1]
        view_url, jpg_url, dcm_url = generate_viewer_url(filename)
        run_to_use_default_browser_open_url(view_url)
        # run_to_use_default_browser_open_url(jpg_url)
