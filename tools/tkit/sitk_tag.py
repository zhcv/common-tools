# =============================================================================
# To get all the tags from the dicom files you need to load them individually. 
# Something along these lines, but iterating over the list generated 
# by GetGDCMSeriesFileNames
# https://itk.org/pipermail/community/2016-May/011426.html
# =============================================================================
r"""Access DICOM private tags using SimpleITK"""

from __future__ import print_function

import SimpleITK as sitk
import sys, os

if len ( sys.argv ) < 2:
    print( "Usage: DicomImagePrintTags <input_file>" )
    sys.exit ( 1 )

inputImage = sitk.ReadImage( sys.argv[1] )

for k in inputImage.GetMetaDataKeys():
    v = inputImage.GetMetaData(k)
    print("({0}) = = \"{2}\"".format(k,v))
