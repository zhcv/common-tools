import SimpleITK as itk


def importDICOM(inputLocation):
    reader = itk.ImageSeriesReader()
    filenamesDICOM = reader.GetGDCMSeriesFileNames(inputLocation)
    reader.SetFileNames(filenamesDICOM)
    image = reader.Execute()
    return image



imdir = "/path/to/images"
image_t = itk.Image[itk.F, 3]


#set up reader
reader = itk.ImageSeriesReader[image_t].New()
dicomIO = itk.GDCMImageIO.New()
dicomFN = itk.GDCMSeriesFileNames.New()
reader.SetImageIO(dicomIO)

#get file names
dicomFN.SetUseSeriesDetails(True)
dicomFN.SetDirectory(imdir)

#get the first image series
uids = dicomFN.GetSeriesUIDs()
fnames = dicomFN.GetFileNames(uids[0])

#read in the files
reader.SetFileNames(fnames)
reader.Update()
image = reader.GetOutput()

#getting the image works fine.  Now to access the meta data dictionary
metad = dicomIO.GetMetaDataDictionary()

#get the entry for gantry tilt using the relevant DICOM tag (0018, 1120)
tilt = metad.Get('0018|1120')
"""
At this point I am stuck.  tilt is of type itkMetaDataObjectBase, rather than 
one the derived classes specific to strings, floats etc.. itkMetaDataObjectBase 
does not contain the GetMetaDataObjectValue() method, whereas the derived classes do.
In the ITK manual, the C++ example uses a dynamic_cast at the type the tag is 
extracted so that 'tilt' would be of one the derived class types.  
I have tried the following:
"""
sst = itk.MetaDataObject.SS
tilt.__class__ = sst
tilt.GetMetaDataObjectValue()
"""
casting tilt does work, and the GetMetaDataObjectValue() method is now present 
as one of tilt's attributes, but just returns an error if executed.

The other approach I have tried is to use the dicomIO.GetValueFromTag() method.  
From the source file the method signature is
"""
GetValueFromTag(self, string tag, string value)

# 'value' is, I presume, meant to be the name of the variable created containing 
#  the tilt angle.  However if I try:

dicomIO.GetValueFromTag('0018|1120', 't_angle')

"""
I get the error in method 'itkGDCMImageIO_GetValueFromTag', 
argument 3 of type 'std::string &

I would be tremendously grateful for any pointers on how to access the tag data - 
it must be a routine operation for people using Python/ITK on DICOM images!
"""
