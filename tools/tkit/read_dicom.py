import cv2
import numpy as np
import pydicom
import SimpleITK as sitk
from skimage import exposure


def read_dicom(filename, cut_min=5, cut_max=99):
    """Read DICOM file and convert it to a decent quality unit8 image.

    Parameters:
    ----------
    filename: str
        Existing DICOM file filename.
    cut_min: int
        lowest percentile which is used to cut the image histogram.
    cut_max: int
        highest percentile which is used to cut the image histogram.
    """
    try:
        data = pydicom.read_file(filename, force=True)
        #
        # img = np.frombuffer(data.PixelData, dtype=np.uint16).astype(np.float64)
        img = data.pixel_array.astype(np.float64)

        # Rescale intercept and slope are a simple linear transform applied to
        # the raw pixel data before applying the window width/center.
        RescaleSlope = getattr(data, 'RescaleScope', None)
        RescaleIntercept = getattr(data, 'RescaleIntercept', None)
        if RescaleSlope and RescaleIntercept:
            img = (img * RescaleSlope) + RescaleIntercept

        center = getattr(data, 'WindowCenter', None)
        width = getattr(data, 'WindowWidth', None)
        # calculate the pixels to display using the window center/width
        # lowest visible value and highest visible vale
        if center and width:
            vMin = center - 0.5 - (width - 1) / 2.
            vMax = center - 0.5 + (width - 1) / 2.
        else:
            vMin, vMax = np.percentile(img, [cut_min, cut_max])

        # Normalized img [0, vMax-vMin]
        img[img < vMin] = vMin
        img[img > vMax] = vMax
        img -= vMin

        if hasattr(data, 'PhotometricInterpretation') and data.PhotometricInterpretation == 'MONOCHROME1':
            img = img.max() - img

        # rescale pixel value [0, 255]
        img /= img.max()
        img *= 255
        img = img.astype(np.uint8)

        # img = img.reshape((data.Rows, data.Columns))
        # method_1 not good for medical image
        # img_eq = exposure.equalize_hist(data.pixel_array)
        # method_2
        # img_adaptive = exposure.equalize_adapthist(img, clip_limit=0.040)
        # img_adaptive = exposure.equalize_adapthist(img)

        return img
    except Exception, e:
        return None


def sitk_read_dicom(filename, cut_min, cut_max):
    # Read DICOM file and convert it to a decent quality unit8 image.
    try:
        ds = sitk.ReadImage(filename)
        img_array = sitk.GetArrayFromImage(ds)
        frame_num, height, width = img_array.shape
        assert frame_num == 1
        img = np.float64(img_array[0])

        tags = ['0028|0004',    # PhotometricInterpretation
                '0028|1050',    # WindowCenter
                '0028|1051',    # WindowWith
                '0028|1052',    # RescaleIntercept
                '0028|1053',    # RescaleSlope
        ]

        # tags_all = ds.GetMetaDataKeys()
        if ds.HasMetaDataKey(tags[3]) and ds.HasMetaDataKey(tags[4]):
            RescaleIntercept = float(ds.GetMetaData(tags[3]))
            RescaleSlope = float(ds.GetMetaData(tags[4]))
            img = (img * RescaleSlope) + RescaleIntercept



        center = float(ds.GetMetaData(tags[1])) if ds.HasMetaDataKey(tags[1]) else None
        width = float(ds.GetMetaData(tags[2])) if ds.HasMetaDataKey(tags[2]) else None

        # calculate the pixels to display using the window center/width
        # lowest visible value and highest visible vale
        if center and width:
            vMin = center - 0.5 - (width - 1) / 2.
            vMax = center - 0.5 + (width - 1) / 2.
        else:
            vMin, vMax = np.percentile(img, [cut_min, cut_max])

        print "vMin", vMin, vMax

        # Normalized img [0, vMax-vMin]
        img[img < vMin] = vMin
        img[img > vMax] = vMax
        img -= vMin

        if ds.HasMetaDataKey(tags[0]) and ds.GetMetaData(tags[0]) == 'MONOCHROME1 ':
            img = img.max() - img

        # rescale pixel value [0, 255]
        img /= img.max()

        print "img_mean:", img.mean()

        img = exposure.equalize_adapthist(img)



        img *= 255
        img = img.astype(np.uint8)


        # method_2
        # img_adaptive = exposure.equalize_adapthist(img, clip_limit=0.040)
        # img = exposure.equalize_adapthist(img)

        return img
    except Exception, e:
        raise


def read_hist(filename, cut_min=1, cut_max=99):
    """Read DICOM file and convert it to a decent quality unit8 image.

    Parameters:
    ----------
    filename: str
        Existing DICOM file filename.
    cut_min: int
        lowest percentile which is used to cut the image histogram.
    cut_max: int
        highest percentile which is used to cut the image histogram.
    """
    try:
        data = pydicom.read_file(filename, force=True)
        #

        img = data.pixel_array.astype(np.float16)

        vMin, vMax = np.percentile(img, [cut_min, cut_max])

        # Normalized img [0, vMax-vMin]
        img[img < vMin] = vMin
        img[img > vMax] = vMax
        img -= vMin




        if hasattr(data, 'PhotometricInterpretation') and data.PhotometricInterpretation == 'MONOCHROME1':
            img = img.max() - img

        # rescale pixel value [0, 255]
        img /= img.max()

        print "image mean gray value:", img.mean()

        img *= 255
        img = img.astype(np.uint8)

        # img = img.reshape((data.Rows, data.Columns))
        # method_1 not good for medical image
        # img_eq = exposure.equalize_hist(data.pixel_array)
        # method_2
        # img_adaptive = exposure.equalize_adapthist(img, clip_limit=0.040)
        # img_adaptive = exposure.equalize_adapthist(img)

        return img
    except:
        return  None







if __name__ == '__main__':
    filename = '1.2.156.14702.18.1.3.420180524110852810.dcm'
    filename = '1.2.156.112536.2.560.7050058251176.1284641038315.11.dcm'
    filename = '1.2.156.14702.18.1.3.420180520104906222.dcm'
    img = read_hist(filename, 1, 95)
    cv2.imwrite('test_79.jpg', img)
