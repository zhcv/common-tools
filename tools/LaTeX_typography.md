# Latex中如何排版中文音调（1～4声）


## 数学环境下给出中文音调
``` latex
ā $\bar{a}$

á $\acute{a}$

ǎ $\check{a}$

à $\grave{a}$
```

## 非数学环境下给出中文音调
```latex
ā \={a}

á \'{a}

ǎ \v{a}

à \`{a}
```

## Latex 
[Listings in Latex with UTF-8 (or at least german umlauts)](https://stackoverflow.com/questions/1116266/listings-in-latex-with-utf-8-or-at-least-german-umlauts)


[LaTeX/Special Characters](https://en.wikibooks.org/wiki/LaTeX/Special_Characters)
