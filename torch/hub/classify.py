import io
import requests
from PIL import Image

from torchvision import models, transforms
from torch.autograd import Variable

LABELS_URL = 'http://s3.amazonaws.com/outcome-blog/imagenet/labels.json'
# This can be any image you like
IMG_URL = 'http://s3.amazonaws.com/outcome-blog/wp-content/uploads/2017/02/25192225/cat.jpg'


# Initialize the pre-trained model
squeeze = models.squeezenet1_1(pretrained=True)


# Image pre-processing transforms
normalize = transforms.Normalize(
    mean=[0.485, 0.456, 0.406],
    std=[0.229, 0.224, 0.225]
    )
preprocess = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    normalize
    ])

# Create a pillow image
response = requests.get(IMG_URL)
img_pil = Image.open(io.BytesIO(response.content))

# Apply transforms
img_tensor = preprocess(img_pil)
# Add a batch dimension
img_tensor.unsqueeze_(0)
# Forward pass without activation
fc_out = squeeze(Variable(img_tensor))

# Download ImageNet labels and store them as dict
labels = {int(key):value for (key, value) 
    in requests.get(LABELS_URL).json().items()}

# Print the label
print labels[fc_out.data.numpy().argmax()]
