# VIM删除重复行


## 下面收录了2篇相关文章

[1](http://kangzye.blog.163.com/blog/static/36819223201061705729933/)
vim 查找相同的两行,以及删除重复行
[2](http://vim.wikia.com/wiki/Uniq_-_Removing_duplicate_lines)
要查找相同的两行，先将内容排序，然后查找前一行等于后一行者

:sort

/^\(.\+\)$\n\1

如此就找到了，博大精深的VIM   – . –

 

删除重复行（先排一下序）：

:sort
:g/^\(.\+\)$\n\1/d

2) 转贴自：http://kangzye.blog.163.com/blog/static/36819223201061705729933/

关于Vim的删除文本中的重复行。

最近开始使用Vim，感觉此乃外人间之神器，怪不得那么多Vimer对其偏爱有加。现在说说文本中重读行删除问题。在众多的编辑器中Vim对正则的支持估计是最强大的了，很多高级的正则表达式在Vim中都是支持的。以下是我的解法：

:%s/\(.*\)\n\1/\1\r/g
:g/^$/d

这里需要两个命令，后再http://vim.wikia.com/wiki/Uniq_-_Removing_duplicate_lines找到了我的加强版：

:g/^\(.*\)$\n\1$/d
:g/\%(^\1$\n\)\@<=\(.*\)$/d

两者都是前面的命令和我的相似，它删除的是重复项的前面一项；后面的命令删除的重复项的后面的内容。注意两个命令独立的，可以分别使用。对于g命令不是很熟悉，它是一个全局命令，以后会有全面介绍。

第二个命令的解释：

g//d <– Delete the lines matching the regexp
\@<= <– If the bit following matches, make sure the bit preceding this symbol directly precedes the match
\(.*\)$ <– Match the line into subst register 1
\%( ) <— Group without placing in a subst register.
^\1$\n <— Match subst register 1 followed by end of line and the new line between the 2 lines


The following command will sort all lines and remove duplicates (keeping unique lines):

:sort u

If you need more control, here are some alternatives.

There are two versions (and \v "verymagic" version as a variant of the second): the first leaves only the last line, the second leaves only the first line. (Use \zs for speed reason.)

g/^\(.*\)\n\1$/d
g/\%(^\1\n\)\@<=\(.*\)$/d
g/\v%(^\1\n)@<=(.*)$/d

Breakdown of the second version:

g/\%(^\1\n\)\@<=\(.*\)$/d
g/                     /d  <-- Delete the lines matching the regexp
            \@<=           <-- If the bit following matches, make sure the bit preceding this symbol directly precedes the match
                \(.*\)$    <-- Match the line into subst register 1
  \%(     \)               <-- Group without placing in a subst register.
     ^\1\n                 <-- Match subst register 1 followed the new line between the 2 lines

In this simple format (matching the whole line), it's not going to make much difference, but it will start to matter if you want to do stuff like match the first word only.

This does a uniq on the first word in the line (with the \v "verymagic" version included after), and deletes all but the first line:

g/\%(^\1\>.*\n\)\@<=\(\k\+\).*$/d
g/\v%(^\1>.*\n)@<=(\k+).*$/d


```vim 
:sort   //可以直接排序，这个太好用了
:g/^\(.*\)$\n\1$/d                      //去除重复行
```

# shell remove repeat lines

$ cat file | sort | uniq

note: 
    -c or count: show the number of repeats of lines
    -d or repeated: just show lines that repeated
    -s or skip-fields: ignore compared bar specified
    -u or unique: only show one line of each rows
    -w or check-chars=<char_location>: specify chars to be compared

# 
>> 1. uniq file.txt
>> 2. sort file.txt | uniq
>> 3. sort -u file.txt

# just show single line
>> 1. uniq -d file.txt
>> 2. sort file.txt | uniq -u

# stastical repeated times of each row
>> 1. sort file.txt | uniq -c

# find repeated lines
>> $: sort file.txt | uniq -d

# vim remove repeated lines
>> : sort 
>> :g/^\(.*\)$\n\1$/d


1. 全局删除匹配到的行

:g/pattern/d

2. 删除第1-10行里的匹配到的行

:1,10g/pattern/d

3. 删除不包含指定字符的行

:v/pattern/d
或
:g!/pattern/d
