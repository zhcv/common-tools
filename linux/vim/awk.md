# 转：使用awk命令获取文本的某一行，某一列

1、打印文件的第一列(域)                 ： awk '{print $1}' filename
2、打印文件的前两列(域)                 ： awk '{print $1,$2}' filename
3、打印完第一列，然后打印第二列  ： awk '{print $1 $2}' filename
4、打印文本文件的总行数                ： awk 'END{print NR}' filename
5、打印文本第一行                          ：awk 'NR==1{print}' filename
6、打印文本第二行第一列                ：sed -n "2, 1p" filename | awk 'print $1'

 

 

#(获取test文件的第4行)

#cat test| awk 'NR==4'或cat test|sed -n '4p'直接获取某一行的数据



    shell里面的赋值方法有两种，格式为
    1) arg=`(命令)`
    2) arg=$(命令)
因此，如果想要把某一文件的总行数赋值给变量nlines，可以表达为：
    1) nlines=`(awk 'END{print NR}' filename)`
或者

    2) nlines=$(awk 'END{print NR}' filename)

 

 
awk练习题
   wang     4
   cui      3
   zhao     4
   liu      3
   liu      3
   chang    5
   li       2
   1 通过第一个域找出字符长度为4的
   2 当第二列值大于3时，创建空白文件，文件名为当前行第一个域$1 (touch $1)
   3 将文档中 liu 字符串替换为 hong
   4 求第二列的和
   5 求第二列的平均值
   6 求第二列中的最大值
   7 将第一列过滤重复后，列出每一项，每一项的出现次数，每一项的大小总和
   1、字符串长度
    awk 'length($1)=="4"{print $1}'
   2、执行系统命令
    awk '{if($2>3){system ("touch "$1)}}'
   3、gsub(/r/,"s",域) 在指定域(默认$0)中用s替代r  (sed 's///g')
    awk '{gsub(/liu/,"hong",$1);print $0}' a.txt
   4、列求和
    df -h | awk '{a+=$2}END{print a}'
   5、列求平均值
    df -h | awk '{a+=$2}END{print a/NR}'
    df -h | awk '{a+=$2;b++}END{print a,a/b}' 
   6、列求最大值
    df -h | awk 'BEGIN{a=0}{if($2>a) a=$2 }END{print a}'
   7、将第一列过滤重复列出每一项，每一项的出现次数，每一项的大小总和
    awk '{a[$1]++;b[$1]+=$2}END{for(i in a){print i,a[i],b[i]}}'
