# vim　行首或行尾插入指定字符串

## 行首/行尾、所有行首
```vim

:%s/^/your_word/
:%s/$/your_word/

:%s/^/your_word/g
```
