Linux process
=============

首先，我们可以使用$ps命令来查询正在运行的进程，比如`$ps -eo pid,comm,cmd，`
下图为执行结果:

(-e表示列出全部进程，-o pid,comm,cmd表示我们需要PID，COMMAND，CMD信息))

## 显示进程树
`$pstree `

## 进程与线程(thread))     
尽管在UNIX中，进程与线程是有联系但不同的两个东西，但在Linux中，    
线程只是一种特殊的进程。多个线程之间可以共享内存空间和IO接口。所以，   
进程是Linux程序的唯一的实现方式。


## Linux: Find Out Which Port Number a Process is Listening on

1. sudo netstat -ltnp
2. sudo netstat -ltnp | grep -w ':80'                                  
3. sudo lsof -i :80
4. sudo apt install psmisc ----> sudo fuser 3306/tcp
5. ps -p [processID] -o comm=  ----> Usage: ps -p [975] -o comm=