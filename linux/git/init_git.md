##Command line instructions
#### Git global setup
```shell
git config --global user.name "张海鹏"
git config --global user.email "haipeng.zhang@jfhealthcare.com"
```
Create a new repository
```shell
git clone ssh://git@180.167.46.105:22222/zhp/dr-segment.git
cd dr-segment
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```
Existing folder
```shell
cd existing_folder
git init
git remote add origin ssh://git@180.167.46.105:22222/zhp/dr-segment.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
Existing Git repository
```shell
cd existing_repo
git remote rename origin old-origin
git remote add origin ssh://git@180.167.46.105:22222/zhp/dr-segment.git
git push -u origin --all
git push -u origin --tags
```
