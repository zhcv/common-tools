# Gitlab如何进行备份恢复与迁移

<!-- gitlab community one-click setup -->
# $ sudo docker pull gitlab/gitlab-ce

[reference](https://blog.csdn.net/ouyang_peng/article/details/77070977)

## 1. Gitlab create & backup
### 1.1 创建备份文件

>> 首先我们得把老服务器上的Gitlab整体备份，使用Gitlab一键安装包安装Gitlab非常简单, 
>> 同样的备份恢复与迁移也非常简单. 使用一条命令即可创建完整的Gitlab备份。

$ gitlab-rake gitlab:backup:create

```shell
>> 使用以上命令会在/var/opt/gitlab/backups目录下创建一个名称类似为
>> 1502357536_2017_08_10_9.4.3_gitlab_backup.tar的压缩包, 这个压缩包就是Gitlab整个
>> 的完整部分, 其中开头的1502357536_2017_08_10_9.4.3是备份创建的日期

    /etc/gitlab/gitlab.rb 配置文件须备份
    /var/opt/gitlab/nginx/conf nginx配置文件
    /etc/postfix/main.cfpostfix 邮件配置备份

>> 生成完后，/var/opt/gitlab/backups目录创建一个名称类似为
>> 1502357536_2017_08_10_9.4.3_gitlab_backup.tar的压缩包
```
### 1.2 更改Gitlab备份目录

>> 当然你也可以通过/etc/gitlab/gitlab.rb配置文件来修改默认存放备份文件的目录

    + gitlab_rails['backup_path'] = "/var/opt/gitlab/backups"

>> /var/opt/gitlab/backups修改为你想存放备份的目录即可，例如下面代码将备份路径修改为/mnt/backups

    + gitlab_rails['backup_path'] = '/mnt/backups'


# 修改完成之后使用下面命令重载配置文件即可.
$ gitlab-ctl reconfigure



## 1.2 Gitlab自动备份
### 1.2.1 定时自动备份

>> 在crontab文件里面，每一行代表一项任务，每行的每个字段代表一项设置，它的格式共分为六个字段，
>> 前五段是时间设定段，第六段是要执行的命令段，每个字段之间用空格分割，没用的段用*代替，格式如下：

m h dom mon dow user command

其中：

    m： 表示分钟，可以是从0到59之间的任何整数。
    h：表示小时，可以是从0到23之间的任何整数。
    dom：表示日期，可以是从1到31之间的任何整数。
    mon：表示月份，可以是从1到12之间的任何整数。
    dow：表示星期几，可以是从0到7之间的任何整数，这里的0或7代表星期日。
    user : 表示执行的用户。
    command：要执行的命令，可以是系统命令，也可以是自己编写的脚本文件(如shell文件)。

实现每天凌晨2点进行一次自动备份:通过crontab使用备份命令实现，需重启cron服务
方法1、在命令行输入: crontab -e 然后添加相应的任务，wq存盘退出。

#输入命令crontab -e
sudo crontab -e  
#输入相应的任务
0 2 * * * /opt/gitlab/bin/gitlab-rake gitlab:backup:create CRON=1  

方法2、直接编辑/etc/crontab 文件，即vi /etc/crontab，然后添加相应的任务

#编辑 /etc/crontab
vi /etc/crontab 

    1
    2

然后再编辑框内输入相应的任务

# edited by ouyang 2017-8-11 添加定时任务，每天凌晨两点，执行gitlab备份
0  2    * * *   root    /opt/gitlab/bin/gitlab-rake gitlab:backup:create CRON=1  

或者直接定时执行一个脚本 auto_backup.sh ，脚本内容为

/opt/gitlab/bin/gitlab-rake gitlab:backup:create CRON=1

    1

然后再 /etc/crontab中，添加相关任务定时执行 auto_backup.sh 脚本文件

sudo chmod +x auto_backup.sh
sudo vim auto_backup.sh

![avatar](20170815105721376.png)

/etc/crontab 中添加执行脚本的定时任务，代码如下：

#也可以按照如下所示的方法，定时执行 auto_backup.sh脚本，脚本内容就填写： /opt/gitlab/bin/gitlab-rake gitlab:backup:create CRON=1 

0 2    * * *   root    /data/gitlabData/backups/auto_backup.sh -D 1   

    1
    2
    3

编写完 /etc/crontab 文件之后，需要重新启动cron服务

#重新加载cron配置文件
sudo /usr/sbin/service cron reload
#重启cron服务
sudo /usr/sbin/service cron restart 

    1
    2
    3
    4

实际运行如下

root@ubuntu4146:~# sudo /usr/sbin/service cron reload
root@ubuntu4146:~# sudo /usr/sbin/service cron restart 
cron stop/waiting
cron start/running, process 17738

    1
    2
    3
    4

关于Cron表达式可以参考链接：

    http://www.cnblogs.com/junrong624/p/4239517.html
    http://blog.csdn.net/xiyuan1999/article/details/8160998
    http://www.cnblogs.com/kaituorensheng/p/4494321.html

关于gitlab备份可以参考链接：


    http://blog.csdn.net/utopiaprince/article/details/50039989
    http://www.cnblogs.com/shansongxian/p/6599144.html
    http://www.ttlsa.com/linux/gitlab-backup-restore/
    https://github.com/sund/auto-gitlab-backup
    https://gitlab.com/help/raketasks/backup_restore.md
    http://blog.csdn.net/felix_yujing/article/details/52918803

关于如何将gitlab备份文件备份到远程备份服务器，参考链接：

    Git学习–>如何通过Shell脚本自动定时将Gitlab备份文件复制到远程服务器?
    http://blog.csdn.net/ouyang_peng/article/details/77334215

1.2.2 设置备份过期时间

设置只保存最近7天的备份，编辑 /etc/gitlab/gitlab.rb 配置文件,找到gitlab_rails[‘backup_keep_time’]，设置为你想要设置的值，然后保存。

gitlab_rails['backup_keep_time'] = 604800  

    1

这里写图片描述

