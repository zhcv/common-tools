# Git pushing to remote GitHub repository as wrong user

[ERROR]: Permission to personaluser/newrepo.git denied to 

[REASON]: it seems that you already have set up your separate ssh-keys, so you
also need to create a ~/.ssh/config file and populate it with information 
similar to this:
```
HOST work.github.com
    HostName github.com
    User WORK_GITHUB_USERNAME
    PreferredAuthentications publickey
    IdentifyFile ~/.ssh/id_work_rsa
    IdentitiesOnly yes

HOST personal.github.com
    HostName github.com
    User PERSONAL_GITHUB_USERNAME
    PreferredAuthentications publickey
    IdentityFile ~/.ssh/id_personal_rsa
    IdentitiesOnly yes
```
<!--
Now you just need to make sure your origin (or any remote in general) url match
the correct Host url in your repective repos depending on your user name. If 
you alread have existing personal repos, you can edit that repo's .git/config
file in your text editor:
[remote "origin"]
    fetch = +refs/heads/*:refs/remotes/origin/*
    url = git@personal.github.com:PERSONAL_GITHUB_USERNAME/project.git

or do it via command line:
    git remote set-url origin git@personal.github.com:PERSONAL_GITHUB_USERNAME/project.git

Likewise to your work one:
    
[remote "origin"]
    fetch = +refs/heads/*:refs/remotes/origin/*
    url = git@work.github.com:your_work_organization/project.gi

or again, via command line:
    git remote set-url origin git@work.github.com:your_work_orginaization/project.git

Of course, your can aways set one of your Host urls in your ~/.ssh/config file
as just

Host github.com

I only used work.github.com to see the config relationships easier.

EDIT
One thing to note that I just found out myself is that if you ever set global 
git config values for your user.email value (and i'm guessing user.name would 
send a different value as well), git will show your commits as that email user. 
To get around this, you can override the global git config settings within 
your local repository:

$ git config user.name "John Doe"
$ git config user.email johndoe@example.com

This should now send up commits as the correct user for that repo.
