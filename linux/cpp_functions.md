# 

In Ubuntu, after installing libstdc++6-x.x-doc, these docs are available via man, examples(libstdc++-4.8-doc)
```
man std::list
man std::weak_ptr
man std::ios_base
```
To get a list of thest entries, use
``` apropos -r '^std' | vim - ```
