sudo 
====


>> 创建新用户
```$ sudo adduse user_name```

>> 向普通用户授予 sudo 权限
```shell
sudo usermod -a -G sudo user_name 
or
sudo adduser user_name sudo
```

>> 移除用户的 sudo 权限
```
sudo deluser user_name sudo
or
sudo gpasswd -d user_name sudo
```

>> 验证用户 ostechnix 是否已从 sudo 组中删除
```$ sudo -l -U user_name```
