1.统计当前目录下，py文件数量：

    find . -name "*.py" |wc -l

2.统计当前目录下，所有py文件行数：

    find . -name "*.py" |xargs cat|wc -l

3.统计当前目录下，所有py文件行数，并过滤空行：

    find . -name "*.py" |xargs cat|grep -v ^$|wc -l

# 统计目录并按行数排序
```shell
find . -name *.py | xargs wc -l | sort -n
find . -name "*.py" | xargs wc -l | sort -k2
```

