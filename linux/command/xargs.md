 bash编程之xargs实用技巧

xargs结合管道操作符|，可以完成很多看似复杂的问题：

 

1、快速删除所有.log日志文件

机器运行久了，就会有各式各样的日志文件，散落在各个目录下，可以利用下面的方法：
```
find ./ -name '*.log' | xargs rm -rf
```
上面的代码，先把当前目录及子目录下的所有后缀是log的文件找出来，然后做为参数，传递给 rm -rf ，这样就把日志文件给干掉了。

 

2、根据名字找到运行的进程并杀掉

比如：有一个java程序在服务器上，我们在写部署脚本时，要先把之前在运行的程序kill掉，如果只知道运行的mainClass名称，不知道pid，可以参考下面这样
```
ps -ef|grep java| grep 'mainClass名称' | awk '{print $2}' | xargs kill -9
```
大家把上面的'mainClass名称'换成需要查找的进程名即可，原理还是先找到所有java进程，然后再grep过滤出指定的mainClass，再用awk把pid取出来，最后利用xargs传给kill 干掉。（注：如果不希望强制杀死，而是等当前请求处理完后再结束，把-9去掉即可）

 

3、批量删除指定key前缀的redis缓存

redis并没有批量删除某些前缀key缓存的方法，可以参考下面的做法
```
./redis-cli -h 10.1.2.3 -p 6381 -n 1 KEYS "shop:info:*" | xargs ./redis-cli -h 10.1.2.3 -p 6381 -n 1 DEL
```
即：先连到10.1.2.3:6381 ，并切换到db1 ,然后找到shop:info:*匹配的所有key，然后再用xargs传到后面的命令删除

 

4、找出端口冲突的进程并kill掉

有时候启动一个程序时，发现端口被其它程序占用了，下面的方法可以干掉占用的程序
```
echo $(lsof -i:8081 | awk '{print $2}') | awk '{print $2}' | xargs kill -9
```
即：找出8081端口对应的pid，然后干掉
