# docker command


## 列出所有正在运行的容器
```
docker ps -a  
docker image ls 
docker stop <name>
docker rm <name>
```
## 从公网拉取镜像
```docker pull images_name
```

## 查看镜像列表
```
docker search nginx
```

## Export image
```
docker save -o image_name.tar image_name
```
## 查看docker 网路
```
docker network ls 
```
## 创建一个docker网络my-docker
```
docker network create -d bridge \
--subnet=192.168.0.0/24 \
--gateway=192.168.0.100 \
--ip-range=192.168.0.0/24 \
my-docker
```
## 利用刚才创建的网络启动一个容器
```
#docker run --network=my-docker --ip=192.168.0.5 -itd --name=con_name -h lb01 image_name
--network   #指定容器网络
--ip        #设定容器ip地址
-h          #给容器设置主机名
```
##  查看容器
```
#方法一：
docker top con_name

#方法二：
docker inspect --format "{{.State.Pid}}" con_name
```
## 运行dockerfile并给dockerfile创建的镜像建立名字
```
docker build -t mysql:3.6.34 `pwd`
```
## mariadb容器启动前需先设置密码方法
```
docker run -d -P -e MYSQL_ROOT_PASSWORD=password  img_id
```
## docker修改镜像名
```
docker tag imageid name:tag
```
## 进入docker容器脚本
```
[root@docker ~]# cat nsenter.sh 
PID=`docker inspect --format "{{.State.Pid}}" $1`
nsenter -t $PID -u --mount -i -n -p
```
## 创建一个网络
```
docker network create --driver bridge --subnet 172.22.16.0/24 --gateway 172.22.16.1 my_net2
```
## 将容器添加到my_net2网络 connect
```
docker network connect my_net2 oldboy1
```
## docker日志模块

## 使用filebeat收集日志
```
{
  "registry-mirrors": ["https://56px195b.mirror.aliyuncs.com"],
  "cluster-store":"consul://192.168.56.13:8500",
  "cluster-advertise": "192.168.56.11:2375",
  "log-driver": "fluentd",
  "log-opts": {
        "fluentd-address":"192.168.56.13:24224",
        "tag":"linux-node1.example.com"
        }
}
```
## 获得docker容器里面的root权限

```
sudo docker exec -ti -u root 7509371edd48 bash

```


<!--------------------------------------------------------------->
                    Enter Into Docker Container                 
<!------------------------------------------------------------ -->
## 查看docker容器详细信息
1. method
```
sudo docker inspect CONTAINER_ID / CONTAINER_NAME
```

## 如果要显示该容器第一个进行的PID可以使用如下方式
```docker inspect -f {{.State.Pid}} tfserving_resnet
```

## 在拿到该进程PID之后我们就可以使用nsenter命令访问该容器了。
```
sudo nsenter --target 20296 --mount --uts --ipc --net --pid
```

2. method

```
sudo docker ps
sudo docker exec -it CONTAINER_ID/CONTAINER_NAME /bin/bash
```
<!--==========================================================-->
