#!/usr/bin/env bash


wget -c https://storage.googleapis.com/download.tensorflow.org/models/official/20181001_resnet/savedmodels/resnet_v2_fp32_savedmodel_NHWC_jpg.tar.gz

tar --strip-components=2 -C /tmp/resnet/ -zxvf resnet_v2_fp32_savedmodel_NHWC_jpg.tar.gz

docker pull tensorflow/serving

docker run -p 8501:8501 --name tfserving_resnet \
    --mount type=bind,source=/tmp/resnet,target=/models/resnet \
    -e MODEL_NAME=resnet -t tensorflow/serving &


<<...
# View logs

docker logs --since 30 container_name
docker logs -f -t --since='2018-11-01' --tail CONTAINER_ID
docker logs -t --since="2018-02-08T13:23:37" CONTAINER_ID
docker logs -t --since="2018-02-08T13:23:37" --until "2018-02-09T12:23:37" CONTAINER_ID

docker kill tfserving_resnet
# Enter into container
docker exec -it tfserving_resnet /bin/bash 
docker rm -f tfserving_resnet
docker stop tfserving_resnet
...
