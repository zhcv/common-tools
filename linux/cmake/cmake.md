# How to let cmake find CUDA

# ==method-1==
export CUDA_BIN_PATH=/usr/loca/cuda-9.0

# ==method-2==
cmake -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda-9.0 ..
