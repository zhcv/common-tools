# remove duplicate lines

# 删除重复行文件
awk '!x[$0]++' filename

# 删除第三列重复行
awk '!a[$3]++' filename

# 删除最后一列重复行


# 去除重复行时对空白行不做处理

* cat filename | nl -b a

# method1
awk '!NF || !a[$0]++'  a.txt |nl -b a
# method2
awk '!NF {print;next} !($0 in a) {a[$0];print}'  a.txt |nl -b a
# method3
awk '!/./ || !a[$0]++' a.txt | nl -b a
