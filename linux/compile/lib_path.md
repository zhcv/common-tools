
# after tar -zxvf * -C /PATH/lib
sudo ldconfig

Or, if you extract the TensorFlow C library to a non-system directory, such as ~/mydir, 
then configure the linker environmental variables:
```shell
export LIBRARY_PATH=$LIBRARY_PATH:~/mydir/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/mydir/lib
```

# gcc specify archive/static lib path
gcc -Wall twohellos.c -L. -lhello -o twohellos
gcc main.c-I../include -L../lib -lhello -o main
