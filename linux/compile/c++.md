```
g++ `pkg-config opencv --cflags` blur.cpp  -o blur `pkg-config opencv --libs`

g++ speak.cpp hello.cpp -o hello

g++ -o main main.cpp name1.cpp ... `pkg-config opencv --cflags --libs`
```
## 指定g++ 头文件路径的方法

1. 在g++ 中用一个option 来指定
```g++ -o main main.cpp -I /usr/local/include/python/ ```

2. 通过环境变量来设置，这样就可以不要在g++ 中来指定了

``
export CPLUS_INCLUDE_PATH=/usr/local/include/python/
g++ -o main main.cpp
```

# 在PATH中找到可执行文件程序的路径。
``
export PATH =$PATH:$HOME/bin (可一次指定多个搜索路径，":"用于分隔它们)
```

#gcc找到头文件的路径
```
C_INCLUDE_PATH=/usr/include/libxml2:/MyLib
export C_INCLUDE_PATH
```

# g++找到头文件的路径
```
CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/usr/include/libxml2:/MyLib
export CPLUS_INCLUDE_PATH
```

# 找到动态链接库的路径
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/MyLib
```

# 找到静态库的路径
```export LIBRARY_PATH=$LIBRARY_PATH:/MyLib```

# 下面是在gcc命令中手动设置搜索路径：

# 添加头文件搜索路径
```gcc foo.c -I /home/xiaowp/include -o foo```

# 添加动态库搜索路径
```gcc foo.c -L /home/xiaowp/lib -lfoo -o foo```

 # 添加静态库搜索路径

```gcc foo.c -L /home/xiaowp/lib -static -lfoo -o foo```

 库文件安装
```
sudo apt-cache search  库文件名
sudo apt-get install 下载搜索到的，后缀是-dev的那个包就可以了。
```

① 进入/usr/local/cppunit/lib，把找不到的动态链接库【libcppunit.so.1.12...】copy到根目录的/lib下.
② 编辑自己个人目录下的配置文件.bashrc. 通过修改LD_LIBRARY_PATH解决.
```export LD_LIBRARY_PATH=/usr/local/cppunit/lib:LD_LIBRARY_PATH```
